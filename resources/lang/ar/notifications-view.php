<?php

return [
    'notifications'=>'الاشعارات',
    'send_general'=>'إرسال إشعار عام',
    'text_ar'=>'النص(عربي)',
    'text_en'=>'النص(انكليزي)',
    'send'=>'ارسل',
    'processing'=>'جاري المعالجة ...',
    'notification_text'=>'نص الإشعار',
    'num_devices_all'=>'عدد الأجهزة (الكلي)',
    'num_devices_success'=>'عدد الأجهزة(نجاح)',
    'num_devices_failed'=>'عدد الأجهزة(فشل)',
    'send_time'=>'توقيت الإرسال',
    'progress'=>'تقدم العملية',
    'no_notifications'=>'لا يوجد أية إشعارات',
    'delete_message'=>'تم حذف الاشعار بنجاح',
    'send_message'=>'عملية إرسال الإشعارات بدأت للاطلاع على تقدم العملية  يمكنك رؤية الجدول في الاسفل , تستغرق العملية بضع دقائق',

];
<?php

return [
    'create_user'=>'تم إنشاء حسابك بنجاح وتم إرسال رمز التفعيل إلى :username',
    'verification_code.success'=>'تم التحقق بنجاح',
    'verification_code.verified_previously'=>'تم التحقق من حسابك سابقاً',
    'verification_code.send_code'=>'تم إرسال رمز التحقق إلى :usernameType',
    'app.lang.changed'=>'تم تغيير لغة التطبيق بنجاح',
    'customer.reset_password'=>'تم تغيير كلمة سر حسابك بنجاح',
    'customer.login_success'=>'تم تسجيل الدخول بنجاح',
    'customer.logout_success'=>'تم تسجيل الخروج بنجاح',
    'customer.delete'=>'تم حذف حسابك والبيانات المتعلقة به بنجاح',
    'customer.update'=>'تم تعديل معلومات حسابك بنجاح',
    'customer.update_need_verify_username'=>'تم تعديل معلومات حسابك بنجاح,تم إرسال رمز التحقق إلى :usernameType لاستخدامه في اكمال تغيير معرف حسابك',
    'customer.reset_username'=>'تم تغيير معرف حسابك بنجاح',
    'send_reset_link.success'=>'تم إرسال رابط إعادة تعيين كلمة السر بنجاح',
    'reset_password.success'=>'تم تغيير كلمة سر حسابك بنجاح',
    'settings.save'=>'تم الحفظ بنجاح',
    'notifications.clear_all'=>'تم مسح كل الاشعارات بنجاح',
    'notifications.delete'=>'تم حذف الاشعار بنجاح',
];


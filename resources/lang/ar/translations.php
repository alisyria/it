<?php

return [
    'upload'=>'رفع ملف الترجمة',
    'download'=>'تحميل ملف الترجمة',
    'select_excel'=>'اختر ملف اكسيل',
    'languages'=>'اللغات',
    'select_language'=>'اختر لغة',
    'download_btn'=>'تحميل',
    'key'=>'مفناح',
    'name'=>'اسم اللغة',
    'native'=>'اسم اللغة(محلياً)',
    'messages'=>[
        'success_update'=>'تم تحديث ترجمة التطبيق بنجاح',
        'langs_not_supported'=>'ليست كل اللغات مدعومة من التطبيق',
        'excel_key_required'=>'يجب أن يحتوي ملف الاكسيل على عمود key',
        'excel_group_required'=>'يجب أن يحتوي ملف الاكسيل على عمود group',
    ],
];
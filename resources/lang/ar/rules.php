<?php

return [
    'lang'=>'اللغة غير مدعومة',
    'latitude'=>'قيمة خط العرض غير صالحة',
    'longitude'=>'قيمة خط الطول غير صالحة',
    'local_value'=>'The value should contain array keyed by language codes',
    'file'=>[
        'image'=>'ملف الصورة غير صالح',
    ],
    'mobile'=>'رقم موبايل غير صالح',
    'password'=>[
        'invalid'=>'كلمة مرور خاطئة',
        'incorrect_user_password'=>'كلمة المرور الحالية خاطئة',
    ],
    'custom'=>[

    ],
];
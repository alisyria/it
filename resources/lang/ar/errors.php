<?php

return [
    'invalid_username_pass'=>':usernameType أو كلمة سر غير صالحة',
    'invalid_current_pass'=>'كلمة السر الحالية غير صحيحة',
    'verification_code.not_exist'=>'رمز التحقق خاطئ',
    'verification_code.expired'=>'انتهت صلاحية رمز التحقق ، يرجى طلب رمز تحقق آخر',
    'verification_code.invalid'=>'رمز التحقق غير صالح أو منتهي الصلاحية',
    'oauth.failed'=>'فشل تسجيل الدخول ، حاول مرة أخرى',
    'reset_password.invalid_token'=>'رابط منتهي الصلاحية أو غير صحيح',
    'users.not_found'=>'المستخدم غير موجود',
    'unauthorized'=>'لا تمتلك صلاحيات كافية للقيام بذلك',
    'throttle'=>'لقد تجاوزت الحد المسموح لعدد المحاولات , إعد المحاولة بعد :retry_after دقيقة/دقائق',
    'invalid_email_format'=>'يرجى التأكد من إدخال عنوان بريد إلكتروني صالح',
    'invalid_mobile_format'=>'يرجى التأكد من إدخال رقم هاتف محمول صحيح',    
];


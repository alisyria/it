<?php

return [
    'admin'=>[

        'default_images'=>[
            'settings'=>[
                'defaults_user_avatar'=>'الصورة الشخصية',
            ]
        ],
        'notification'=>[
            'text'=>'النص',
            'text.ar'=>'النص(عربي)',
            'text.en'=>'النص(انكليزي)',
            'text.ku'=>'النص(كردي)',
        ],
        'notification_image'=>[
            'title'=>'العنوان',
            'title.ar'=>'العنوان(عربي)',
            'title.en'=>'العنوان(انكليزي)',
            'title.ku'=>'العنوان(كردي)',
            'description'=>'الوصف',
            'description.ar'=>'الوصف(عربي)',
            'description.en'=>'الوصف(انكليزي)',
            'description.ku'=>'الوصف(كردي)',
            'url'=>'الرابط',
            'image'=>'الصورة'
        ],
        'settings'=>[

        ],
        'download_translations'=>[
            'langs'=>'اللغات',
        ],
        'device'=>[
            'serial_number'=>'معرف الجهاز',
            'model'=>'الموديل',
            'manufacturer'=>'الشركة الصانعة',
            'device_type_id'=>'نوع الجهاز',
            'owner_id'=>'المستثمر',
        ],
        'owner'=>[
            'name'=>'الاسم',
            'last_name'=>'الكنية',
            'department_id'=>'الدائرة',
        ],
        'order' => [
            'title' => 'عنوان الطلب',
            'description' => 'وصف المشكلة',
            'state' => 'حالة الطلب',
            'device_id' => 'الجهاز',
            'employee_id' => 'الموظف',
        ],
    ],
    'api'=>[
        'enquiry'=>[
            'name'=>'الاسم',
            'phone'=>'رقم الموبايل',
            'message'=>'الرسالة'
        ],
        'user'=>[
            'name'=>'الاسم',
            'username'=>'رقم الموبايل',
            'email'=>'الايميل',
            'avatar'=>'الصورة الشخصية',
        ],
    ],
];

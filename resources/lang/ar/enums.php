<?php

use App\Enums\Admin\AdminStatistic;
use App\Enums\OrderStatus;
use App\Enums\UsernameType;
use App\Enums\OS;
use App\Enums\Permission;
use App\Enums\Gender;
use App\Enums\FieldType;
use App\Enums\DeviceStatus;

return [
    UsernameType::class=>[
        UsernameType::USERNAME=>"اسم مستخدم",
    ],
    OS::class=>[
        OS::Android=>"أندرويد",
        OS::IOS=>"IOS"
    ],
    Gender::class=>[
        Gender::MALE=>'ذكر',
        Gender::FEMALE=>'انثى'
    ],
    Permission::class=>[

    ],
    AdminStatistic::class => [
        AdminStatistic::COUNT_EMPLOYEES => 'الموظفين',
        AdminStatistic::COUNT_DEVICES => 'الأجهزة',
        AdminStatistic::COUNT_ORDERS_PENDING => 'طلبات قيد الانتظار',
        AdminStatistic::COUNT_ORDERS_PROCESSING => 'طلبات قيد الاصلاح',
        AdminStatistic::COUNT_ORDERS_SUCCESSES => 'طلبات نجحت',
        AdminStatistic::COUNT_ORDERS_FAILED => 'طلبات فشلت',
    ],
    FieldType::class=>[
        FieldType::TEXT_SINGLE_LINE=>'نص سطر واحد',
        FieldType::SINGLE_OPTION=>'خيار واحد'
    ],
    DeviceStatus::class=>[
        DeviceStatus::OPERATIVE=>'شغال',
        DeviceStatus::INOPERATIVE=>'معطل',
    ],
    OrderStatus::class => [
        OrderStatus::PENDING => 'قيد الانتظار',
        OrderStatus::PROCESSING => 'جاري الإصلاح',
        OrderStatus::SUCCESSES => 'نجاح الإصلاح',
        OrderStatus::FAILED => 'فشل الإصلاح',
    ],
];

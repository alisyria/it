<?php

return [
    'reset_password'=>'إعادة تعيين كلمة المرور',
    'change_password'=>'تغيير كلمة السر',
    'change_password.message'=>'لتغيير كلمة المرور الحالية ، يرجى النقر على الزر أدناه',
];
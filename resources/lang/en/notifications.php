<?php

return [
    'reset_password'=>'Reset password',
    'change_password'=>'change password',
    'change_password.message'=>'To change your current password please click the button below',
    'enquiry'=>[
        'title'=>'Enquiry',
        'from'=>'Enquiry From :name',
        'name'=>'Name',
        'phone'=>'Phone',
        'email'=>'Email',
        'message_title'=>'Title'
    ],
];
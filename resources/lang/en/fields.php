<?php

return [
    'admin'=>[

        'default_images'=>[
            'settings'=>[
                'defaults_user_avatar'=>'User Avatar',
            ]
        ],
        'settings'=>[

        ],
        'download_translations'=>[
            'langs'=>'Languages',
        ],
        'employee'=>[
            'first_name'=>'First Name',
            'last_name'=>'Last Name',
            'email'=>'Email',
            'mobile'=>'Mobile',
            'username_type'=>'Username Type',
            'username'=>'Username',
            'password'=>'Password',
            'image'=>'Image',
            'roles'=>'Roles',
        ],
        'role'=>[
            'name'=>'Name',
            'description'=>'Description',
            'permissions'=>'Permissions',
        ],
        'notification'=>[
            'target'=>'Target',
            'vendor_type'=>'Vendor Type',
            'governorate_id'=>'Governorate',
            'region_id'=>'Region',
            'title'=>'Title',
            'text'=>'Text',
            'image'=>'Image',
        ],
        'profile'=>[
            'first_name'=>'First Name',
            'last_name'=>'Last Name',
            'email'=>'Email',
            'mobile'=>'Mobile',
            'username_type'=>'Username Type',
            'username'=>'Username',
            'new_password'=>'New Password',
            'new_password_confirmation'=>'New Password Confirmation',
            'image'=>'Image',
            'current_password'=>'Current Password',
        ],
        'device'=>[
            'serial_number'=>'Serial Number',
            'model'=>'Model',
            'manufacturer'=>'Manufacturer',
            'device_type_id'=>'Device Type',
            'owner_id'=>'Owner',
        ],
        'owner'=>[
            'name'=>'Name',
            'last_name'=>'Last Name',
            'department_id'=>'Department',
        ],
    ],
];

<?php

return [
    'invalid_username_pass'=>'invalid :usernameType or password',
    'invalid_current_pass'=>'incorrect current password',
    'verification_code.not_exist'=>'invalid verfication code',
    'verification_code.expired'=>'Verification code has expired, please request another verification code',
    'verification_code.invalid'=>'invalid or expired verification code',
    'oauth.failed'=>'Logon failed, try again',
    'reset_password.invalid_token'=>'Link is expired or incorrect',
    'users.not_found'=>'User does not exist',
    'unauthorized'=>'You do not have sufficient permissions to do so',
    'throttle'=>'You have exceeded maximum number of attempts, retry after :retry_after minute/minutes',
    'invalid_email_format'=>'please be sure that you entered a valid email address',
    'invalid_mobile_format'=>'please be sure that you entered a valid mobile number',
    'product_add_basket_out_of_stock'=>'This product is out of stock',
];


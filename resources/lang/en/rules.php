<?php

return [
    'model_exist'=>':attribute not exists',
    'lang'=>'The language is unsupported',
    'latitude'=>'Invalid latitude value',
    'longitude'=>'Invalid longitude value',
    'local_value'=>'The value should contain array keyed by language codes',
    'file'=>[
        'image'=>'Invalid image',
    ],
    'mobile'=>'Invalid mobile number',
    'password'=>[
        'invalid'=>'Invalid password',
        'incorrect_user_password'=>'Inavlid Current Password',
    ],
    'custom'=>[
        'verification_code'=>[
            'not_exist'=>'invalid verfication code',
            'expired'=>'Verification code has expired, please request another verification code',
        ],
        'password'=>[
            'invalid_current'=>'incorrect current password',
        ],
        'currency'=>[
            'invalid'=> 'Invalid Currency'
        ],
        'promocode'=>[
            'invalid'=>'Invalid Promocode',
            'expired'=>'Expired Promocode',
            'not_applicable'=>'Inapplicable Promocode',
            'exists'=>'Promocode code already exists',
        ],
        'product_not_exist'=>'Product not found',
        'product_exceed_max_request'=>'maximum quantity to order product :product_title is :max_quantity :unit',
        'product_less_min_request'=>'minimum quantity to order product :product_title is :min_quantity :unit',
        'product_out_of_stock'=>'product :product_title is out of stock',
        'customer_shipping_address_invalid'=>'Invalid Shipping Address',
    ],
];

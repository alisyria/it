<?php

use App\Enums\UsernameType;
use App\Enums\OS;
use App\Enums\Permission;
use App\Enums\FileType;
use App\Enums\Gender;
use App\Enums\EnquiryStatus;
use App\Enums\Period;
use App\Enums\Admin\AdminStatistic;
use App\Enums\UserType;
use App\Enums\OrderStatus;
use App\Enums\TranslationType;
use App\Enums\NotificationTarget;

return [
    UsernameType::class=>[
        UsernameType::USERNAME=>"Username",
    ],
    OS::class=>[
        OS::Android=>"Android",
        OS::IOS=>"IOS"
    ],
    Gender::class=>[
        Gender::MALE=>'Male',
        Gender::FEMALE=>'Female'
    ],
    FileType::class=>[
        FileType::IMAGE=>'image',
        FileType::VIDEO=>'video',
    ],
    Permission::class=>[
        Permission::MANAGE_EMPLOYEES=>'manage employees',
        Permission::MANAGE_ROLES=>'manage roles',
        Permission::MANAGE_DEVICES=>'manage devices',
        Permission::MANAGE_MAINTENANCE=>'manage maintenance',
        Permission::MANAGE_SETTINGS=>'manage settings',
        Permission::MANAGE_LOCALIZATION=>'manage localization',
    ],
    EnquiryStatus::class=>[
        EnquiryStatus::PENDING=>'Pending',
        EnquiryStatus::RESPONDED=>'Responded',
    ],
    Period::class=>[
        Period::UNDETERMINED=>'Undetermined',
        Period::YEARLY=>'Yearly',
        Period::MONTHLY=>'Monthly',
        Period::DAILY=>'Daily',
    ],
    AdminStatistic::class=>[
        //count
        AdminStatistic::COUNT_EMPLOYEES=>'Employees',
    ],
    UserType::class=>[
        UserType::SUPER_ADMIN=>'Super Admin',
        UserType::EMPLOYEE=>'Employee',
    ],
    TranslationType::class=>[
        TranslationType::GENERAL=>'General',
    ],
    NotificationTarget::class=>[
        NotificationTarget::COMPANIES=>'Companies',
        NotificationTarget::SHOPS=>'Shops',
    ],
    OrderStatus::class => [
        OrderStatus::PENDING => 'Pending',
        OrderStatus::PROCESSING => 'Processing',
        OrderStatus::SUCCESSES => 'Successes',
        OrderStatus::FAILED => 'Failed',
    ],
];

<?php

return [
    'hi'=>'Hi :name',
    'verification.code'=>'The verification code for your account is :code',
    'thanks'=>'Thanks',
];


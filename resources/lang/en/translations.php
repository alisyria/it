<?php

return [
    'upload'=>'Upload Translations',
    'download'=>'Download Translations',
    'select_excel'=>'Select Excel File',
    'languages'=>'languages',
    'select_language'=>'select language',
    'download_btn'=>'Download',
    'key'=>'key',
    'name'=>'name',
    'native'=>'native',
    'messages'=>[
        'success_update'=>'Application translation successfully updated',
        'langs_not_supported'=>'Not all languages are supported by the app',
        'excel_key_required'=>'The excel file should contain "key" column',
        'excel_group_required'=>'The excel file should contain "group" column',
    ],
];
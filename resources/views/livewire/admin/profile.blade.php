@section('title',$user->full_name)
<div class="mb-24">
    <x-components.page-header class="flex justify-between">
        <x-components.page-title>{{ $user->full_name }}</x-components.page-title>
    </x-components.page-header>

    <form wire:submit.prevent="store" enctype="multipart/form-data">
        @php $transBase="fields.admin.profile."; @endphp
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-0 p-1 px-2 bg-white md:w-full lg:w-7/12 border-t">
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'first_name')</label>
                <x-form.input name="first_name" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'last_name')</label>
                <x-form.input name="last_name" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'email')</label>
                <x-form.input type='email' name="email" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'mobile')</label>
                <x-form.input name="mobile" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'username_type')</label>
                <x-form.select name="username_type" live-name="username_type" :items="$this->usernameTypes"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'username')</label>
                <x-form.input type="text" name="username" live-name="username" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'new_password')</label>
                <x-form.input type="password" name="new_password" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'new_password_confirmation')</label>
                <x-form.input type="password" name="new_password_confirmation" :model="null"/>
            </div>
            <div class="mt-2">
                <label  class="mb-2 block text-sm">@lang($transBase.'image')</label>
                <x-form.img live-name="image" :temporary-url="optional($image)->temporaryUrl() ?? $imageUrl" :mimes="'image/*'"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@lang($transBase.'current_password')</label>
                <x-form.input type="password" name="current_password" :model="null"/>
            </div>
        </div>
        <div class="text-center md:w-full lg:w-7/12 bg-white py-4">
            <x-components.save-button live-target="store" />
        </div>
    </form>
</div>

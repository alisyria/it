<div>
    <div class="mb-24">
        <x-components.page-header>
            <x-components.page-title>@langj('Localization')</x-components.page-title>
        </x-components.page-header>

        <div class="flex justify-start">
            <button wire:click="openUploadModal" wire:loading.attr="disabled" class="px-4 py-2 font-medium leading-5 text-white transition-colors duration-150 bg-green-600 border border-transparent rounded-lg active:bg-green-600 hover:bg-green-700 focus:outline-none focus:shadow-outline-purple text-sm me-2">
                <x-icons.css-spinner-two class="animate-spin" wire:loading wire:target="openUploadModal"/> <x-icons.fas-cloud-upload-alt class="w-6 h-6 inline-block" wire:loading.remove wire:target="openUploadModal"/> @langj('Upload Translations')
            </button>

            <x-components.drop-down :title="__j('Download Translations')" >
                <x-slot name="icon">
                    <x-icons.css-spinner-two class="animate-spin w-6 h-6 me-1" wire:loading="download" wire:target="download"/>
                    <x-icons.fas-cloud-download-alt class="w-6 h-6 me-1 inline-block" wire:loading.remove="download" wire:target="download"/>
                </x-slot>
                <x-slot name="options">
                    @foreach(\App\Enums\TranslationType::asSelectArray() as $key=>$text)
                        <x-components.drop-down-anchor-option :title="$text" :href="route('admin.localization.download',['translation_type'=>$key])"/>
                    @endforeach
                </x-slot>
            </x-components.drop-down>
        </div>

        <x-components.data-table sortable-entity-name="appLanguages" class="mt-6 lg:w-4/6">
            <x-slot name="head">
                <tr>
                    <x-components.data-table-th></x-components.data-table-th>
                    <x-components.data-table-th class="text-start">@lang('translations.key')</x-components.data-table-th>
                    <x-components.data-table-th class="text-start">@lang('translations.name')</x-components.data-table-th>
                    <x-components.data-table-th class="text-start">@lang('translations.native')</x-components.data-table-th>
                </tr>
            </x-slot>
            <x-slot name="body">
                @foreach($this->appLanguages as $appLanguage)
                    <tr data-itemId="{{{ $appLanguage->id }}}" class="hover:bg-gray-50">
                        <x-components.data-table-sortable-handle />
                        <x-components.data-table-td>
                            {{ $appLanguage->id }}
                        </x-components.data-table-td>

                        <x-components.data-table-td>
                            {{ $appLanguage->name }}
                        </x-components.data-table-td>

                        <x-components.data-table-td>
                            {{ $appLanguage->native }}
                        </x-components.data-table-td>
                    </tr>
                @endforeach
            </x-slot>
        </x-components.data-table>
    </div>

    <x-components.modal :is-modal-open="$isModalOpen" action="upload">
        <x-slot name="title">
            @langj('Upload Translations')
        </x-slot>
        <x-slot name="body">
            <div class="mt-2">
                <span class="text-gray-700 font-semibold">@langj('Translations Excel File')</span>
                <x-form.file live-name="file" :url="$fileUrl" :fileName="$fileName" mimeType="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
            </div>
        </x-slot>
        <x-slot name="buttons">
            <x-components.save-button live-target="upload"/>
        </x-slot>
    </x-components.modal>
</div>



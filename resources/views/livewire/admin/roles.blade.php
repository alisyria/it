@section('title',__j('Roles'))
<div class="mb-24">
    <x-components.page-header class="flex justify-between">
        <x-components.page-title>@langj('Roles')</x-components.page-title>
    </x-components.page-header>

    <x-components.crud-toolbox>
        <x-components.create-button>
            @langj('New Role')
        </x-components.create-button>
    </x-components.crud-toolbox>

    <div class="mt-4">
        <livewire:admin.data-tables.roles />
    </div>

    <x-components.modal :is-modal-open="$isModalOpen" action="store()">
        <x-slot name="title">
            @if($this->isCreateOpertion())
                @langj('New Role')
            @else
                @langj('Update :name',[
                'name'=>$this->role->name
                ])
            @endif
        </x-slot>
        <x-slot name="body">
            <div class="mt-0">
                <label class="mb-2 block text-sm">@langj('Name')</label>
                <x-form.input name="name" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@langj('Description')</label>
                <x-form.input-langs type="textarea" name="description" :model="null"/>
            </div>
            <div class="mt-2">
                <label class="mb-2 block text-sm">@langj('Permissions')</label>
                @error('permissions')
                <p class="text-xs text-red-600 px-1">{{ $message }}</p>
                @enderror
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($this->permissionsModels as $permission)
                            <tr>
                                <td>
                                    <x-form.checkbox label="{{ \App\Enums\Permission::getDescription($permission->name) }}" name="permissions[{{ $permission->id }}]" live-name="{{'permissions.'.$permission->id}}" value="{{$permission->id}}"/>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </x-slot>
        <x-slot name="buttons">
            <x-components.save-button live-target="store" />
        </x-slot>
    </x-components.modal>
</div>

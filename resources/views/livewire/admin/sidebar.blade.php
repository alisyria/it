<x-admin.layouts.sidebar>
    <x-admin.layouts.sidebar-link :title="__j('Dashboard')"  href="{{ route('admin.home') }}" :active="route_is('admin.home')">
        <x-slot name="icon">
            <x-icons.bxs-dashboard-fill class="w-4 h-4"/>
        </x-slot>
    </x-admin.layouts.sidebar-link>

    <x-admin.layouts.sidebar-link :title="__j('Devices')"  href="{{ route('admin.devices.index') }}" :active="route_is('admin.devices.index')" :permission="\App\Enums\Permission::MANAGE_DEVICES">
        <x-slot name="icon">
            <x-icons.zondicon-computer-desktop class="w-4 h-4"/>
        </x-slot>
    </x-admin.layouts.sidebar-link>

    <x-admin.layouts.sidebar-link title="طلبات الصيانة"  href="{{ route('admin.orders.index') }}" :active="route_is('admin.orders.index')" :permission="\App\Enums\Permission::MANAGE_DEVICES">
        <x-slot name="icon">
            <x-icons.carbon-license-maintenance-32 class="w-4 h-4"/>
        </x-slot>
    </x-admin.layouts.sidebar-link>

    <x-admin.layouts.sidebar-link-collection :title="__j('Employees')" :active="Request::is('employees-section*')"
                                             :permissions="[\App\Enums\Permission::MANAGE_EMPLOYEES,\App\Enums\Permission::MANAGE_ROLES]">
        <x-slot name="icon">
            <x-icons.fas-user-tie class="w-4 h-4"/>
        </x-slot>

        <x-admin.layouts.sidebar-link-collection-item :title="__j('Employees')"
                                                      href="{{ route('admin.employees.index') }}" :active="Request::is('employees-section/employees*')" :permission="\App\Enums\Permission::MANAGE_EMPLOYEES"/>
        <x-admin.layouts.sidebar-link-collection-item :title="__j('Roles')"
                                                      href="{{ route('admin.roles.index') }}" :active="Request::is('employees-section/roles*')" :permission="\App\Enums\Permission::MANAGE_ROLES"/>
    </x-admin.layouts.sidebar-link-collection>

    <x-admin.layouts.sidebar-link-collection :title="__j('Settings')" :active="Request::is('settings*')"
                                             :permissions="[\App\Enums\Permission::MANAGE_SETTINGS,\App\Enums\Permission::MANAGE_LOCALIZATION]">
        <x-slot name="icon">
            <x-icons.bxs-select-multiple class="w-4 h-4"/>
        </x-slot>
        <x-admin.layouts.sidebar-link-collection-item :title="__j('Localization')"
                                                      href="{{ route('admin.localization.index') }}" :active="Request::is('settings/localization*')" :permission="\App\Enums\Permission::MANAGE_LOCALIZATION"/>
        <x-admin.layouts.sidebar-link-collection-item :title="__j('Others')"
                                                      href="{{ route('admin.settings.others') }}" :active="Request::is('settings/others*')" :permission="\App\Enums\Permission::MANAGE_SETTINGS"/>
    </x-admin.layouts.sidebar-link-collection>

</x-admin.layouts.sidebar>

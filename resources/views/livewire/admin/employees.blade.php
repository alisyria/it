@section('title',__j('Employees'))
<div class="mb-24">
    <x-components.page-header class="flex justify-between">
        <x-components.page-title>@langj('Employees')</x-components.page-title>
    </x-components.page-header>

    <x-components.crud-toolbox>
        <x-components.create-button>
            @langj('New Employee')
        </x-components.create-button>
    </x-components.crud-toolbox>

    <div class="mt-4">
        <livewire:admin.data-tables.employees />
    </div>

    <x-components.modal :is-modal-open="$isModalOpen" action="store()" class="lg:max-w-3xl">
        <x-slot name="title">
            @if($this->isCreateOpertion())
                @langj('New Employee')
            @else
                @langj('Update :name',[
                'name'=>$this->user->full_name
                ])
            @endif
        </x-slot>
        <x-slot name="body">
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                <div class="mt-2">
                    <label class="mb-2 block text-sm">@langj('First Name')</label>
                    <x-form.input name="first_name" :model="null"/>
                </div>
                <div class="mt-2">
                    <label class="mb-2 block text-sm">@langj('Last Name')</label>
                    <x-form.input name="last_name" :model="null"/>
                </div>
                <div class="mt-2">
                    <label class="mb-2 block text-sm">@langj('Email')</label>
                    <x-form.input type='email' name="email" :model="null"/>
                </div>
                <div class="mt-2">
                    <label class="mb-2 block text-sm">@langj('Mobile')</label>
                    <x-form.input name="mobile" :model="null"/>
                </div>
                <div class="mt-2">
                    <label class="mb-2 block text-sm">@langj('Username')</label>
                    <x-form.input type="text" name="username" live-name="username" :model="null"/>
                </div>
                <div class="mt-2">
                    <label class="mb-2 block text-sm">@langj('Password')</label>
                    <x-form.input name="password" :model="null"/>
                </div>
                <div class="mt-2">
                    <label class="mb-2 block text-sm">@langj('Roles')</label>
                    <x-admin.select.roles name="roles[]" live-name="roles" id="roles"/>
                </div>
                <div class="mt-2">
                    <label  class="mb-2 block text-sm">@langj('Image')</label>
                    <x-form.img live-name="image" :temporary-url="optional($image)->temporaryUrl() ?? $imageUrl" :mimes="'image/*'"/>
                </div>
            </div>
        </x-slot>
        <x-slot name="buttons">
            <x-components.save-button live-target="store" />
        </x-slot>
    </x-components.modal>
</div>

<div class="mt-4 mb-8">
    <x-components.tab active="about-us">
        <x-slot name="links">
            <x-components.tab-link id="about-us" :label="__j('About Us')">
                <x-slot name="class">
                    bg-gray-100
                </x-slot>
            </x-components.tab-link>
        </x-slot>
        <x-slot name="panes">
            <x-components.tab-pane id="about-us" class="p-1 bg-white p-2">
                <x-slot name="content">
                    <livewire:admin.settings.about-us/>
                </x-slot>
            </x-components.tab-pane>
        </x-slot>
    </x-components.tab>
</div>

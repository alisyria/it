@section('title',__j('Orders'))
<div class="mb-24">
    <x-components.page-header class="flex justify-between">
        <x-components.page-title>@langj('طلبات الصيانة')</x-components.page-title>
    </x-components.page-header>

    <x-components.crud-toolbox>
        <div>
            <button wire:click="$emit('openModal','admin.modals.save-order-modal')" wire:loading.attr="disabled" wire:target="create,edit" class="px-4 py-2 font-medium leading-5 text-white transition-colors duration-150 bg-green-600 border border-transparent rounded-lg active:bg-green-600 hover:bg-green-700 focus:outline-none focus:shadow-outline-purple text-sm">@langj('طلب جديد')</button>
        </div>
    </x-components.crud-toolbox>

    <div class="mt-4">
        <livewire:admin.data-tables.orders/>
    </div>
</div>

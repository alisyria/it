<div class="mt-0">
    <form wire:submit.prevent="store" enctype="multipart/form-data">
        <div class="mt-2">
            <label class="mb-2 block">@langj('App Name')</label>
            <x-form.input-langs name="app_name" :model="null"/>
        </div>

        <div class="mt-3 text-center">
            <x-components.save-button live-target="store"/>
        </div>
    </form>
</div>

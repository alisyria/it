<div class="p-4" dir="{{ localClass() }}">
    <h2 class="text-start font-semibold text-2xl">{{ $this->pageTitle() }}</h2>
    @php $transBase="fields.admin.order."; @endphp
    <form wire:submit.prevent="store">
        <div class="grid grid-cols-1 md:grid-cols-3 gap-4 py-2 pb-8" dir="{{ localClass() }}">
            <div class="mt-2 text-start col-span-3">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'title')</label>
                <x-form.input name="title" :model="null"/>
            </div>
            <div class="mt-2 text-start col-span-3">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'description')</label>
                <x-form.input type="textarea" name="description" :model="null"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'device_id')</label>
                <x-select.device name="device_id" :lazy-update="true"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'employee_id')</label>
                <x-select.employee name="employee_id" :lazy-update="true"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'state')</label>
                <x-select.order-status name="state" :lazy-update="true"/>
            </div>
        </div>
        @if($this->sections->isNotEmpty())
            <h3 class="font-weight-bold text-2xl text-center pb-1 pt-2">إجرائية الإصلاح</h3>
            @foreach($this->sections as $section)
                <h2 class="text-start border-b-2">{{ $section->title }}</h2>
                <div class="grid grid-cols-1 py-2 pb-8" dir="{{ localClass() }}">
                @foreach($section->items as $item)
                    <div class="mt-2 text-start">
                        <label class="mb-2 block text-sm text-start">{{ $item->text }}</label>
                        <x-form.input type="textarea" rows="1" name="items[{{ $item->id }}]" live-name="items.{{ $item->id }}" :model="null"/>
                    </div>
                @endforeach
                </div>
            @endforeach
        @endif
        <div class="text-start">
            <x-components.save-button live-target="store" />
            <button type="button" wire:click.prevent="closeModal" class="w-full px-5 py-3 text-sm font-medium leading-5 text-white text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg dark:text-gray-400 sm:px-4 sm:py-2 sm:w-auto active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray">
                @langj('Cancel')
            </button>
        </div>
    </form>
</div>

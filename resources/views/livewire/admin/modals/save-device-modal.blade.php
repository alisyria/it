<div class="p-4" dir="{{ localClass() }}">
    <h2 class="text-start font-semibold text-2xl">@langj('New Device')</h2>
    @php $transBase="fields.admin.device."; @endphp
    <form wire:submit.prevent="store">
        <div class="grid grid-cols-1 md:grid-cols-3 gap-4 py-2 pb-8" dir="{{ localClass() }}">
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">معرف الجهاز</label>
                <x-form.input name="serial_number" :model="null"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'model')</label>
                <x-form.input name="model" :model="null"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'manufacturer')</label>
                <x-form.input name="manufacturer" :model="null"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'device_type_id')</label>
                <x-select.device-type name="device_type_id" :lazy-update="true"/>
            </div>
            <div class="mt-2 text-start col-span-2">
                <label class="mb-2 block text-sm text-start">المستثمر</label>
                <div class="w-3/4 inline-block">
                    <x-select.owner-raw name="owner_id"/>
                </div>
                <button wire:click.prevent="$emit('openModal','admin.modals.save-owner-modal')" wire:loading.attr="disabled" class="px-4 py-2 font-medium leading-5 text-white transition-colors duration-150 bg-green-600 border border-transparent rounded-lg active:bg-green-600 hover:bg-green-700 focus:outline-none focus:shadow-outline-purple text-sm inline">+</button>
            </div>
        </div>
        @if($this->properties->isNotEmpty())
        <h3 class="font-semibold text-start border-b-2 pb-1">الخصائص</h3>
        <div class="grid grid-cols-1 md:grid-cols-3 gap-4 py-2 pb-16" dir="{{ localClass() }}">
            @foreach($this->properties as $property)
                <div class="mt-2 text-start">
                    <label class="mb-2 block text-sm text-start">{{ $property->name }}</label>
                    @if($property->isFieldTextSingleLine())
                        <x-form.input name="options[{{ $property->id }}]" live-name="options.{{ $property->id }}" :model="null"/>
                    @elseif($property->isFieldSingleOption())
                        <x-form.select name="options[{{ $property->id }}]" value="text" live-name="options.{{ $property->id }}" :items="$property->options"/>
                    @endif
                </div>
            @endforeach
        </div>
        @endif
        <div class="text-start">
            <x-components.save-button live-target="store" />
            <button type="button" wire:click.prevent="closeModal" class="w-full px-5 py-3 text-sm font-medium leading-5 text-white text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg dark:text-gray-400 sm:px-4 sm:py-2 sm:w-auto active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray">
                @langj('Cancel')
            </button>
        </div>
    </form>
</div>

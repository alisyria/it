<div class="p-4" dir="{{ localClass() }}">
    <h2 class="text-start font-semibold text-2xl">@langj('New Owner')</h2>
    @php $transBase="fields.admin.owner."; @endphp
    <form wire:submit.prevent="store">
        <div class="grid grid-cols-1 md:grid-cols-3 gap-4 py-2 pb-8" dir="{{ localClass() }}">
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'name')</label>
                <x-form.input name="name" :model="null"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">@lang($transBase.'last_name')</label>
                <x-form.input name="last_name" :model="null"/>
            </div>
            <div class="mt-2 text-start">
                <label class="mb-2 block text-sm text-start">الدائرة</label>
                <x-select.department name="department_id" :model="null"/>
            </div>
        </div>

        <div class="text-start">
            <x-components.save-button live-target="store" />
            <button type="button" wire:click.prevent="$emit('closeModal')" class="w-full px-5 py-3 text-sm font-medium leading-5 text-white text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg dark:text-gray-400 sm:px-4 sm:py-2 sm:w-auto active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray">
                @langj('Cancel')
            </button>
        </div>
    </form>
</div>


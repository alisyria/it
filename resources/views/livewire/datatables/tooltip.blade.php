<span class="relative group cursor-pointer" x-data="{show:false}" x-on:mouseenter="show=true" x-on:mouseleave="show=false">
    <span class="inline-block flex items-center">{{ Str::limit($slot, $length) }}</span>
    <span x-show="show" class="group-hover:block absolute z-10 -ms-28 w-96 mt-2 p-2 text-xs whitespace-pre-wrap rounded-lg bg-gray-100 border border-gray-300 shadow-xl text-gray-700 text-start whitespace-normal">{{ $slot }}</span>
</span>

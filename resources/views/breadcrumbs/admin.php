<?php

use App\Models\Device;
use App\Models\Employee;
use App\Models\Role;
use Illuminate\Support\Str;


/**
 *  Employees
 */
Breadcrumbs::register('admin.employees.index',function($breadcrumbs){
    $breadcrumbs->push(__j('Employees'),route('admin.employees.index'));
});
Breadcrumbs::register('admin.employees.show',function($breadcrumbs,Employee $employee){
    $breadcrumbs->parent('admin.employees.index');
    $breadcrumbs->push($employee->user->full_name,route('admin.employees.show',[$employee]));
});
/**
 *  Roles
 */
Breadcrumbs::register('admin.roles.index',function($breadcrumbs){
    $breadcrumbs->push(__j('Roles'),route('admin.roles.index'));
});
Breadcrumbs::register('admin.roles.show',function($breadcrumbs,Role $role){
    $breadcrumbs->parent('admin.roles.index');
    $breadcrumbs->push($role->name,route('admin.roles.show',[$role]));
});
/**
 *  Devices
 */
Breadcrumbs::register('admin.devices.index',function($breadcrumbs){
    $breadcrumbs->push(__j('Devices'),route('admin.devices.index'));
});
Breadcrumbs::register('admin.devices.show',function($breadcrumbs,Device $device){
    $breadcrumbs->parent('admin.devices.index');
    $breadcrumbs->push($device->id,route('admin.devices.show',[$device]));
});

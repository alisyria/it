<div>
    <x-form.select2 :name="$name" :live-name="$liveName" :id="$id" :options="$options()" multiple>

    </x-form.select2>
</div>

@if($show())
<li class="relative px-6 py-3">
    <div wire:ignore>
        @if($active)
            <span class="absolute inset-y-0 left-0 w-1 bg-green-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>
        @endif
    </div>
    <a
        class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-green-700 dark:hover:text-gray-200"
        href="{{ $href }}"
    >
        {{ $icon }}
        <span class="ms-4">{{ $title }}</span>
        {{ $slot }}
    </a>
</li>
@endif

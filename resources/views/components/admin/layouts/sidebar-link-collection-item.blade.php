@if($show())
<li
    class="px-2 py-1 transition-colors duration-150 hover:text-green-600 {{ $active?'text-green-800':'' }} dark:hover:text-gray-200" wire:ignore.self
>
    <div wire:ignore>
        @if($active)
            <span class="absolute inset-y-0 left-0 w-1 bg-green-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>
        @endif
    </div>

    <a class="w-full" href="{{ $href }}">{{ $title }}</a>
</li>
@endif

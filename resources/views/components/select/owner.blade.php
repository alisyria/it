<x-form.select2  :name="$name" data-source-route="{{ route('admin.dataSources.owners') }}" :placeholder="__j('Select Owner')">
    <x-slot name="textExp">
        obj.name
    </x-slot>
</x-form.select2>

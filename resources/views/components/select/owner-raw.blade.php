<x-form.select :name="$name" value="full_name_with_department" :live-name="$liveName" :items="$owners()" :lazy-update="$lazyUpdate" class="{{ $class }}"/>

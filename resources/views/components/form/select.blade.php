<div>
    <select {{ $attributes->merge(['class' => 'form-select mt-1 block w-full']) }} @if($lazyUpdate) wire:model.lazy @else wire:model.defer @endif="{{ $liveName }}">
        @if($isItemsArray())
            <option value="">{{ $placeholder }}</option>
            @foreach($items as $key=>$text)
                <option value="{{ $useTextAsKey ?$text:$key }}">{{ $text }}</option>
            @endforeach
        @else
            <option value="">{{ $placeholder }}</option>
            @foreach($items as $item)
                <option value="{{ $item->$key }}">{{ $item->$value }}</option>
            @endforeach
        @endif
    </select>
    @error($liveName)
    <p class="text-xs text-red-600 px-1 text-start">{{ $message }}</p>
    @enderror
</div>

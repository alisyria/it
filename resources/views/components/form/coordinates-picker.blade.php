
<div>
    <label class="mb-2 block text-sm">{{ title_case(__('Location on the map')) }}</label>
    <p class="text-red-600 mb-2 text-xs">
        - @langj('you could enter coordinates immediately into latitude & longitude fields')<br/>
        - @langj('you could drag & drop the red marker on the target location')<br/>
        - @langj('you could click at any point on the map to put the marker on it')<br/>
        - @langj('you could search by place name')<br/>
    </p>

    <div class="row" x-data>
        <div class="col-md-12">
            <div class="grid grid-cols-4 gap-4 mb-2">
                <div class="">
                    <div class="form-group">
                        <label class="mb-2 block text-sm">@lang('Latitude')</label>
{{--                        x-on:input="alert($event.target.value);"--}}
                        <x-form.input type="text" id="{{ $latitudeId }}" name="{{ $latitudeName }}" live-name="{{ $latitudeLiveName }}" :model="null" class="ltr"  />
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label class="mb-2 block text-sm">@lang('Longitude')</label>
{{--                        x-on:change="$dispatch('input', value);"--}}
                        <x-form.input type="text" id="{{ $longitudeId }}" name="{{ $longitudeName }}" live-name="{{ $longitudeLiveName }}" :model="null" class="ltr"/>
                    </div>
                </div>
                <div class="col-span-2">
                    <div class="form-group">
                        <label class="mb-2 block text-sm">@lang('Search by location')</label>
{{--                        //wire:model.defer="{{ $addressLiveName }}"--}}
                        <input type="text" id="{{ $addressId }}"   class="form-input mt-1 block w-full" autocomplete="off" placeholder="@lang('Type the name of the region')"/>
                    </div>
                </div>
            </div>
            <div class="embed-responsive aspect-ratio-16/9 mb-2" wire:ignore>
                <div id="{{ $id }}" class="embed-responsive-item"></div>
            </div>
        </div>
    </div>
</div>

@once
    @push('js-links')
        <script src="https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCAmSbgPfqla7UjQjRaMmYOffTh3bL7_vQ"></script>
        <script src="{{ asset('vendor/jquery-locationpicker/src/locationpicker.jquery.js') }}"></script>
    @endpush
    @push('js-scripts')
        <script>
            function initiateJqueryLocationPicker(id,latitude,longitude,zoom,latitudeID,longitudeID,addressId)
            {
                var mapSelector=$('#'+id);
                mapSelector.locationpicker({
                    location: {
                        latitude: latitude,
                        longitude: longitude
                    },
                    radius:0,
                    zoom :zoom,
                    mapOptions: {
                        draggableCursor:"crosshair",
                        mapTypeControl:true
                    },

                    markerCursor: 'grap',
                    mapTypeId: google.maps.MapTypeId.ROADMAP,

                    inputBinding:{
                        latitudeInput:$("#"+latitudeID),
                        longitudeInput:$("#"+longitudeID),
                        locationNameInput: $('#'+addressId)
                    },
                    enableAutocomplete: true,
                    oninitialized: function(){
                        var cb = function (event) {
                            mapSelector.locationpicker('location', {
                                latitude: event.latLng.lat(),
                                longitude: event.latLng.lng()
                            });

                        @this.set('{{ $latitudeLiveName }}',event.latLng.lat());
                        @this.set('{{ $longitudeLiveName }}',event.latLng.lng());
                            // document.getElementById(latitudeID).dispatchEvent(new Event('input'));
                            // document.getElementById(longitudeID).dispatchEvent(new Event('input'));
                        };
                        mapSelector.locationpicker('subscribe', {
                            event: 'click',
                            callback: cb
                        });
                    },
                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                        // console.log(currentLocation);
                        @this.set('{{ $latitudeLiveName }}',currentLocation.latitude);
                        @this.set('{{ $longitudeLiveName }}',currentLocation.longitude);
                        // document.getElementById(latitudeID).dispatchEvent(new Event('input'));
                        // document.getElementById(longitudeID).dispatchEvent(new Event('input'));
                    },
                });
            }
        </script>
    @endpush
    @push('css-scripts')
        <style>
            .pac-container{z-index:2000 !important;}
        </style>
    @endpush
@endonce

@push('js-scripts')
    <script>
        $initializer=initiateJqueryLocationPicker('{{ $id }}',{{ $latitude }},{{ $longitude }},{{ $zoom }},'{{ $latitudeId }}','{{ $longitudeId }}','{{ $addressId }}');
        $(document).ready(function(){
            $initializer;
        });
        Livewire.on("pickLocation#{{ $id }}",function(data){
            {{--$('#'+'{{ $latitudeId }}').val(data.latitude).change();--}}
            {{--$('#'+'{{ $longitudeId }}').val(data.longitude).change();--}}
            {{--document.getElementById('').value=data.latitude;--}}
            {{--document.getElementById('{{ $longitudeId }}').value=data.longitude;--}}
            initiateJqueryLocationPicker('{{ $id }}',data.latitude,data.longitude,{{ $zoom }},'{{ $latitudeId }}','{{ $longitudeId }}','{{ $addressId }}');
        });
    </script>
@endpush

<div wire:model.defer="{{ $liveName }}">
    <div x-data="{
        selected:[],
        isSelected(value){
            if(Array.isArray(this.selected))
            {
                var items=JSON.parse(JSON.stringify(this.selected));
                return items.find(element => element == value)==value;
            }
            return value==this.selected;
        }
    }" x-init="()=>{
    $('#{{ $id }}').on('select2:select', function (e) {
        if(!@json($multiple))
        {
            selected=e.params.data.id;
        }
        else
        {
            selected.push(e.params.data.id);
        }
    }).on('select2:unselect', function (e) {
        if(!@json($multiple))
        {
            selected=null;
        }
        else
        {
            if(e.params.data.id && selected)
            {
                selected=selected.filter(function(value, index, arr){
                    return value !=e.params.data.id;
                });
{{--                const index = selected.indexOf(''+e.params.data.id+'');console.log(index);--}}
    {{--                if (index > -1) {--}}
    {{--                  selected=selected.splice(index, 1);--}}
    {{--                }--}}
        }
    }
})
.on('select2:clear', function (e) {
        if(!@json($multiple))
        {
            selected=null;
        }
        else
        {
            selected=[];
        }
});
if(!@json($multiple))
    {
        $wire.on('{{$liveName}}Updated',(id,text)=>{
            selected=id;
            $('#{{ $id }}').append(new Option(text, id, true, true)).trigger('change');
            $('#{{ $id }}').val(id).trigger('change');
        });
        $watch('selected',value=>$dispatch('input', value));
    }
    else
    {
        $wire.on('{{$liveName}}Updated',(options)=>{
            selected=[];
            var ids=[];
            for(var i=0;i <options.length; i++)
            {
                if($('#{{ $id }}').find('option[value=\''+ options[i].id + '\']').length)
                {
                    ids.push(options[i]['id']);
                }
                else
                {
                       $('#{{ $id }}').append(new Option(options[i]['text'], options[i]['id'], true, true)).trigger('change');
                            ids.push(options[i]['id']);
                }
            }
            $('#{{ $id }}').val(ids).trigger('change');
            selected=ids;
        });
        $watch('selected',value=>$dispatch('input', value));
    }
}">
        <div wire:ignore>
            <select name="{{ $name }}" {{ $multiple?'multiple':'' }}  id="{{ $id }}" {{ $attributes->merge(['class'=>'form-control']) }} style="width:100%;" wire:key="{{ \Illuminate\Support\Str::random() }}">
                @unless($hasDataSource)
                    @if($placeholder)
                        <option value="">{{ $placeholder }}</option>
                    @endif
                    @if($isOptionsArray())
                        @foreach($options as $key=>$text)
                            @php $value=$useTextAsKey ?$text:$key; @endphp
                            <option value="{{ $value }}">{{ $text }}</option>
                        @endforeach
                    @else
                        @foreach($options as $option)
                            <option value="{{ $option->$key }}">{{ $option->$value }}</option>
                        @endforeach
                    @endif
                @endunless
            </select>
        </div>
        <div>
            @error($liveName)
            <p class="text-xs text-red-600 px-1 text-start">{{ $message }}</p>
            @enderror
        </div>
    </div>
</div>


@once
    @push('css-links')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    @endpush
    @push('css-scripts')
        <style>
            .select2-container .select2-selection--single{
                height: 40px !important;
                border-color: #d2d6dc !important;
                color: inherit !important;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered,
            .select2-container--default .select2-selection--multiple .select2-selection__rendered{
                padding-top: 5px;
            }
            .select2-container--default .select2-selection--single .select2-selection__placeholder,
            .select2-container--default .select2-selection--multiple .select2-search select2-search--inline .select2-selection__placeholder{
                color: inherit !important;
            }
        </style>
    @endpush
    @push('js-links')
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    @endpush
    @push('js-scripts')
        <script>
            function renderSelect2FromDataSource(id,route,dir,placeholder,dataFilter,customParams=[]){
                $("#"+id).select2({
                    ajax:{
                        url:route,
                        data:function(params){
                            var term = params.term;
                            var custom=customParams;
                            var query={
                                term:params.term,
                                page: params.page,
                                perpage:10
                            };
                            for(var item in custom)
                            {
                                query[item]=custom[item];
                            }
                            return query;
                        },
                        processResults:function(response,params){
                            params.page = params.page || 1;
                            // console.log(data.availableBranchManagers.data);
                            var data=$.map(response.data,dataFilter);
                            return {
                                results:data,
                                pagination: {
                                    more: params.page * 10 < response.total
                                }
                            };
                        }
                    },
                    language:'{{ app()->getLocale() }}',
                    dir:dir,
                    placeholder: placeholder,
                    allowClear:true
                });
            };
            function renderSelect2(id,dir){
                $("#"+id).select2({
                    language:'{{ app()->getLocale() }}',
                    dir:dir,
                });
            };
        </script>
    @endpush
@endonce

@push('js-scripts')
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('message.processed', (message, component) => {
                @if($hasDataSource)
                renderSelect2FromDataSource('{{ $id }}','{{ $dataSourceRoute }}','{{ localClass() }}','{{ $placeholder }}',function(obj){
                    obj.text= {{ $textExp }};
                    return obj
                });
                @else
                renderSelect2('{{ $id }}','{{ localClass() }}');
                @endif
            });
        });
    </script>
@endpush

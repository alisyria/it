<div>
    <label class="inline-flex items-center mt-3">
        <input type="checkbox" {{ $attributes->merge(['class'=>"form-checkbox h-5 w-5 text-blue-600"]) }} name="{{ $name }}" id="{{ $id }}" @if($lazyUpdate) wire:model.lazy @else wire:model.defer @endif="{{ $liveName }}" value="{{$value}}">
        <span class="ms-2 text-gray-700">{{ $label }}</span>
    </label>
</div>

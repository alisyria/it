<div>
    <x-components.tab active="{{ $defaultLang }}">
        <x-slot name="links">
            @foreach($langs() as $lang=>$langName)
                <x-components.tab-link :id="$lang" :label="$langName">
                    <x-slot name="class">
                        text-xs
                    </x-slot>
                </x-components.tab-link>
            @endforeach
        </x-slot>
        <x-slot name="panes">
            @foreach($langs() as $lang=>$langName)
                <x-components.tab-pane :id="$lang">
                    <x-slot name="content">
                        <x-form.input :type="$type" name="{{ $name }}[{{ $lang }}]" :live-name="$liveName.'.'.$lang"   :is-rtl="isLangRtl($lang)"  without-errors="true"/>
                    </x-slot>
                </x-components.tab-pane>
            @endforeach
        </x-slot>
    </x-components.tab>
    @if($errors->has($liveName.".*"))
        <ul>
        @foreach ($errors->get($liveName.".*") as $messages)
            <li class="text-xs text-red-600 px-1 text-start">{{ data_get($messages,'0') }}</li>
        @endforeach
        </ul>
    @endif
</div>

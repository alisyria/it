<div>
    <div wire:ignore x-data="{pond:null}" x-init="
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginImageExifOrientation,
            FilePondPluginImageValidateSize,
            // FilePondPluginImageEdit
        );

        pond=FilePond.create(
            document.querySelector('#{{ $id }}'),{
                server: {
                    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                        @this.upload('{{ $liveName }}', file, load, error, progress);
                    },
                    revert: (filename, load) => {
                        @this.removeUpload('{{ $liveName }}', filename, load);
                    },
                }
            }
        );"      x-cloak
         x-on:file-pond-clear.window="
         const id = $wire && $wire.__instance.id;
         const sentId = $event.detail.id;
         if (id && (sentId !== id)) {
            return;
         }
         @if ($maxFiles>1)
             pond.getFiles().forEach(file => pond.removeFile(file.id));
@else
             pond.removeFile();
@endif
             ">
        <input type="file"
               id="{{ $id }}"
               class="filepond"
               name="{{ $name }}[]"
               @if($maxFiles>1) multiple @endif
               {{--           data-allow-reorder="true"--}}
               data-max-file-size="{{ $maxFileSize }}"
               data-max-files="{{ $maxFiles }}"
               wire:model="{{ $liveName }}"
        >
    </div>
    @error($liveName)
    <p class="text-xs text-red-600 px-1">{{ $message }}</p>
    @enderror
    @error($liveName.'.*')
    <p class="text-xs text-red-600 px-1">{{ $message }}</p>
    @enderror
</div>

@push('js-scripts')
    <script>

    </script>
@endpush

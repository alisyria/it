
<x-form.input type="text" id="{{ $id }}" name="{{ $name }}" live-name="{{ $liveName }}" :model="null" class="ltr" autocomplete="off" placeholder="{{ $placeholder }}"/>

@once
    @push('css-links')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css">
    @endpush

    @push('css-scripts')
        <style>
            .daterangepicker {
                direction: ltr;
            }
        </style>
    @endpush

    @push('js-links')
        <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @endpush

    @push('js-scripts')
        <script>
            function initiateDateRangePicker(id,liveName,timePicker,timePickerIncrement,minDate,maxDate)
            {
                var element=$('#'+id);
                element.daterangepicker({
                    autoApply: true,
                    showDropdowns: true,
                    timePicker: timePicker,
                    timePickerIncrement: timePickerIncrement,
                    minDate:minDate,
                    maxDate:maxDate,
                    autoUpdateInput:false,
                    locale: {
                        format: 'YYYY-MM-DD h:mm A',
                        cancelLabel: '{{ __j('Clear') }}'
                    }
                });

                element.on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD h:mm A') + ' - ' + picker.endDate.format('YYYY-MM-DD h:mm A'));
                });

                element.on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                    @this.set(liveName,null);
                });
                element.on('apply.daterangepicker', function(ev, picker) {
                    @this.set(liveName,picker.startDate.format('YYYY-MM-DD h:mm A')+' - '+picker.endDate.format('YYYY-MM-DD h:mm A'));
                });
            }
        </script>
    @endpush
@endonce

@push('js-scripts')
    <script>
        initiateDateRangePicker(@json($id),@json($liveName),(@json($timePicker)==true),@json($timePickerIncrement),
            @json($minDate),@json($maxDate));
    </script>
@endpush

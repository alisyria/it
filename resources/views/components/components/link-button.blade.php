<a href="{{ $link }}" {{ $attributes->merge([
    'class'=>'px-1 py-1 font-medium leading-5 text-white transition-colors duration-150 bg-pink-600 border border-transparent rounded-md active:bg-pink-600 hover:bg-pink-700 focus:outline-none focus:shadow-outline-purple text-sm me-2'
]) }}>
    {{ $text }}
</a>

<li  :class='{"me-1":true,"-mb-px":active === @json($id),"{{ $attributes->get('class') }}":true}' wire:ignore>
    <a :class='{"bg-white inline-block py-2 px-4  font-semibold":true,"border-l border-t border-r rounded-t text-green-700":active === @json($id),"text-green-500":active !== @json($id),"hover:text-green-800":true,"{{$class ?? ""}}":true}' href="#" @click='active=@json($id)' >{{ $label }}</a>
</li>


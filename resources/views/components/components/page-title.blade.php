<h2 class="text-green-900 text-2xl font-semibold my-2 mb-4">
    {{ $slot }}
</h2>

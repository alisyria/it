<dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
    <ul class="border border-gray-200 rounded-md divide-y divide-gray-200">
        @foreach(\App\Models\AppLanguage::langsSelect() as $lang=>$langName)
            <li wire:key="{{ $loop->index }}" class="ps-3 pe-4 py-3 flex items-center justify-between text-sm">
                <div class="ms-4">
                    <h2 class="font-semibold text-green-700">{{ $langName }}</h2>
                    @php $text=$model->getTranslation($attribute,$lang,false); @endphp
                    @if(!$isHtml)
                        {{ $text }}
                    @else
                        {!! $text !!}
                    @endif
                </div>
            </li>
        @endforeach
    </ul>
</dd>

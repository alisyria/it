<header class="z-10 py-2 bg-white shadow-md dark:bg-gray-800  bg-green-800 opacity-90">
    <div
        class="container flex items-center justify-between h-full px-6 mx-auto text-green-600 dark:text-green-300"
    >
        <!-- Mobile hamburger -->
        <button
            class="p-1 me-5 -ms-1 rounded-md md:hidden focus:outline-none focus:shadow-outline-green"
            @click="toggleSideMenu"
            aria-label="Menu"
        >
            <svg
                class="w-6 h-6"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
            >
                <path
                    fill-rule="evenodd"
                    d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                    clip-rule="evenodd"
                ></path>
            </svg>
        </button>
        <!-- Search input -->
        <div class="flex justify-center flex-1 lg:me-32">

        </div>
        <ul class="flex items-center flex-shrink-0 space-x-6">
            <!-- Theme toggler -->
            <li class="relative">
                <button
                    class="align-middle rounded-full focus:shadow-outline-purple focus:outline-none"
                    @click="toggleProfileMenu"
                    @keydown.escape="closeProfileMenu"
                    aria-label="Account"
                    aria-haspopup="true"
                >
                    <img
                        class="object-cover w-8 h-8 rounded-full"
                        src="{{ mediaConversionUrl(user()->avatar,\App\Enums\ImageConversion::SMALL) ?? user()->getFirstMediaUrl(\App\Models\User::MEDIA_AVATAR) }}"
                        alt=""
                        aria-hidden="true"
                    />
                </button>
                <template x-if="isProfileMenuOpen">
                    <ul
                        x-transition:leave="transition ease-in duration-150"
                        x-transition:leave-start="opacity-100"
                        x-transition:leave-end="opacity-0"
                        @click.away="closeProfileMenu"
                        @keydown.escape="closeProfileMenu"
                        class="absolute end-0 w-56 p-2 mt-2 space-y-2 text-gray-600 bg-white border border-gray-100 rounded-md shadow-md dark:border-gray-700 dark:text-gray-300 dark:bg-gray-700"
                        aria-label="submenu"
                    >
                        <li class="flex px-0">
                            <span class="text-sm border-b border-gray-200 w-full pb-1">{{ adminUser()->full_name }}</span>
                        </li>
                        <li class="flex">
                            <a
                                class="inline-flex items-center w-full px-2 py-1 text-sm font-semibold transition-colors duration-150 rounded-md hover:bg-gray-100 hover:text-gray-800 dark:hover:bg-gray-800 dark:hover:text-gray-200"
                                href="{{ route('admin.profile.index') }}"
                            >
                                <x-icons.css-profile class="w-4 h-4 me-3"/>
                                <span>@langj('Profile')</span>
                            </a>
                        </li>
{{--                        @can(\App\Enums\Permission::MANAGE_SETTINGS)--}}
{{--                        <li class="flex">--}}
{{--                            <a--}}
{{--                                class="inline-flex items-center w-full px-2 py-1 text-sm font-semibold transition-colors duration-150 rounded-md hover:bg-gray-100 hover:text-gray-800 dark:hover:bg-gray-800 dark:hover:text-gray-200"--}}
{{--                                href="{{ route('admin.settings.others') }}"--}}
{{--                            >--}}
{{--                                <svg--}}
{{--                                    class="w-4 h-4 me-3"--}}
{{--                                    aria-hidden="true"--}}
{{--                                    fill="none"--}}
{{--                                    stroke-linecap="round"--}}
{{--                                    stroke-linejoin="round"--}}
{{--                                    stroke-width="2"--}}
{{--                                    viewBox="0 0 24 24"--}}
{{--                                    stroke="currentColor"--}}
{{--                                >--}}
{{--                                    <path--}}
{{--                                        d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"--}}
{{--                                    ></path>--}}
{{--                                    <path d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>--}}
{{--                                </svg>--}}
{{--                                <span>@langj('Settings')</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        @endcan--}}
                        <li>
                            <a  href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="inline-flex items-center w-full px-2 py-1 text-sm font-semibold transition-colors duration-150 rounded-md hover:bg-gray-100 hover:text-gray-800 dark:hover:bg-gray-800 dark:hover:text-gray-200">
                                <svg
                                    class="w-4 h-4 me-3"
                                    aria-hidden="true"
                                    fill="none"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1"
                                    ></path>
                                </svg>
                                <span>@langj('Log out')</span>
                            </a>
                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </template>
            </li>
        </ul>
    </div>
</header>

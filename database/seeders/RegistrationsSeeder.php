<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Registration;

class RegistrationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $num=(int)$this->command->ask("how many registrations do you need?",10);
        Registration::factory()->count($num)->create();
        $this->command->line("{$num} registrations have been created successfully");
    }
}

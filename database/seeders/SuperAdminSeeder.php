<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Models\Employee;
use Illuminate\Database\Seeder;
use App\Models\User;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::getSuperAdmin())
        {
            return;
        }
        User::factory()->superAdmin()->create()->assignRole(Role::SUPER_ADMIN);
    }
}

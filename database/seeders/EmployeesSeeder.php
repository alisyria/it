<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Models\Employee;
use Illuminate\Database\Seeder;
use App\Enums\Role as RoleEnum;
use App\Models\User;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numEmployees=(int)$this->command->ask("How many employees do you need?", 30);

        Employee::factory()->count($numEmployees)->create()
            ->each(function(Employee $employee){
                $employee->user->assignRole(Role::DEVICES_MANAGER);
            });

        $this->command->line("{$numEmployees} employees has been created successfully");
    }
}

<?php

namespace Database\Seeders;

use App\Enums\Disk;
use Illuminate\Database\Seeder;
use App\Models\Setting;
use Illuminate\Support\Facades\Storage;
use Nahid\JsonQ\Jsonq;
use App\Enums\SettingDataType;
use App\Enums\SettingType;
use Illuminate\Support\Facades\Cache;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonq=new Jsonq(Storage::disk('local')->path('fake/data/settings.json'));
        foreach ($jsonq->get() as $item)
        {
            if(!SettingDataType::hasValue($item->setting_data_type_id) || !SettingType::hasValue($item->setting_type_id))
            {
                $this->command->error("setting_data_type: {$item->setting_data_type_id} or setting_type: {$item->setting_type_id} not found");
            }

            $setting=Setting::firstOrCreate(['key'=>$item->key],array_except($item->toArray(),['meta']));
//            if($setting->setting_data_type_id=='file.image' && $setting->wasRecentlyCreated)
//            {
//                $setting->addMedia(Storage::disk(Disk::FAKE)->path($item['meta']['path']))
//                    ->preservingOriginal()
//                    ->toMediaCollection($setting->setting_type_id);
//            }
//            if($setting->setting_data_type_id=='file.pdf' && $setting->wasRecentlyCreated)
//            {
//                $setting->addMedia(Storage::disk(Disk::FAKE)->path($item['meta']['path']))
//                    ->preservingOriginal()
//                    ->toMediaCollection($setting->setting_data_type_id);
//            }
        }


        Cache::forget('settings.app');
    }
}

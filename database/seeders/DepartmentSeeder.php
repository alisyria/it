<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    public $departments=[
        'مكتب المدير',
        'المكتب الفني',
        'التخطيط والمتابعة',
        'الدراسات',
        'الطرق',
        'الأبنية والصرف الصحي',
        'الطبوغرافيا',
        'التخطيط العمراني',
        'المعلوماتية',
        'المكتب الإداري',
        'شؤون العاملين',
        'الشؤون الإدارية',
        'الشؤون القانونية',
        'الشؤون المالية',
        'التأمين الصحي',
        'مكتب طرطوس',
        'مكتب بانياس',
        'مكتب الشيخ بدر',
        'مكتب الدريكيش',
        'مكتب صافيتا',
        'مكتب القدموس',
        'الآليات'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->departments as $department)
        {
            Department::firstOrCreate([
                'name'=>$department,
            ],[
                'name'=>$department,
            ]);
        }
    }
}

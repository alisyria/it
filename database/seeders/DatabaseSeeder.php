<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Laravel\Telescope\Telescope;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Telescope::stopRecording();
        if(app()->environment('production'))
        {
            return;
        }
        if($this->command->confirm('Do you wish to fresh migration before seeding, it will clear all old data ?'))
        {
            $this->command->call('migrate:fresh');
            $this->command->line('Data cleared, starting from blank database.');
        }
        if($this->command->confirm('Do you wish to clear media disk?'))
        {
            if(!app()->environment('production'))
            {
                Storage::disk('public')->deleteDirectory('media');
            }
            $this->command->line('Media Disk cleared successfully.');
        }
        $this->call(AppLanguagesSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(SuperAdminSeeder::class);
        $this->call(EmployeesSeeder::class);
        $this->call(OwnerSeeder::class);
        $this->call(DeviceTypeSeeder::class);
        $this->call(DeviceSeeder::class);
        $this->call(SectionSeeder::class);

        $this->command->call('ide-helper:models',[
            '-N'=>true
        ]);
        Telescope::startRecording();
    }
}

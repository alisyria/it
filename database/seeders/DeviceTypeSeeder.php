<?php

namespace Database\Seeders;

use App\Models\DeviceType;
use Illuminate\Database\Seeder;
use Nahid\JsonQ\Jsonq;
use Storage;

class DeviceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonq=new Jsonq(Storage::disk('local')->path('fake/data/device-types.json'));
        foreach ($jsonq->get() as $deviceTypeItem)
        {
            $deviceType=DeviceType::firstOrCreate(['name'=>$deviceTypeItem->name],['name'=>$deviceTypeItem->name]);
            foreach ($deviceTypeItem->properties as $propertyItem)
            {
                $propertyItem=(object)$propertyItem;
                $property=$deviceType->properties()->updateOrCreate([
                    'name'=>$propertyItem->name
                ],[
                    'field_type'=>$propertyItem->field_type,
                ]);
                if(optional($propertyItem)->options)
                {
                    foreach ($propertyItem->options as $optionItem)
                    {
                        $optionItem=(object)$optionItem;
                        $option=$property->options()->firstOrCreate([
                            'text'=>$optionItem->text,
                        ]);
                    }
                }
            }
        }
    }
}

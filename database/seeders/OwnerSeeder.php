<?php

namespace Database\Seeders;

use App\Models\Owner;
use Illuminate\Database\Seeder;

class OwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numItems=(int)$this->command->ask("How many Owners do you need?", 30);
        Owner::factory()->count($numItems)->create();
        $this->command->line("{$numItems} Owners has been created successfully");
    }
}

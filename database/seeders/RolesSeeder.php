<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Permission;
use App\Enums\AuthGuard;
use Illuminate\Database\Seeder;
use App\Enums\Role as RoleEnum;
use App\Enums\Permission as PermissionEnum;
use App\Enums\CompanyRole as CompanyRoleEnum;
use App\Enums\CompanyPermission as CompanyPermissionEnum;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $roleSuperAdmin=Role::updateOrCreate([
            'name'=>RoleEnum::SUPER_ADMIN,
            'guard_name'=>AuthGuard::ADMIN
        ],[
            'key'=>RoleEnum::SUPER_ADMIN,
            'description'=>[
                "ar"=>"يعطي هذا الدور للموظف إمكانية التحكم الكامل والشامل بالنظام وهذا الدور يعطي للمدير العام",
                "en"=>"This role gives the employee full and comprehensive control of the system and this role given to the higher administrator",
            ],
        ]);
        $roleSuperAdmin->syncPermissions(PermissionEnum::asArray());

        $roleGeneralManager=Role::updateOrCreate([
            'name'=>RoleEnum::DEVICES_MANAGER,
            'guard_name'=>AuthGuard::ADMIN,
        ],[
            'key'=>RoleEnum::DEVICES_MANAGER,
            'description'=>[
                "ar"=>"يعطي هذا الدور للموظف إمكانية إدارة أجهزة الحواسيب وملحقاتها لدى المديرية",
                "en"=>"يعطي هذا الدور للموظف إمكانية إدارة أجهزة الحواسيب وملحقاتها لدى المديرية",
            ],
        ]);
        $roleGeneralManager->syncPermissions([PermissionEnum::MANAGE_DEVICES]);
//
//        //Company
//        $comapnyRoleSuperAdmin=Role::updateOrCreate([
//            'name'=>CompanyRoleEnum::SUPER_ADMIN,
//            'guard_name'=>AuthGuard::COMPANY
//        ],[
//            'key'=>CompanyRoleEnum::SUPER_ADMIN,
//            'description'=>[
//                "ar"=>"يعطي هذا الدور للموظف إمكانية التحكم الكامل والشامل بالنظام وهذا الدور يعطي للمدير العام",
//                "en"=>"This role gives the employee full and comprehensive control of the system and this role given to the higher administrator",
//            ],
//        ]);
//        $comapnyRoleSuperAdmin->syncPermissions(CompanyPermissionEnum::asArray());
//
//        $comapnyRoleGeneralManager=Role::updateOrCreate([
//            'name'=>CompanyRoleEnum::GENERAL_MANAGER,
//            'guard_name'=>AuthGuard::COMPANY
//        ],[
//            'key'=>CompanyRoleEnum::GENERAL_MANAGER,
//            'description'=>[
//                "ar"=>"يعطي هذا الدور للموظف إمكانية التحكم الكامل والشامل بالنظام وهذا الدور يعطي للمدير العام",
//                "en"=>"This role gives the employee full and comprehensive control of the system and this role given to the higher administrator",
//            ],
//        ]);
//        $comapnyRoleGeneralManager->syncPermissions(CompanyPermissionEnum::toGeneralManager());
    }
}

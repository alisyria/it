<?php

namespace Database\Seeders;

use App\Enums\AuthGuard;
use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Enums\Permission as PermissionEnum;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        foreach (PermissionEnum::asArray() as $permissionName)
        {
            Permission::updateOrCreate([
                'name'=>$permissionName,
                'guard_name'=>AuthGuard::ADMIN,
            ],[

            ]);
        }

    }
}

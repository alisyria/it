<?php

namespace Database\Seeders;

use App\Models\AppLanguage;
use Illuminate\Database\Seeder;

class AppLanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('laravellocalization.supportedLocales') as $lang=>$value)
        {
            AppLanguage::updateOrCreate([
                'id'=>$lang
            ],[
                'name'=>$value['name'],
                'desc'=>[
                    'en'=>$value['name'],
                ],
                'native'=>$value['native'],
                'script'=> data_get($value,'script'),
                'regional'=>$value['regional'],
                'is_supported'=>data_get($value,'supported',0),
                'is_default'=>data_get($value,'default',0),
            ]);
        }
    }
}

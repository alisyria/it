<?php

namespace Database\Seeders;

use App\Models\Device;
use Illuminate\Database\Seeder;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numItems=(int)$this->command->ask("How many Devices do you need?", 30);
        Device::factory()->count($numItems)->create();
        $this->command->line("{$numItems} Devices has been created successfully");
    }
}

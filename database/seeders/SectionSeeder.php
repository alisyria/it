<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Nahid\JsonQ\Jsonq;

class SectionSeeder extends Seeder
{
    public function run()
    {
        $jsonq=new Jsonq(Storage::disk('local')->path('fake/data/sections.json'));
        foreach ($jsonq->get() as $sectionItem)
        {
            $section = Section::firstOrCreate(['title' => $sectionItem->title],[]);
            foreach ($sectionItem->items as $itemItem)
            {
                $section->items()->updateOrCreate(
                    Arr::only($itemItem, 'text'),
                    $itemItem
                );
            }
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Notification;

class NotificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numGeneral=(int)$this->command->ask("How many General Notifications do you need?", 30);
        Notification::factory()->count($numGeneral)->create();
        $this->command->line("{$numGeneral} General Notification has been created successfully");
    }
}

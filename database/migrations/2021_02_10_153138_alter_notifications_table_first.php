<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNotificationsTableFirst extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->string('target',30)->nullable()->after('key');
            $table->json('title')->nullable()->after('type');
            $table->string('vendor_type',30)->nullable()->after('text');
            $table->nullableMorphs('senderable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropColumn([
                'target','title','vendor_type'
            ]);
            $table->dropMorphs('senderable');
        });
    }
}

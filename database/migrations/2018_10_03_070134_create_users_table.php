<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',50)->nullable();
            $table->string('middle_name',50)->nullable();
            $table->string('last_name',50)->nullable();
            $table->string('full_name',255)->nullable();
            $table->string('username',255)->nullable();
            $table->string('password')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile',50)->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->timestamp('blocked_at')->nullable();
            $table->string('username_type',50);
            $table->unsignedInteger('user_type');
            $table->json('extra');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['username','username_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

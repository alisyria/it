<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->id();
            $table->uuid('instance_uuid')->unique();
            $table->string('reg_id',500)->nullable();
            $table->string('device_id',50)->nullable();
            $table->string('os_id',50)->nullable();
            $table->unsignedTinyInteger('version');
            $table->decimal('ios_version',3,2)->default(1);
            $table->boolean('is_notifiable')->default(1);
            $table->string('lang',2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}

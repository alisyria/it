<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_languages', function (Blueprint $table) {
            $table->string('id',30)->primary();
            $table->string('name','100');
            $table->json('desc')->nullable();
            $table->string('native','100');
            $table->string('script','100');
            $table->string('regional','50')->nullable();
            $table->boolean('is_supported')->default(0);
            $table->boolean('is_default')->default(0);
            $table->unsignedInteger('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_languages');
    }
}

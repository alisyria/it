<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('state', 30);
            $table->string('title', 500)
                ->nullable();
            $table->text('description')
                ->nullable();
            $table->timestamp('state_updated_at')
                ->nullable();
            $table->timestamp('requested_at')
                ->nullable();
            $table->foreignId('device_id')
                ->constrained();
            $table->foreignId('employee_id')
                ->constrained();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

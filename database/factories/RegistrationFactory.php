<?php

namespace Database\Factories;

use App\Enums\ENV;
use App\Models\AppLanguage;
use App\Models\Registration;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RegistrationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Registration::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'instance_uuid'=>(string) Str::uuid(),
            'reg_id'=>app()->environment(ENV::LOCAL) ? (string) Str::uuid():null,
            'device_id'=>(string) Str::uuid(),
            'os_id'=>$this->faker->randomElement(['android','ios']),
            'version'=>1,
            'is_notifiable'=>$this->faker->boolean(80),
            'lang'=>$this->faker->randomElement(AppLanguage::langs())
        ];
    }
}

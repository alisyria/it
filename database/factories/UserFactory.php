<?php

namespace Database\Factories;

use App\Enums\CompanyRole;
use App\Enums\UsernameType;
use App\Enums\UserType;
use App\Models\AppLanguage;
use App\Models\Currency;
use App\Models\Location;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'middle_name'=>null,
            'last_name'=>$this->faker->lastName,
            'email' => $this->faker->optional()->email,
            'mobile'=> $this->faker->optional()->phoneNumber,
            'username'=>$this->faker->unique()->userName,
            'activated_at'=> now(),
            'verified_at'=> now(),
            'username_type'=> UsernameType::USERNAME,
            'user_type'=> $this->faker->randomElement(UserType::asArray()),
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'remember_token' => str_random(10),
            'extra'=>[
                'last_seen_notifications'=>null,
            ],
        ];
    }

    public function superAdmin()
    {
        return $this->state(function (array $attributes){

            return [
                'username'=>env('SUPER_ADMIN_USERNAME'),
                'password'=>bcrypt(env('SUPER_ADMIN_PASS')),
                'username_type'=> UsernameType::USERNAME,
                'user_type'=>UserType::SUPER_ADMIN,
            ];
        });
    }
    public function employee()
    {
        return $this->state(function (array $attributes){

            return [
                'user_type'=>UserType::EMPLOYEE
            ];
        });
    }

    public function configure()
    {
        return $this->afterCreating(function(User $user){
            $usersService=app(UserRepository::class);
            $usersService->storeAvatarDefault($user,$user->name);
            $userType=UserType::fromValue($user->user_type);
        });
    }
}

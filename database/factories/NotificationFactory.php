<?php

namespace Database\Factories;

use App\Enums\NotificationStatistic;
use App\Models\AppLanguage;
use App\Models\Customer;
use App\Models\Notification;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Enums\NotificationKey;
use Illuminate\Support\Str;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'=>(string) Str::uuid(),
            'key'=>NotificationKey::GENERAL,
            'title'=>function(){
                $text=[];
                foreach (AppLanguage::langs() as $lang)
                {
                    $text[$lang]=$this->faker->sentence(6);
                }
                return $text;
            },
            'text'=>function(){
                $text=[];
                foreach (AppLanguage::langs() as $lang)
                {
                    $text[$lang]=$this->faker->realText(200);
                }
                return $text;
            },
            'data'=>[],
            'statistics'=>function(){
                $successCount=rand(10000,30000);
                $failedCount=rand(0,100);
                $totalCount=$successCount+$failedCount;
                return [
                    NotificationStatistic::SUCCESS_DEVICES=>$successCount,
                    NotificationStatistic::FAILED_DEVICES=>$failedCount,
                    NotificationStatistic::TOTAL_DEVICES=>$totalCount,
                ];
            },
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function(Notification $notification){
            $notification->users()->sync(Shop::pluck('user_id')->toArray());
        });
    }
}

<?php

namespace Database\Factories;

use App\Enums\DeviceStatus;
use App\Enums\FieldType;
use App\Models\Device;
use App\Models\DeviceType;
use App\Models\Owner;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeviceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Device::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'state'=>$this->faker->randomElement(DeviceStatus::asArray()),
            'serial_number'=>$this->faker->unique()->bankAccountNumber,
            'model'=>$this->faker->word,
            'manufacturer'=>$this->faker->company,
            'device_type_id'=>DeviceType::inRandomOrder()->first(),
            'owner_id'=>Owner::factory(),
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Device $device){
            foreach ($device->deviceType->properties->load('options') as $property)
            {
                $optionId=$value=null;
                if($property->field_type->is(FieldType::TEXT_SINGLE_LINE))
                {
                    if($property->name=='MAC')
                    {
                        $value=$this->faker->macAddress;
                    }
                    else
                    {
                        $value=$this->faker->sentence(3);
                    }
                }
                elseif($property->field_type->is(FieldType::SINGLE_OPTION))
                {
                    $optionId=$this->faker->randomElement($property->options->pluck('id')->toArray());
                }
                $device->deviceProperties()->create([
                    'property_id'=>$property->id,
                    'option_id'=>$optionId,
                    'value'=>$value,
                ]);
            }
        });
    }
}

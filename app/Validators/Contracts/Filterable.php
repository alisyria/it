<?php

namespace App\Validators\Contracts;


interface Filterable
{
    public function getFilters(string $field=null):array;
}
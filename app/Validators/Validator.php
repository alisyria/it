<?php

namespace App\Validators;


use App\Enums\Operation;
use App\Enums\Platform;
use Illuminate\Support\Arr;

abstract class Validator
{
    protected string $translationBase;
    public string $method;
    public string $platform;

    public function __construct(string $method,string $platform=null)
    {
        $this->method=$method;
        $this->platform=$platform ?? Platform::WEB;
    }

    abstract protected function getGeneralRules(string $field=null):array;
    protected function getMobileRules(string $field=null):array
    {
        return [];
    }
    protected function getWebRules(string $field=null):array
    {
        return [];
    }
    protected function getCreateRules(string $field=null):array
    {
        return [];
    }
    protected function getUpdateRules(string $field=null):array
    {
        return [];
    }

    final public function getRules(string $field=null):array
    {
        $rules=$this->getGeneralRules();
        $platformRules=$this->isMobilePlatform()?$this->getMobileRules()
            :$this->getWebRules();
        $methodRules=$this->isCreateMethod()?$this->getCreateRules()
            :$this->getUpdateRules();

        return array_merge($rules,$platformRules,$methodRules);
    }

    final public function getAttributes():array
    {
        $attributes=__($this->translationBase);
        $appendedAttributes=$this->appendAttributes();

        if(filled($appendedAttributes))
        {
            $attributes=array_merge($attributes,$appendedAttributes);
        }
        return Arr::dot($attributes);
    }
    public function appendAttributes():array
    {
        return [];
    }
    public function getMessages(string $field=null):array
    {
        return [];
    }
    protected function isMobilePlatform():bool
    {
        return $this->platform==Platform::MOBILE;
    }
    protected function isWebPlatform():bool
    {
        return $this->platform==Platform::WEB;
    }
    protected function isCreateMethod():bool
    {
        return $this->method==Operation::CREATE;
    }
    protected function isUpdateMethod():bool
    {
        return $this->method==Operation::UPDATE;
    }
    protected function requiredForCreateRule(bool $otherwiseSometimes=false):string
    {
        if($this->isCreateMethod())
        {
            return 'required';
        }
        return $otherwiseSometimes?'sometimes':'nullable';
    }
    protected function requiredIfRule($value1,$value2):string
    {
        return $value1==$value2?'required':'nullable';
    }
    protected function requiredIfInRule($value1,array $haystack):string
    {
        return in_array($value1,$haystack)?'required':'nullable';
    }
    protected function httpMethod():string
    {
        $method='POST';
        if($this->method==Operation::UPDATE)
        {
            $method='PUT';
        }

        return $method;
    }
}

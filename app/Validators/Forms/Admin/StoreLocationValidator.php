<?php


namespace App\Validators\Forms\Admin;


use App\Models\AppLanguage;
use App\Models\Governorate;
use App\Models\Region;
use App\Rules\ModelExistRule;
use App\Validators\Contracts\Filterable;
use App\Validators\Validator;

class StoreLocationValidator extends Validator implements Filterable
{
    protected string $translationBase='fields.admin.location';


    protected function getGeneralRules(string $field = null): array
    {
        $rules= [
            'location.governorate_id'=>[
                'bail','required',new ModelExistRule($this->translationBase,Governorate::class)
            ],
            'location.region_id'=>[
                'bail','required',new ModelExistRule($this->translationBase,Region::class)
            ],
            'location.address'=>[
                'bail','required','array'
            ],
//            'location.is_main'=>[
//                'bail','required','boolean'
//            ],
        ];

        $defaultLocal=AppLanguage::getDefaultLocal();
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $rules["location.address.$lang->id"]=[$lang->id==$defaultLocal?'required':'nullable','min:2','max:500'];
        }

        return $rules;
    }
    public function appendAttributes(): array
    {
        $attributes=[];
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $attributes["location.address.$lang->id"]=__($this->translationBase.'.location.address')."($lang->name)";
        }
        return $attributes;
    }

    public function getFilters(string $field = null): array
    {
        return [
            'location.is_main'=>'boolean'
        ];
    }
}

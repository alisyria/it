<?php


namespace App\Validators\Forms\Admin;


use App\Enums\NotificationTarget;
use App\Enums\VendorType;
use App\Models\AppLanguage;
use App\Models\Governorate;
use App\Models\Region;
use App\Rules\FileImageRule;
use App\Rules\ModelExistRule;
use App\Validators\Validator;
use BenSampo\Enum\Rules\EnumValue;

class StoreNotificationValidator extends Validator
{
    protected string $translationBase='fields.admin.notification';

    public ?int $governorateID;

    public function __construct(string $method, string $platform = null,int $governorateID=null)
    {
        parent::__construct($method, $platform);
        $this->governorateID=$governorateID;
    }

    protected function getGeneralRules(string $field = null): array
    {
        $rules= [
            'target'=>['nullable',new EnumValue(NotificationTarget::class)],
            'vendor_type'=>['nullable',new EnumValue(VendorType::class)],
            'title'=> ['required','array'],
            'text'=> ['required','array'],
            'governorate_id'=>['nullable',new ModelExistRule($this->translationBase,Governorate::class)],
            'region_id'=>['nullable',new ModelExistRule($this->translationBase,Region::class,'id',function($q){
                $q->where('governorate_id',$this->governorateID);
            })],
            'image'=> ['nullable',new FileImageRule($this->translationBase)],
        ];

        $defaultLocal=AppLanguage::getDefaultLocal();
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $rules["title.$lang->id"]=[$lang->id==$defaultLocal?'required':'nullable','min:2','max:200'];
            $rules["text.$lang->id"]=[$lang->id==$defaultLocal?'required':'nullable','min:2',];
        }

        return $rules;
    }
    public function appendAttributes(): array
    {
        $attributes=[];
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $attributes["title.$lang->id"]=__($this->translationBase.'.title')."($lang->name)";
            $attributes["text.$lang->id"]=__($this->translationBase.'.text')."($lang->name)";
        }
        return $attributes;
    }
}

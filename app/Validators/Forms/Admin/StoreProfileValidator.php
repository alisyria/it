<?php


namespace App\Validators\Forms\Admin;


use App\Enums\UsernameType;
use App\Models\User;
use App\Rules\Custom\ValidCurrentPasswordRule;
use App\Rules\FileImageRule;
use App\Rules\MobileRule;
use App\Rules\UsernameRule;
use App\Validators\Validator;
use Illuminate\Validation\Rule;

class StoreProfileValidator extends Validator
{
    protected string $translationBase='fields.admin.profile';

    public ?string $usernameTypeId=null;
    public ?User $user;

    public function __construct(string $method, string $platform = null,?string $usernameTypeId,
                                ?User $user)
    {
        parent::__construct($method, $platform);
        $this->usernameTypeId=$usernameTypeId;
        $this->user=$user;
    }

    protected function getGeneralRules(string $field = null): array
    {
        $rules= [
            'first_name'=>[
                'bail','required','max:50'
            ],
            'last_name'=>[
                'bail','required','max:50'
            ],
            'email'=>[
                'bail','nullable','email'
            ],
            'mobile'=>[
                'bail','nullable',new MobileRule($this->translationBase)
            ],
            'username_type'=>[
                'bail','required',Rule::in([
                    UsernameType::Mobile,UsernameType::Email
                ]),
            ],
            'username'=>[
                'bail','required',
            ],
            'new_password'=>[
                'bail','nullable','confirmed','min:6'
            ],
            'current_password'=>[
                'bail','required',new ValidCurrentPasswordRule($this->user),
            ],
            'image'=>[
                'nullable',new FileImageRule($this->translationBase),
            ],
        ];

        if(filled($this->usernameTypeId))
        {
            $rules['username'][]=new UsernameRule($this->translationBase,
                $this->usernameTypeId,[],$this->httpMethod(),optional($this->user)->id);
        }

        return $rules;
    }
}

<?php


namespace App\Validators\Forms\Admin;


use App\Enums\UsernameType;
use App\Rules\FileImageRule;
use App\Rules\MobileRule;
use App\Rules\UsernameRule;
use App\Validators\Validator;
use Illuminate\Validation\Rule;

class StoreEmployeeValidator extends Validator
{
    protected string $translationBase='fields.admin.employee';

    public ?string $usernameTypeId=null;
    public ?int $userId;

    public function __construct(string $method, string $platform = null,?string $usernameTypeId,
                                ?int $userId)
    {
        parent::__construct($method, $platform);
        $this->usernameTypeId=$usernameTypeId;
        $this->userId=$userId;
    }

    protected function getGeneralRules(string $field = null): array
    {
        $rules= [
            'first_name'=>[
                'bail','required','max:50'
            ],
            'last_name'=>[
                'bail','required','max:50'
            ],
            'email'=>[
                'bail','nullable','email'
            ],
            'mobile'=>[
                'bail','nullable',new MobileRule($this->translationBase)
            ],
//            'username_type'=>[
//                'bail','required',Rule::in([
//                    UsernameType::USERNAME
//                ]),
//            ],
            'username'=>[
                'bail','required',
            ],
            'password'=>[
                'bail',$this->requiredForCreateRule(),'min:6'
            ],
            'image'=>[
                'nullable',new FileImageRule($this->translationBase),
            ],
            'roles'=>[
                'required','array'
            ],
        ];

        if(filled($this->usernameTypeId))
        {
            $rules['username'][]=new UsernameRule($this->translationBase,
                $this->usernameTypeId,[],$this->httpMethod(),$this->userId);
        }

        return $rules;
    }
}

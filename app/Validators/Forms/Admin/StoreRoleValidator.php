<?php


namespace App\Validators\Forms\Admin;


use App\Models\AppLanguage;
use App\Validators\Validator;
use Illuminate\Validation\Rule;

class StoreRoleValidator extends Validator
{
    protected string $translationBase='fields.admin.role';

    public ?int $roleId;

    public function __construct(string $method, string $platform = null,?int $roleId)
    {
        parent::__construct($method, $platform);
        $this->roleId=$roleId;
    }
    protected function getGeneralRules(string $field = null): array
    {
        $nameRule=Rule::unique('roles','name')->where(function($query) {
            $query->when($this->isUpdateMethod(), function ($query) {
                $query->where('id', '<>', $this->roleId);
            });
        });

        $rules= [
            'name'=>['bail','required','max:50',$nameRule],
            'description'=> ['bail','required','array'],
            'permissions'=> ['bail','required','array'],
        ];

        $defaultLocal=AppLanguage::getDefaultLocal();
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $rules["description.$lang->id"]=[$lang->id==$defaultLocal?'required':'nullable','min:2','max:1000'];
        }

        return $rules;
    }
    public function appendAttributes(): array
    {
        $attributes=[];
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $attributes["description.$lang->id"]=__($this->translationBase.'.description')."($lang->name)";
        }
        return $attributes;
    }
}

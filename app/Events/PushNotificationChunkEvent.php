<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use App\Models\Notification;
use App\Enums\NotificationStatistic;

class PushNotificationChunkEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id;
    public $total_devices;
    public $success_devices;
    public $failed_devices;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Notification $push)
    {
        $this->id=$push->id;
        $this->total_devices=$push->statistics->totalDevices;
        $this->success_devices=$push->statistics->successDevices;
        $this->failed_devices=$push->statistics->failedDevices;
    }
//    public function broadcastWith()
//    {
//        $push= $this->job->pushNotification;
//        return [
//            'id'=>$push->id,
//            'all_devices'=>$push->all_devices,
//            'success_devices'=>$push->success_devices,
//            'failed_devices'=>$push->failed_devices
//        ];
//    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('push-notifications');
    }
}

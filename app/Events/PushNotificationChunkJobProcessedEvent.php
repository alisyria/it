<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Jobs\SendPushNotificationsChunkJob;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class PushNotificationChunkJobProcessedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
        
    public $job;
    public $status;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SendPushNotificationsChunkJob $job,string $status)
    {
        $this->job=$job;
        $this->status=$status;
    }

//    public function broadcastWith()
//    {
//        $push= $this->job->pushNotification;
//        return [
//            'id'=>$push->id,
//            'all_devices'=>$push->all_devices,
//            'success_devices'=>$push->success_devices,
//            'failed_devices'=>$push->failed_devices
//        ];
//    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('ss');
    }
}

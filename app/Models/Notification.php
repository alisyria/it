<?php

namespace App\Models;

use App\Enums\NotificationTarget;
use App\Models\Contracts\CompanyConstrained;
use App\Models\Pivots\NotifiablePivot;
use App\Models\Traits\HasMediaTrait;
use App\Values\NotificationStats;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use App\Enums\NotificationKey;
use Spatie\Translatable\HasTranslations;

/**
 * @property NotificationKey $key
 * @property NotificationTarget $target
 * @property NotificationStats $statistics
 */
class Notification extends Model implements HasMedia
{
    use HasTranslations,HasFactory,HasMediaTrait;

    protected $keyType = 'string';
    public $incrementing=false;
    protected $table = 'notifications';
    protected $guarded = [
        'statistics','created_at','updated_at'
    ];
    protected $casts = [
        'key'=>NotificationKey::class,
        'target'=>NotificationTarget::class,
        'statistics'=>NotificationStats::class,
        'data' => 'array',
    ];
    public $translatable=[
        'title','text',
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope());
    }

    public function senderable()
    {
        return $this->morphTo();
    }
    public function users()
    {
        return $this->morphedByMany(User::class,'notifiable','notifiables')
            ->withPivot('read_at')
            ->using(NotifiablePivot::class);
    }
    public function registrations()
    {
        return $this->morphedByMany(Registration::class,'notifiable','notifiables')
            ->withPivot('read_at')
            ->using(NotifiablePivot::class);
    }

    public function getKeyDescAttribute()
    {
        return NotificationKey::getDescription($this->key);
    }

    public function scopeGeneral($query)
    {
        $query->whereIn('key',NotificationKey::toGeneral());
    }
    public function scopeGeneralCompany($query)
    {
        $query->whereIn('key',NotificationKey::toGeneralCompany());
    }
    public function scopePrivate($query)
    {
        $query->where(function($query){
            $query->whereIn('key',NotificationKey::toPrivate())
                ->orWhereNull('key');
        });
    }
    public function scopeNotPrivate($query)
    {
        $query->whereNotIn('key',NotificationKey::toPrivate())
            ->whereNotNull('key');
    }

    public function isGeneral():bool
    {
        if(in_array($this->key,NotificationKey::toGeneral()))
        {
            return true;
        }
        return false;
    }
    public function isPrivate():bool
    {
        if(in_array($this->key,NotificationKey::toPrivate()) || is_null($this->key))
        {
            return true;
        }
        return false;
    }
    public function isNotPrivate()
    {
        return !$this->isPrivate();
    }
    public function isGroup():bool
    {
        if(in_array($this->key,NotificationKey::toGroup()))
        {
            return true;
        }
        return false;
    }

    public function markAsRead($notifiable)
    {
        $notifiable->unreadNotifications()
            ->syncWithoutDetaching([
                $this->id=>['read_at'=>now()]
            ]);
    }
    public function markAsUnread($notifiable)
    {
        $notifiable->readNotifications()
            ->syncWithoutDetaching([
                $this->id=>['read_at'=>null]
            ]);
    }

    public function read()
    {
        return $this->pivot->read_at !== null;
    }

    public function unread()
    {
        return $this->pivot->read_at === null;
    }

    public function getPercentageOfProgress():int
    {
        if(blank($this->statistics))
        {
            return 0;
        }
        $successDevices=$this->statistics->successDevices;
        $failedDevices=$this->statistics->failedDevices;
        $totalDevices=$this->statistics->totalDevices;
        if($totalDevices==0)
        {
            return 0;
        }
        return floor((($successDevices+$failedDevices)/$totalDevices)*100);
    }
}

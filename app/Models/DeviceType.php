<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class DeviceType extends Model
{
    use SoftDeletes;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[

    ];

    public function properties()
    {
        return $this->morphMany(Property::class,'propertiable');
    }
    public function devices()
    {
        return $this->hasMany(Device::class);
    }
}

<?php

namespace App\Models;

class DeviceProperty extends Model
{
    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[

    ];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
    public function property()
    {
        return $this->belongsTo(Property::class);
    }
    public function option()
    {
        return $this->belongsTo(Option::class);
    }

    public function getTextValueAttribute()
    {
        if($this->option)
        {
            return $this->option->text;
        }

        return $this->value;
    }
}

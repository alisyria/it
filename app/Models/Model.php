<?php

namespace App\Models;

use App\Enums\Period;
use App\Models\Scopes\ShopScope;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Spatie\Translatable\HasTranslations;

/** @mixin \Eloquent */
class Model extends BaseModel
{

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        if(in_array(HasTranslations::class,class_uses(get_called_class())))
        {
            foreach ($this->getTranslatableAttributes() as $name) {
                $attributes[$name] = $this->getTranslation($name, app()->getLocale());
            }
        }

        return $attributes;
    }
    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
    public static function getMorphClassStatically():string
    {
        static $model;
        $class=static::class;
        if(!$model)
        {
            $model=new $class();
        }
        return $model->getMorphClass();
    }
    public function scopeForPolymorphic($query,string $relation,string $model)
    {
        $query->where($relation.'_type',$model::getMorphClassStatically());
    }
    public function scopeForPolymorphicID($query,string $relation,string $model,$id)
    {
        $query->forPolymorphic($relation,$model)->where($relation.'_id',$id);
    }

    public function scopePeriod($query,$period)
    {
        if(!$period)
        {
            return;
        }
        switch ($period)
        {
            case Period::DAILY:
                $query->daily();
                break;
            case Period::MONTHLY:
                $query->monthly();
                break;
            case Period::YEARLY:
                $query->yearly();
                break;
        }
    }
    public function scopeDaily($query)
    {
        $query->monthly()->whereDay('created_at',now()->day);
    }
    public function scopeMonthly($query)
    {
        $query->yearly()->whereMonth('created_at',now()->month);
    }
    public function scopeYearly($query)
    {
        $query->whereYear('created_at',now()->year);
    }
    public static function getAllItems()
    {
        return self::all();
    }
    public static function syncCache()
    {
        static::saved(function () {
            self::flushCache();
        });
        static::deleted(function(){
            self::flushCache();
        });
    }
}

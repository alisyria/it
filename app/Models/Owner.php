<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use SoftDeletes,HasFactory;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[

    ];

    public function department()
    {
        return $this->belongsTo(Department::class)
            ->withTrashed();
    }
    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function getFullNameAttribute()
    {
        return $this->name.' '.$this->last_name;
    }
    public function getFullNameWithDepartmentAttribute()
    {
        return $this->name.' '.$this->last_name.' - '.$this->department->name;
    }
}

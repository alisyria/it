<?php

namespace App\Models;

use App\Enums\Role as RoleEnum;
use App\Models\Scopes\RoleScope;
use Spatie\Translatable\HasTranslations;
use Spatie\Permission\Models\Role as BaseRole;
use Cache;

class Role extends BaseRole
{
    use HasTranslations;

    public static $cacheKey="employee.roles";
    public $translatable=[
        'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::updated(function () {
            self::flushCache();
        });

        static::created(function() {
            self::flushCache();
        });

        static::deleted(function(){
            self::flushCache();
        });
        static::addGlobalScope(new RoleScope());
    }

    public function roleable()
    {
        return $this->morphTo();
    }

    public function scopeAllowed($query)
    {
        $query->whereNotIn('name',RoleEnum::toNotAllowed());
    }

    public static function getAllowedRoles()
    {
        return Cache::rememberForever(self::$cacheKey, function() {
            return self::allowed()->latest()->get();
        });
    }
    public static function flushCache()
    {
        Cache::forget(self::$cacheKey);
    }

}

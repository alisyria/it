<?php

namespace App\Models;


use App\Models\Scopes\SortedScope;
use Cache;
use Illuminate\Support\Facades\Schema;
use LaravelLocalization;
use Rutorika\Sortable\SortableTrait;
use Spatie\Translatable\HasTranslations;

class AppLanguage extends Model
{
    use SortableTrait,HasTranslations;

    public $incrementing=false;
    protected static $cacheKey='app.languages';
    protected $translatable=[
        'desc'
    ];
    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at'
    ];

    public static  function boot()
    {
        parent::boot();
        static::addGlobalScope(new SortedScope());

        static::saved(function() {
            self::flushCache();
        });
        static::deleted(function(){
            self::flushCache();
        });
    }

    public function getIsSuppotedAttribute($value)
    {
        return $value;
    }
    public function getIsUnsuppotedAttribute()
    {
        return !$this->is_supported;
    }
    public function getDirectionAttribute()
    {
        return self::getLangDirection($this->id);
    }

    public function scopeDefault($query)
    {
        $query->where('is_default',1);
    }
    public function scopeSupported($query)
    {
        $query->where('is_supported',1);
    }
    public function scopeUnsupported($query)
    {
        $query->where('is_supported',0);
    }

    public static function getCurrentLanguage()
    {
        return self::getAllItems()->where('id',app()->getLocale())->first();
    }
    public static function getCurrentDirection()
    {
        return self::getLangDirection(self::getCurrentLanguage()->id);
    }
    public static function getDefaultLanguage()
    {
        return self::getAllItems()->where('is_default',1)->first();
    }
    public static function getDefaultLocal()
    {
        return optional(self::getDefaultLanguage())->id ?? 'en';
    }
    public static function getSupportedLocales()
    {
        return self::getAllItems()->where('is_supported',1);
    }
    public static function getSupportedLocalesWithoutCurrent()
    {
        return self::getSupportedLocales()->where('id','<>',app()->getLocale());
    }
    public static function langs():array
    {
        return self::getSupportedLocales()->pluck('id')->toArray();
    }
    public static function langsSelect():array
    {
        $result=[];
        foreach (self::getSupportedLocales() as $lang)
        {
            $result[$lang->id]=$lang->desc;
        }
        return $result;
    }
    public static function getSupportedLocalesConfig():array
    {
        $locales=[];
        foreach (self::getAllItems()->where('is_supported',1) as $lang)
        {
            $locales[$lang->id]=[
                'name'=>$lang->name,
                'native'=>$lang->native,
                'script'=>$lang->script,
                'regional'=>$lang->regional,
                'supported'=>$lang->is_supported,
                'default'=>$lang->is_default,
            ];
        }
        return $locales;
    }
    public static function isDefault(string $lang):bool
    {
        return self::getDefaultLocal()==$lang;
    }
    public static function getLangDirection($lang)
    {
        $supportedLocales=LaravelLocalization::getSupportedLocales();
        if (!empty($supportedLocales[$lang]['dir'])) {
            return $supportedLocales[$lang]['dir'];
        }

        switch ($supportedLocales[$lang]['script']) {
            // Other (historic) RTL scripts exist, but this list contains the only ones in current use.
            case 'Arab':
            case 'Hebr':
            case 'Mong':
            case 'Tfng':
            case 'Thaa':
                return 'rtl';
            default:
                return 'ltr';
        }
    }
    public static function isLangDirection($lang,$dir)
    {
        return self::getLangDirection($lang)==$dir;
    }
    public static function isLocalDirection($dir)
    {
        return self::getLangDirection(app()->getLocale())==$dir;
    }
    public static function isUnicode($lang)
    {
        return in_array($lang,['ar']);
    }
    public static function getLang($lang)
    {
        return self::getAllItems()->where('id',$lang)->first();
    }

    public static function getAllItems()
    {
        return Cache::rememberForever(static::$cacheKey,function (){
            if(!Schema::hasTable('app_languages'))
            {
                return collect([]);
            }
            return self::get();
        });
    }
    public static function flushCache()
    {
        Cache::forget(static::$cacheKey);
    }
}

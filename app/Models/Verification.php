<?php

namespace App\Models;


class Verification extends Model
{
    protected $guarded=['id','created_at','updated_at'];
    protected $hidden=['created_at','updated_at'];   
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }    
}

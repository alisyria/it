<?php

namespace App\Models;

use App\Enums\AuthGuard;
use App\Enums\ImageConversion;
use App\Models\Contracts\LocationableContract;
use App\Models\Traits\HasMediaTrait;
use App\Values\UserExtra;
use App\Models\Contracts\NotifiableContract;
use App\Models\Traits\Notifiable;
use Carbon\Carbon;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\File;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use App\Enums\UsernameType;
use App\Enums\Role as RoleEnum;
use App\Enums\UserType;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Traits\Chat\Chatable;
use libphonenumber\PhoneNumberFormat;
use Illuminate\Notifications\Notification;

/**
 * @property UserType $user_type
 * @property UsernameType $username_type
 * @property UserExtra $extra
 */
class User extends Authenticatable implements HasMedia,NotifiableContract
{
    use Notifiable,HasApiTokens,HasFactory,HasRoles,HasMediaTrait,SoftDeletes,CascadeSoftDeletes;

    protected $dates = ['deleted_at'];
    protected $cascadeDeletes =[];
    protected $fillable = [
        'name','middle_name','last_name','full_name','username', 'password', 'email',
        'mobile','user_type','username_type'
    ];
    protected $appends=['username_type','full_name','national_mobile','international_mobile','mobile_original',
        'mobile_original'];
    protected $hidden = [
        'password', 'remember_token','blocked_at','verified_at','activated_at',
        'deleted_at','updated_at'
    ];
    protected $casts=[
        'user_type'=>UserType::class,
        'username_type'=>UsernameType::class,
        'extra'=>UserExtra::class,
    ];
    const MEDIA_AVATAR='avatar';

    public static  function boot() {
        parent::boot();
        static::deleted(function($user) {
            if(is_null($user->activated_at))
            {
                return;
            }
            $user->username= "del".$user->id.'-'.$user->username;
            $user->save();
        });
    }

    public function guardName()
    {
        if(is_null($this->user_type))
        {
            return null;
        }
        if($this->isAdminUser())
        {
            return AuthGuard::ADMIN;
        }
        elseif($this->isCompanyUser())
        {
            return AuthGuard::COMPANY;
        }
        else
        {
            AuthGuard::API;
        }
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_AVATAR)
            ->singleFile()
            ->useFallbackUrl(asset('img/defaults/avatar.png'))
            ->registerMediaConversions(function(Media $media){

                $this->addMediaConversion(ImageConversion::SMALL)
                    ->fit(Manipulations::FIT_STRETCH,100,100)
                    ->keepOriginalImageFormat();
                $this->addMediaConversion(ImageConversion::MEDIUM)
                    ->fit(Manipulations::FIT_CONTAIN,400,400)
                    ->keepOriginalImageFormat();
                $this->addMediaConversion(ImageConversion::LARGE)
                    ->fit(Manipulations::FIT_CONTAIN,600,600)
                    ->keepOriginalImageFormat();
                $this->addMediaConversion(ImageConversion::DEFAULT)
                    ->keepOriginalImageFormat()
                    ->nonQueued();
            });
    }
    public function avatar()
    {
        return $this->firstMedia(self::MEDIA_AVATAR);
    }

    public function routeSMS(Notification $notification)
    {
        if($notification->forMobile)
        {
            return $notification->forMobile;
        }
        if($notification->forUsername)
        {
            return $this->username;
        }
        return $this->mobile_original;
    }
    public function routeNotificationForFcm()
    {
        return $this->loggedInRegistrations()->notifiable()->get()->pluck('reg_id')->toArray();
    }

    public function registrations()
    {
        return $this->belongsToMany(Registration::class)
                ->withPivot('login_at')
                ->withTimestamps();
    }
    public function loggedInRegistrations()
    {
        return $this->registrations()->whereNotNull('registration_user.login_at');
    }
    public function verifications()
    {
        return $this->hasMany(Verification::class);
    }
    public function employee()
    {
        return $this->hasOne(Employee::class,'id');
    }
    public function getFullNameAttribute($value)
    {
        if(filled($value))
        {
            return $value;
        }
        return $this->middle_name ? sprintf("%s %s %s", $this->name, $this->middle_name,
            $this->last_name) : sprintf("%s %s", $this->name,$this->last_name);
    }
    public function getMobileOriginalAttribute($value)
    {
        return $this->getRawOriginal('mobile');
    }
    public function getNationalMobileAttribute()
    {
        if(blank($this->country_id) || blank($this->mobile) || !is_numeric($this->mobile))
        {
            return null;
        };
        return phone($this->mobile,Country::getCountryCode($this->country_id),PhoneNumberFormat::NATIONAL);
    }
    public function getInternationalMobileAttribute()
    {
        if(blank($this->country_id) || blank($this->mobile) || !is_numeric($this->mobile))
        {
            return null;
        }
        return phone($this->mobile,Country::getCountryCode($this->country_id),PhoneNumberFormat::INTERNATIONAL);
    }
    public function scopeEmployeeType($query)
    {
        $query->where('user_type',UserType::EMPLOYEE);
    }
    public function scopeActive($query)
    {
        $query->whereNotNull('activated_at');
    }
    public function scopeInactive($query)
    {
        $query->whereNull('activated_at');
    }
    public function scopeFindUser($query,$usernameType=null,$username,$userTypeID=null)
    {
        $query->active()
              ->when(filled($usernameType),function($query)use($usernameType){
                  $usernameType=is_array($usernameType)?$usernameType:[$usernameType];
                  $query->whereIn('username_type',$usernameType);
              })
              ->where('username', $username)
              ->when(filled($userTypeID),function($query)use($userTypeID){
                  $userTypeID=is_array($userTypeID)?$userTypeID:[$userTypeID];
                  $query->whereIn('user_type',$userTypeID);
              });
    }
    public function scopeNotAdminType($q)
    {
        $q->whereNotIn('user_type',UserType::getAdminTypes());
    }
    public function isActive()
    {
        return !is_null($this->activated_at);
    }
    public function isSuperAdmin():bool
    {
        return $this->hasRole(RoleEnum::SUPER_ADMIN);
    }
    public static function getSuperAdmin()
    {
        return self::role(RoleEnum::SUPER_ADMIN)->first();
    }
    public function generateProfileUrl()
    {
        $url="";
        switch($this->user_type)
        {
            case UserType::EMPLOYEE:
                $url= route('admin.employees.details',[$this->id]);
                break;
        }
        return $url;
    }

    public function getLastSeenNotifications():Carbon
    {
        return $this->extra->lastSeenNotifications ?? $this->created_at;
    }
    public function markAllNotificationsSeen():void
    {
        $this->extra->lastSeenNotifications=now();
        $this->save();
    }
    public function isAdminUser():bool
    {
        return $this->user_type->in([UserType::EMPLOYEE,UserType::SUPER_ADMIN]);
    }
    public function isEmployee():bool
    {
        return $this->user_type->is(UserType::EMPLOYEE);
    }
    public function associateAvatar($avatar):Media
    {
        return $this->addMedia($avatar)
            ->toMediaCollection(static::MEDIA_AVATAR);
    }
}

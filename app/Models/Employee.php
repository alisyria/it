<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use App\Enums\Role as RoleEnum;

class Employee extends Model
{
    use SoftDeletes,CascadeSoftDeletes,HasFactory;

    public $incrementing=false;
    protected $dates = ['deleted_at'];
    protected $cascadeDeletes =[
        'user'
    ];
    protected $guarded = [
        'id','deleted_at','created_at','updated_at'
    ];
    protected $appends=[];
    protected $hidden = [
        'deleted_at','created_at','updated_at'
    ];

    public static function boot()
    {
        parent::boot();
//        static::addGlobalScope('active',function(Builder $builder){
//            $builder->whereHas('user',function ($query){
//                $query->active();
//            });
//        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'id');
    }
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function scopeAllowed($query)
    {
        $query->whereDoesntHave('user',function($query){
            $query->role(RoleEnum::SUPER_ADMIN);
        });
    }
    public function scopeWithoutOtherManagers($query)
    {
        $query->whereHas('user.roles',function($query){
            $query->where('name','<>',RoleEnum::GENERAL_MANAGER);
        });
    }
}

<?php

namespace App\Models;

use App\Models\States\Device\DeviceState;
use App\Models\States\Device\Inoperative;
use App\Models\States\Device\Operative;
use App\Models\States\Device\Transitions\ToInoperative;
use App\Models\States\Device\Transitions\ToOperative;
use App\Models\Traits\HasStatusesExtended;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\ModelStates\HasStates;

/**
 * @property DeviceState $state
 */
class Device extends Model
{
    use SoftDeletes,HasFactory,HasStates,HasStatusesExtended;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[

    ];

    public function registerStates(): void
    {
        $this->addState('state',DeviceState::class)
            ->default(Operative::class)
            ->allowTransition(Operative::class,Inoperative::class,ToInoperative::class)
            ->allowTransition(Operative::class,Operative::class,ToOperative::class)
            ->allowTransition(Inoperative::class,Operative::class,ToOperative::class)
            ->allowTransition(Inoperative::class,Inoperative::class,ToInoperative::class);;

    }

    public function deviceType()
    {
        return $this->belongsTo(DeviceType::class);
    }
    public function deviceProperties()
    {
        return $this->hasMany(DeviceProperty::class);
    }
    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function scopeOperative($query)
    {
        $query->whereState('state',Operative::class);
    }
    public function scopeInoperative($query)
    {
        $query->whereState('state',Inoperative::class);
    }

    public function isOperative():bool
    {
        return $this->state->is(Operative::class);
    }
    public function isInoperative():bool
    {
        return $this->state->is(Inoperative::class);
    }

    public function transitionOperative(User $user,string $reason=null)
    {
        $this->state->transitionTo(Operative::class,$user,$reason);
    }
    public function transitionInoperative(User $user,string $reason=null)
    {
        $this->state->transitionTo(Inoperative::class,$user,$reason);
    }

    public function propertiesToText(bool $isHtml=true):?string
    {
        $text=null;
        $lastOptionID=optional($this->deviceProperties->last())->id;
        foreach($this->deviceProperties as $deviceProperty)
        {
            if($isHtml)
            {
                $text.="<b>{$deviceProperty->property->name}:</b> {$deviceProperty->text_value}";
            }
            else
            {
                $text.="{$deviceProperty->property->name}: {$deviceProperty->text_value}";
            }
            if($deviceProperty->id!=$lastOptionID)
            {
                $text.=', ';
            }
        }
        return $text;
    }
}

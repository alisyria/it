<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rutorika\Sortable\SortableTrait;

class Item extends Model
{
    use SoftDeletes, SortableTrait;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[

    ];

    public function section(): BelongsTo
    {
        return $this->belongsTo(Section::class);
    }

    public function items(): BelongsToMany
    {
        return $this->belongsToMany(Order::class)
            ->withPivot('value')
            ->withTimestamps();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rutorika\Sortable\SortableTrait;

class Section extends Model
{
    use SoftDeletes, SortableTrait;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[

    ];

    public function items(): HasMany
    {
        return $this->hasMany(Item::class);
    }
}

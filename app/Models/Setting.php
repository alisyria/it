<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\SchemalessAttributes\SchemalessAttributes;
use Illuminate\Database\Eloquent\Builder;
use Spatie\MediaLibrary\MediaCollections\File;
use Cache;

class Setting extends Model implements HasMedia
{
    use InteractsWithMedia;

    public $casts=[
        'value'=>'array'
    ];
    protected $guarded=['id','old_id','created_at','updated_at'];
    protected $hidden=['created_at','updated_at'];
    protected static $logName = 'settings';
    
    protected static function boot()
    {
        parent::boot();

        static::saved(function () {
            self::flushCache();
        });
        
        static::deleted(function(){
            self::flushCache();
        });
    }      
    public function getValueAttribute(): SchemalessAttributes
    {
        return SchemalessAttributes::createForModel($this, 'value');
    }

    public function scopeWithValueAttributes(): Builder
    {
        return SchemalessAttributes::scopeWithSchemalessAttributes('value');
    }    
    
    public function registerMediaCollections():void
    {
        $this->addMediaCollection('pdf')
             ->singleFile()
             ->acceptsFile(function(File $file){
                 return TRUE;
             });
    }


    public function scopeFindByKey($query,$key)
    {
        return $query->where("key",$key);
    }
    public function scopeBasic($query)
    {
        $query->with('media');
    }
    
    public static function getAllSettings()
    {
        return Cache::rememberForever('settings.app', function() {
            return self::basic()->get();
        });
    }
    public static function flushCache()
    {
        Cache::forget('settings.app');
    }

}

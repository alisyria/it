<?php

namespace App\Models;

use Spatie\ModelStatus\Status as BaseStatus;

class Status extends BaseStatus
{
    public function causer()
    {
        return $this->belongsTo(User::class,'causer_id')
                    ->withTrashed();
    }
}

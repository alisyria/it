<?php

namespace App\Models;

use App\Enums\FieldType;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[
        'field_type'=>FieldType::class,
    ];

    public function propertiable()
    {
        return $this->morphTo();
    }
    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function isFieldTextSingleLine():bool
    {
        return $this->field_type->is(FieldType::TEXT_SINGLE_LINE());
    }
    public function isFieldSingleOption():bool
    {
        return $this->field_type->is(FieldType::SINGLE_OPTION());
    }
}

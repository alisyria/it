<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\NotifiableTrait;

class Notifiable extends Model
{
    protected $table='notifiables';
    protected $casts = [
        'read_at' => 'datetime',
    ];
}

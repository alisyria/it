<?php

namespace App\Models\Traits;


trait NotifiableTrait
{
    protected $table='notifiables';
    protected $casts = [
        'read_at' => 'datetime',
    ];
}
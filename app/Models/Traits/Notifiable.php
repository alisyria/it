<?php

namespace App\Models\Traits;


use App\Models\Notification;
use Illuminate\Notifications\RoutesNotifications;

trait Notifiable
{
    use RoutesNotifications;

    public $pivotTable='notifiables';
    /**
     * Get the entity's notifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications()
    {
        return $this->morphToMany(Notification::class,'notifiable')
            ->withPivot('read_at')
            ->withTimestamps()
            ->withoutGlobalScopes()
            ->orderBy('created_at', 'desc');
    }
    public function privateNotifications()
    {
        return $this->notifications()->private();
    }
    public function notPrivateNotifications()
    {
        return $this->notifications()->notPrivate();
    }
    /**
     * Get the entity's read notifications.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function readNotifications()
    {
        return $this->notifications()->whereNotNull($this->pivotTable.'.read_at');
    }

    /**
     * Get the entity's unread notifications.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function unreadNotifications()
    {
        return $this->notifications()->whereNull($this->pivotTable.'.read_at');
    }

    public function markNotificationsRead(...$ids):void
    {
        $syc=[];
        foreach($ids as $id)
        {
            $syc[$id]=['read_at'=>now()];
        }
        $this->notifications()->syncWithoutDetaching($syc);
    }
    public function markNotificationsUnRead(...$ids):void
    {
        $syc=[];
        foreach($ids as $id)
        {
            $syc[$id]=['read_at'=>null];
        }
        $this->notifications()->syncWithoutDetaching($syc);
    }
    public function markAllNotificationsRead():void
    {
        \App\Models\Notifiable::where('notifiable_type',self::getMorphClass())
            ->whereNull('read_at')
            ->where('notifiable_id',$this->id)
            ->update(['read_at'=>now()]);
    }
    public function clearNotifications():void
    {
        $this->privateNotifications()->delete();
        $this->notPrivateNotifications()->detach();
    }
    public function deleteNotification(Notification $notification):void
    {
        $this->privateNotifications()->where('notifications.id',$notification->id)->delete();
        $this->notPrivateNotifications()
            ->where('notifications.id',$notification->id)
            ->detach($notification->id);
    }
    public function getUnseenNotificationsCount():int
    {
        $lastSeen=$this->getLastSeenNotifications() ?? now();
        return $this->notifications()->where('notifications.created_at','>=',$lastSeen)->count();
    }
}

<?php

namespace App\Models\Traits;


use App\Models\User;
use Spatie\ModelStatus\HasStatuses;

trait HasStatusesExtended
{
    use HasStatuses;

    public function changeStatus(string $status,User $causer=null,string $reason=null)
    {
        $this->setStatus($status,$reason);
        $this->latestStatus()->update([
            'causer_id'=>optional($causer)->id,
        ]);
    }
}
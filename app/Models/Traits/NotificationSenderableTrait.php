<?php


namespace App\Models\Traits;


use App\Models\Notification;

trait NotificationSenderableTrait
{
    public function sendedNotifications()
    {
        return $this->morphMany(Notification::class,'senderable');
    }
}

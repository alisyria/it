<?php


namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Relations\MorphOne;
use Spatie\MediaLibrary\InteractsWithMedia;

trait HasMediaTrait
{
    use InteractsWithMedia;

    public function firstMedia(string $collectionName): MorphOne
    {
        return $this->morphOne(config('media-library.media_model'), 'model')
                    ->where('collection_name',$collectionName);
    }
}

<?php

namespace App\Models;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;

class Media extends BaseMedia 
{
    protected $appends=['conversions','url'];
    protected $hidden=['model_id','model_type','collection_name','disk','manipulations','created_at','updated_at'];
    
    public function getUrlAttribute()
    {
        return $this->getFullUrl();
    }
    public function getConversionsAttribute()
    {
        $generatedConversions=$this->getGeneratedConversions();
        $justDefaultGeneratedConversion=$generatedConversions->count()==1 && $generatedConversions->has('default');

        if($generatedConversions->isEmpty() || $justDefaultGeneratedConversion)
        {
            $generatedConversions=collect([
                'small'=>true,
                'medium'=>true,
                'large'=>true,
            ]);
        }

        $conversions=[];
        foreach ($generatedConversions as $conversion=>$bool)
        {
            if($conversion=="default")
            {
                continue;
            }
            $conversions[$conversion]=  ['url'=> $this->getConversionUrl($conversion)];
        }
        return $conversions;
    }
    private function getConversionUrl(string $conversion):string
    {
        $url="";
        
        if($this->hasGeneratedConversion($conversion))
        {
            $url=$this->getFullUrl($conversion);
        }
        elseif($this->hasGeneratedConversion("default")) 
        {
            $url=$this->getFullUrl("default");
        }
        else
        {
           $url= $this->getFullUrl(); 
        }   
        
        return $url;
    }
}
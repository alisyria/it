<?php

namespace App\Models;

use App\Enums\Permission as PermissionEnum;
use App\Models\Scopes\PermissionScope;
use Spatie\Permission\Models\Permission as BasePermission;
use Cache;

class Permission extends BasePermission
{
    public static $cacheKey="employee.permissions";
    protected $appends=[
        'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function () {
            self::flushCache();
        });

        static::deleted(function(){
            self::flushCache();
        });

        static::addGlobalScope(new PermissionScope());
    }

    public function getDescriptionAttribute()
    {
        return PermissionEnum::getDescription($this->name);
    }

    public function scopeAllowed($query)
    {
        $query->whereNotIn('name',PermissionEnum::toNotAllowed());
    }

    public static function getAllowedPermissions()
    {
        return Cache::rememberForever(self::$cacheKey, function() {
            return self::allowed()->get();
        });
    }
    public static function flushCache()
    {
        Cache::forget(self::$cacheKey);
    }
}

<?php

namespace App\Models;

use App\Enums\OrderStatus;
use App\Models\States\Order\Failed;
use App\Models\States\Order\OrderState;
use App\Models\States\Order\Pending;
use App\Models\States\Order\Processing;
use App\Models\States\Order\Successes;
use App\Models\States\Order\Transitions\ToFailed;
use App\Models\States\Order\Transitions\ToPending;
use App\Models\States\Order\Transitions\ToProcessing;
use App\Models\States\Order\Transitions\ToSuccesses;
use App\Models\Traits\HasStatusesExtended;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\ModelStates\HasStates;

/**
 * @property OrderState $state
 */
class Order extends Model
{
    use SoftDeletes, HasStates, HasStatusesExtended;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];

    public $casts=[
        'state_updated_at' => 'datetime',
        'requested_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('employee', function (Builder $q) {
            if (user() === null || user()->isSuperAdmin()) {
                return;
            }
            $q->where('orders.employee_id', user()->id);
        });
    }

    public function registerStates(): void
    {
        $this->addState('state', OrderState::class)
            ->default(Pending::class);
    }

    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class);
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

    public function items(): BelongsToMany
    {
        return $this->belongsToMany(Item::class)
            ->withPivot('value')
            ->withTimestamps();
    }

    public function scopePending(Builder $query)
    {
        $query->whereState('state', Pending::class);
    }

    public function scopeProcessing(Builder $query)
    {
        $query->whereState('state', Processing::class);
    }

    public function scopeSuccesses(Builder $query)
    {
        $query->whereState('state', Successes::class);
    }

    public function scopeFailed(Builder $query)
    {
        $query->whereState('state', Failed::class);
    }

    public function transitionPending(User $user): void
    {
        $this->state->transition(new ToPending($this, $user));
    }
    public function transitionProcessing(User $user): void
    {
        $this->state->transition(new ToProcessing($this, $user));
    }
    public function transitionSuccesses(User $user): void
    {
        $this->state->transition(new ToSuccesses($this, $user));
    }
    public function transitionFailed(User $user): void
    {
        $this->state->transition(new ToFailed($this, $user));
    }
    public function transitionFromStatus(OrderStatus $orderStatus, User $user)
    {
        switch ($orderStatus->value)
        {
            case OrderStatus::PENDING : $this->transitionPending($user);
            break;
            case OrderStatus::PROCESSING : $this->transitionProcessing($user);
            break;
            case OrderStatus::SUCCESSES : $this->transitionSuccesses($user);
            break;
            case OrderStatus::FAILED : $this->transitionFailed($user);
            break;
        }
    }
}

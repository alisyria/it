<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $guarded=[
        'id','deleted_at','created_at','updated_at'
    ];
    protected $hidden=[
        'deleted_at','created_at','updated_at','position'
    ];
    protected $translatable=[

    ];
    public $casts=[

    ];

    public function owners()
    {
        return $this->hasMany(Owner::class);
    }
}

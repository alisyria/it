<?php

namespace App\Models;

use App\Enums\NotificationTarget;
use App\Enums\RegistrationExtra;
use App\Enums\VendorType;
use App\Models\Traits\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\SchemalessAttributes\SchemalessAttributes;

class Registration extends Model
{
    use Notifiable,HasFactory;

    public $casts=[
        'extra'=>'array',
        'ios_version'=>'float',
    ];
    protected $appends=['app_i_env','os'];
    protected $fillable=['device_id','os_id','instance_uuid','language'];
    protected $hidden=['instance_uuid','extra'];

    public function getExtraAttribute(): SchemalessAttributes
    {
        return SchemalessAttributes::createForModel($this, 'extra');
    }
    public function scopeWithExtraAttributes(): Builder
    {
        return SchemalessAttributes::scopeWithSchemalessAttributes('extra');
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
                ->withPivot('login_at')
                ->withTimestamps();
    }
    public function loggedInUsers()
    {
        return $this->users()->whereNotNull('registration_user.login_at');
    }

    public function getOsAttribute()
    {
        return $this->os_id;
    }
    public function getAppIEnvAttribute()
    {
        if(filled(setting('app_iphone_env')) && $this->ios_version > config('custom.last_ios_version'))
        {
            return setting('app_iphone_env');
        }
        return 'production';
    }

    public function scopeDevice($query,$instance_uuid)
    {
        return $query->where('instance_uuid',$instance_uuid);
    }
    public function scopeNotifiable($query)
    {
        return $query->where('is_notifiable',1)->whereNotNull('reg_id');
    }
    public function scopeFilter(Builder $q,?NotificationTarget $target,?VendorType $vendorType,
                                array $governorates=[],array $regions=[])
    {
        $q->filterByUsers($target,$vendorType,$governorates,$regions);
    }
    public function scopeFilterNotifiable(Builder $q,?NotificationTarget $target,?VendorType $vendorType,
                                          array $governorates=[],array $regions=[])
    {
        $q->notifiable()->filter($target,$vendorType,$governorates,$regions);
    }
    public function scopeFilterByUsers(Builder $q,NotificationTarget $target=null,?VendorType $vendorType,
                                array $governorates=[],array $regions=[])
    {
        $q->whereHas('loggedInUsers',fn($q)=>$q->forNotification($target,$vendorType,$governorates,$regions));
    }
    public function getLastSeenNotifications()
    {
        return $this->extra->{RegistrationExtra::LAST_SEEN_NOTIFICATIONS};
    }
}

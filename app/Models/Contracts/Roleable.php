<?php

namespace App\Models\Contracts;


interface Roleable
{
    public function roles();
}
<?php


namespace App\Models\Contracts;


interface NotificationSenderable
{
    public function sendedNotifications();
}

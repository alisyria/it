<?php

namespace App\Models\Contracts;


use App\Models\Notification;
use Carbon\Carbon;

interface NotifiableContract
{
    public function notifications();
    public function privateNotifications();
    public function notPrivateNotifications();
    public function readNotifications();
    public function unreadNotifications();
    public function markNotificationsRead(...$ids):void;
    public function markNotificationsUnRead(...$ids):void;
    public function markAllNotificationsRead():void;
    public function markAllNotificationsSeen():void;
    public function getLastSeenNotifications():Carbon;
    public function deleteNotification(Notification $notification):void;
    public function clearNotifications():void;
    public function getUnseenNotificationsCount():int;
}

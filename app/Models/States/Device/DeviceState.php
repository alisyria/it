<?php


namespace App\Models\States\Device;


use App\Enums\DeviceStatus;
use Spatie\ModelStates\State;

abstract class DeviceState extends State
{
    abstract public static function getStatus():DeviceStatus;
    abstract public static function getColor():string;
}

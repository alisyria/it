<?php


namespace App\Models\States\Device\Traits;


use App\Enums\DeviceStatus;

trait DeviceStatusTrait
{
    public static function getStatus(): DeviceStatus
    {
        return DeviceStatus::fromValue(static::$name);
    }
    public static function getColor(): string
    {
        return DeviceStatus::getColor(static::$name);
    }
}

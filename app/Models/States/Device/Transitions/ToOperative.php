<?php


namespace App\Models\States\Device\Transitions;

use App\Models\User;
use DB;
use App\Enums\DeviceStatus;
use App\Models\States\Device\Operative;
use App\Models\Device;
use Spatie\ModelStates\Transition;

class ToOperative extends Transition
{
    private Device $device;
    private User $causer;
    private ?string $reason;

    public function __construct(Device $device,User $causer,string $reason=null)
    {
        $this->device=$device;
        $this->causer=$causer;
        $this->reason=$reason;
    }

    public function handle():Device
    {
        DB::transaction(function (){
            $this->device->state=new Operative($this->device);
            $this->device->save();

            $this->device->changeStatus(DeviceStatus::OPERATIVE,$this->causer,$this->reason);
        });

        return $this->device;
    }
}

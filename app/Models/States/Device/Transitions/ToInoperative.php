<?php


namespace App\Models\States\Device\Transitions;


use App\Models\User;
use DB;
use App\Enums\DeviceStatus;
use App\Models\States\Device\Inoperative;
use App\Models\Device;
use Spatie\ModelStates\Transition;

class ToInoperative extends Transition
{
    private Device $device;
    private User $causer;
    private ?string $reason;

    public function __construct(Device $device,User $causer,string $reason=null)
    {
        $this->device=$device;
        $this->causer=$causer;
        $this->reason=$reason;
    }

    public function handle():Device
    {
        DB::transaction(function (){
            $this->device->state=new Inoperative($this->device);
            $this->device->save();

            $this->device->changeStatus(DeviceStatus::INOPERATIVE,$this->causer,$this->reason);
        });

        return $this->device;
    }
}

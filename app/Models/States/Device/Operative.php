<?php


namespace App\Models\States\Device;


use App\Enums\DeviceStatus;
use App\Models\States\Device\Traits\DeviceStatusTrait;

class Operative extends DeviceState
{
    use DeviceStatusTrait;

    public static $name=DeviceStatus::OPERATIVE;
}

<?php

namespace App\Models\States\Order;


use App\Enums\OrderStatus;
use App\Models\States\Order\Traits\OrderStatusTrait;

class Successes extends OrderState
{
    use OrderStatusTrait;

    public static $name=OrderStatus::SUCCESSES;
    public static $color='green';
}

<?php

namespace App\Models\States\Order;


use App\Enums\OrderStatus;
use App\Models\States\Order\Traits\OrderStatusTrait;

class Failed extends OrderState
{
    use OrderStatusTrait;

    public static $name=OrderStatus::FAILED;
    public static $color='pink';
}

<?php

namespace App\Models\States\Order\Transitions;


use App\Enums\CompanyPermission;
use App\Enums\OrderStatus;
use App\Models\Order;
use App\Models\States\Order\Pending;
use App\Models\User;
use Spatie\ModelStates\Transition;
use DB;

class ToPending extends Transition
{
    private $order;
    private $causer;

    public function __construct(Order $order,User $user)
    {
        $this->order=$order;
        $this->causer=$user;
    }

    public function handle():Order
    {
        DB::transaction(function (){
            $this->order->state=new Pending($this->order);
            $this->order->state_updated_at=now();
            $this->order->requested_at=now();
            $this->order->save();

            $this->order->changeStatus(OrderStatus::PENDING,$this->causer);
            $this->order->device->transitionInoperative($this->causer);
        });

        return $this->order;
    }
}

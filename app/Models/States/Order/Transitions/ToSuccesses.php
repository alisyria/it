<?php

namespace App\Models\States\Order\Transitions;

use App\Enums\OrderStatus;
use App\Models\Order;
use App\Models\States\Order\Successes;
use App\Models\User;
use Spatie\ModelStates\Transition;
use DB;

class ToSuccesses extends Transition
{
    private Order $order;
    private User $causer;

    public function __construct(
        Order $order,
        User $causer
    ) {
        $this->order=$order;
        $this->causer=$causer;
    }

    public function handle():Order
    {
        DB::transaction(function (){
            $this->order->state=new Successes($this->order);
            $this->order->state_updated_at=now();
            $this->order->save();

            $this->order->changeStatus(OrderStatus::SUCCESSES,$this->causer);
            $this->order->device->transitionOperative($this->causer);
        });

        return $this->order;
    }
}

<?php

namespace App\Models\States\Order;


use App\Enums\OrderStatus;
use App\Models\States\Order\Traits\OrderStatusTrait;

class Processing extends OrderState
{
    use OrderStatusTrait;

    public static $name=OrderStatus::PROCESSING;
    public static $color='blue';
}

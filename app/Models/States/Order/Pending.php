<?php

namespace App\Models\States\Order;


use App\Enums\OrderStatus;
use App\Models\States\Order\Traits\OrderStatusTrait;

class Pending extends OrderState
{
    use OrderStatusTrait;

    public static $name=OrderStatus::PENDING;
    public static $color='yellow';
}

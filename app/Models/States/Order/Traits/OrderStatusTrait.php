<?php

namespace App\Models\States\Order\Traits;


use App\Enums\OrderStatus;

trait OrderStatusTrait
{
    public static function getStatus(): OrderStatus
    {
        return OrderStatus::fromValue(static::$name);
    }
    public static function getColor(): string
    {
        return static::$color;
    }
}

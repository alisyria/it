<?php

namespace App\Models\States\Order;


use App\Enums\OrderStatus;
use Spatie\ModelStates\State;

abstract class OrderState extends State
{
    abstract public static function getStatus():OrderStatus;
    abstract public static function getColor():string;
}

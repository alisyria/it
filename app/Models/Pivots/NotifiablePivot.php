<?php

namespace App\Models\Pivots;

use Illuminate\Database\Eloquent\Relations\MorphPivot;

class NotifiablePivot extends MorphPivot
{
    public $incrementing = true;
    protected $table='notifiables';
    protected $casts = [
        'read_at' => 'datetime',
    ];
}

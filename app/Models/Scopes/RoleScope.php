<?php


namespace App\Models\Scopes;

use App\Enums\AuthGuard;
use App\Enums\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class RoleScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $adminUser=adminUser();

        if($adminUser)
        {
            $builder->where('guard_name',AuthGuard::ADMIN);
        }

        if(optional($adminUser)->isEmployee())
        {
            $builder->where('name','<>',Role::SUPER_ADMIN);
        }
    }
}

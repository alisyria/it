<?php


namespace App\Models\Scopes;


use App\Enums\AuthGuard;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class PermissionScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $adminUser = adminUser();
        if ($adminUser) {
            $builder->where('guard_name', AuthGuard::ADMIN);
        }
    }
}

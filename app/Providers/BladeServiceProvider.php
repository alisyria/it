<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         /*
         * use translation blade directive @langj to get translation only from json files
         */
         Blade::directive('langj',function ($expression){
            return "<?php echo __($expression); ?>";
         });
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Maatwebsite\Excel\Concerns\WithEvents;
use \Maatwebsite\Excel\Sheet;

class ExcelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Sheet::macro('customize', function (Sheet $sheet, string $cellRange,array $colsWidth) {
            $customStyle=[
                'alignment'=> [
                    'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                ],
                'font'=>[
                    'bold'=>true,
                    'color'=> ['argb' => 'FFFFFF'],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => '057A55',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($customStyle);
            foreach($colsWidth as $key=>$width)
            {
                $sheet->getColumnDimension($key)->setWidth($width);
            }

            $sheet->setRightToLeft(true);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

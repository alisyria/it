<?php

namespace App\Providers;

use App\Models\Enquiry;
use App\Models\Verification;
use App\Observers\EnquiryObserver;
use App\Observers\VerificationObserver;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Listeners\StatusUpdatedListener;
use Illuminate\Database\Events\MigrationEnded;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Verification::observe(VerificationObserver::class);

        Event::listen(MigrationEnded::class,function(MigrationEnded $event){
            if(!$this->app->isLocal())
            {
                return;
            }
            \Artisan::call('ide-helper:models',[
                '--nowrite'=>true,
                '--reset'=>true,
                '--no-interaction'=>true,
            ]);
        });
    }
}

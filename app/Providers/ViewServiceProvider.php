<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use App\Models\Role;
use App\Models\Permission;
use App\Models\AppLanguage;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*',function(\Illuminate\View\View $view){
            static $data=null;
            if(is_null($data))
            {
                $data=[
                    'langDir'=>localClass(),
                ];
            }
            share($data);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

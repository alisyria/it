<?php

namespace App\Providers;

use App\Enums\Admin\AdminStatistic;
use App\Enums\CompanyStatistic;
use App\Policies\Admin\AdminStatisticPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Role;
use App\Models\User;
use App\Enums\Ability;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::guessPolicyNamesUsing(function ($modelClass) {
            return 'App\\Policies\\' . class_basename ( $modelClass).'Policy';
        });
        /**
         * grant all abilities to super admin
         * @param $user \App\Models\User
         * @param $ability
         */
        Gate::after(function ($user, $ability) {
            if ($user->isSuperAdmin()) {
                return true;
            }
        });
        /**
         * view user's secret data ability
         * @param $user \App\Models\User
         * @param $user \App\Models\User or any sub type like:customer,admin
         */
        Gate::define(Ability::VIEW_MY,function($user,$target){
            return $user->id==$target->id;
        });
        Gate::define('store-role',function (User $user,Role $role){
            if(!is_null($role->key))
            {
                return false;
            }
            return true;
        });
        Gate::define('delete-role',function (User $user,Role $role){
            if(!is_null($role->key))
            {
                return false;
            }
            return true;
        });
        //Admin Statistics
        Gate::resource('admin-statistics',AdminStatisticPolicy::class,AdminStatistic::$gates);

    }
}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\Events\JobFailed;
use App\Jobs\SendPushNotificationsChunkJob;
use App\Events\PushNotificationChunkJobProcessedEvent;
use App\Events\PushNotificationChunkEvent;

class QueueServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::after(function(JobProcessed $event){
            if($event->job->isReleased())
            {
                return;
            }
            $jobClass=$event->job->payload()['data']['commandName'];
            switch($jobClass)
            {
                case SendPushNotificationsChunkJob::class:
                    $job=unserialize($event->job->payload()['data']['command']);
                    event(new PushNotificationChunkJobProcessedEvent($job,'processed'));
                    event(new PushNotificationChunkEvent($job->notification));
                    break;
            }
            
            //$job->pushNotification->increment('all_devices');
            //logger($job->pushNotification->toArray());
        });
        Queue::failing(function (JobFailed $event) {
            if($event->job->isReleased())
            {
                return;
            }            
            $jobClass=$event->job->payload()['data']['commandName'];
            switch($jobClass)
            {
                case SendPushNotificationsChunkJob::class:
                    $job=unserialize($event->job->payload()['data']['command']);
                    event(new PushNotificationChunkJobProcessedEvent($job,'failed'));  
                    event(new PushNotificationChunkEvent($job->notification));
                    break;
            }
        });        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

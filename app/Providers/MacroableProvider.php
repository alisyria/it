<?php

namespace App\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use BenSampo\Enum\Enum;

class MacroableProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Enum::macro('only', function($vals=null){
            return array_filter(self::asArray(),function($value)use($vals){
                    return in_array($value, $vals);
                });
        });
        Enum::macro('exclude', function($vals=null){
            return array_filter(self::asArray(),function($value)use($vals){
                return !in_array($value, $vals);
            });
        });
        Enum::macro('toComma', function($vals=null){
            $array=self::asArray();
            if(!is_null($vals))
            {
                $array= self::only($vals);
            }
            return implode(',',$array);
        });
        Enum::macro('toResource', function(array $except=[]){
            $array=self::asSelectArray();
            $array=Arr::except($array,$except);
            $resources=[];
            foreach ($array as $id=>$value)
            {
                $resources[]=['id'=>$id,'desc'=>$value];
            }
            return $resources;
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

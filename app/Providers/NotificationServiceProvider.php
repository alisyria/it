<?php

namespace App\Providers;

use App\Notifications\Channels\ZainSMSChannel;
use Illuminate\Support\ServiceProvider;
use App\Notifications\Channels\Contracts\SMSNotificationChannel;
use App\Notifications\Channels\Contracts\PushNotificationChannel;
use App\Notifications\Channels\SmartSMSChannel;
use App\Notifications\Channels\LogSMSChannel;
use App\Notifications\Channels\ProductionPushChannel;
use App\Notifications\Channels\LogPushChannel;
use App\Enums\PushChannel;
use App\Enums\SMSChannel;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}

<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Adaojunior\Passport\SocialUserResolverInterface;
use Illuminate\Database\Eloquent\Relations\Relation;
use Redis;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if($this->app->isLocal())
        {
            //Redis::enableEvents();
        }
//        DB::listen(function ($query) {
//            logger($query->sql,$query->bindings,$query->time);
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}

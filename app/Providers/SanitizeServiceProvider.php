<?php

namespace App\Providers;

use App\Filters\BooleanFilter;
use App\Filters\MobileFilter;
use App\Filters\NumberToArabicFilter;
use App\Filters\PercentDecimalFilter;
use App\Filters\ShippingAddressFilter;
use App\Filters\ZeroNullFilter;
use Illuminate\Support\ServiceProvider;
use Sanitizer;

class SanitizeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Sanitizer::extend('arabic_number',NumberToArabicFilter::class);
        Sanitizer::extend('zero_null',ZeroNullFilter::class);
        Sanitizer::extend('mobile',MobileFilter::class);
        Sanitizer::extend('percent_decimal',PercentDecimalFilter::class);
        Sanitizer::extend('boolean',BooleanFilter::class);
    }
}

<?php

namespace App\Http\Requests\Admin\RoleCtrl;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRoleReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $nameUniqueRule=Rule::unique('roles');
        if($this->isMethod('PUT'))
        {
            $nameUniqueRule->ignore($this->role->id);
        }
                
        return [
            'name'=>['required',$nameUniqueRule],
            'description'=>'required|max:4000',
            'permissions'=>'nullable|array',
            'permissions.*'=>'exists:permissions,id'
        ];
    }
    public function attributes() {
        return [
            'name'=>'اسم الدور'
        ];
    }
}

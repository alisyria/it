<?php

namespace App\Http\Requests\Admin\EmployeeCtrl;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Enums\UserType;

class StoreEmployeeReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $usernameUniqueRule=Rule::unique('users','username')->where(function($query){
                    $query->whereNotNull('activated_at')
                          ->where('user_type',UserType::EMPLOYEE)
                          ->whereNull('deleted_at');
        });

        if($this->isMethod('PUT'))
        {
            $usernameUniqueRule->ignore($this->employee->id);
        }
        $rules=[
            'name'=>'required|max:50',
            'last_name'=>'required|max:50',
            'username'=>['required','email',$usernameUniqueRule],
            'password'=>['nullable','min:6'],
            'mobile'=>'required|max:50',
            'avatar'=>'nullable|file|mimes:jpeg,png|max:'.config('custom.images.max-size'),
            'roles'=>'required|array|min:1',
            'roles.*'=>'exists:roles,id'
        ];
        if($this->isMethod('POST'))
        {
            $rules['password']= array_prepend($rules['password'],'required');
        }

        return $rules;
    }
    public function attributes()
    {
        return [
            'username'=>__('validation.attributes.email'),
            'avatar'=>__('validation.attributes.image'),
        ];
    }
}

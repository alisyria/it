<?php

namespace App\Http\Requests\Admin\EmployeeCtrl;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UpdateAccountReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'last_name'=>'required',
            'mobile'=>'nullable|max:30',
            'email'=>['required','email',Rule::unique('users','username')->ignore($this->user()->id)],
            'new_password'=>'nullable|min:6|confirmed',
            'current_password'=>'required',
            'image'=>'nullable|file|mimes:jpeg,png|max:'.config('custom.images.max-size')
        ];
    }
    public function withValidator($validator)
    {
        $validator->after(function($validator){
            if(!Hash::check($this->current_password,$this->user()->password))
            {
                $validator->errors()->add('current_password',__j('inavlid current password'));
            }
        });
    }
}

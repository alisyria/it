<?php

namespace App\Http\Requests\Admin\TranslationCtrl;

use App\Models\AppLanguage;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DownloadTransReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $LangSupported=Rule::in(AppLanguage::langs());
        return [
            'langs'=>['nullable','array'],
            'langs.*'=>[$LangSupported],
        ];
    }
    public function attributes()
    {
        return __('fields.admin.download_translations');
    }
}

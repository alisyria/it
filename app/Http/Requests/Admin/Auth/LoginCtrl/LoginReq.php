<?php

namespace App\Http\Requests\Admin\Auth\LoginCtrl;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LoginReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>'required',
            'password'=>'required'
        ];
    }
    public function attributes()
    {
        return [
            'username'=>__('validation.attributes.email')
        ];
    }
}

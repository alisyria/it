<?php

namespace App\Http\Requests\Admin\NotificationCtrl;

use Illuminate\Foundation\Http\FormRequest;

class SendGeneralReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'=>'required|array',
            'text.ar'=>'required|max:500',
            'text.en'=>'required|max:500',
        ];
    }
}

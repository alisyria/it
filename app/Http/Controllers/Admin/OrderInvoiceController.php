<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Spatie\Browsershot\Browsershot;

class OrderInvoiceController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,Order $order)
    {
        $order->load([
            'orderProducts'=>function($query){
                $query->with(['product'=>function($query){
                    $query->with('mainImage','productGroup.category','propertyOptions.property');
                }]);
            }
        ]);
        $shop=$order->shop;
        $company=$order->company;
        $infoEmail=$company->contacts->email;
        $infoMobile=$company->contacts->phone;
        $infoWhats=$company->contacts->whatsup;
        $shippingAddress=$order->shippingAddress;

        $view= view('invoices.order',[
            'order'=>$order,
            'shop'=>$shop,
            'company'=>$company,
            'infoEmail'=>$infoEmail,
            'infoMobile'=>$infoMobile,
            'infoWhats'=>$infoWhats,
            'shippingAddress'=>$shippingAddress,
        ]);
        $title=__j('order#:order',[
            'order'=>$order->id
        ]);
        if($request->view_media=='pdf')
        {
            return response()->streamDownload(function()use($view){
                echo Browsershot::html($view->render())
                    ->showBackground()
                    ->noSandbox()
                    ->pdf();
            },$title.".pdf");
        }
        return $view;
    }
}

<?php

namespace App\Http\Controllers\Admin\DataSources;

use App\Http\Controllers\Controller;
use App\Models\Owner;
use Illuminate\Http\Request;

class OwnersDataSource extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $req)
    {
        $query= Owner::orderBy('name')
            ->when($req->has('with_trashed'),fn($q)=>$q->withTrashed())
            ->when(filled($req->term),function($query)use($req){
                $term= trim($req->term);
                $query->where(function ($q)use($term){
                    $q->where('name','like',"%{$term}%");
                });
            });
        $items=$query->paginate($req->input('perpage',20));
        return $items;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Actions\DownloadTranslationsFile;
use App\Enums\TranslationType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DownloadTranslationFile extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $req)
    {
        return app(DownloadTranslationsFile::class)
            ->execute(TranslationType::fromValue($req->translation_type));
    }
}

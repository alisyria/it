<?php
namespace App\Http\Middleware;
use App\Enums\AuthGuard;
use Closure;
use Illuminate\Support\Facades\Auth;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch($guard)
        {
            case AuthGuard::ADMIN:
                if(Auth::guard(AuthGuard::ADMIN)->check())
                {
                    return redirect(adminHome());
                }
                break;
            case AuthGuard::COMPANY:
                if(Auth::guard(AuthGuard::COMPANY)->check())
                {
                    return redirect(companyHome());
                }
                break;
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckForInstance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->wantsJson())// && locale()->isMobilePlaform()
        {

            $instanceUuid=$request->header('A-Instance');
            if(blank($instanceUuid))
            {
                return response()->json(['error'=>'A-Instance Header is Required, it represents uuid of the client app'],422);
            }
            if(!validator(['instance'=>$instanceUuid],['instance'=>'uuid'])->fails())
            {
                locale()->setInstance($instanceUuid);
            }
            else
            {
                return response()->json(['error'=>'invalid instance'],404);
            }
        }
        return $next($request);
    }
}

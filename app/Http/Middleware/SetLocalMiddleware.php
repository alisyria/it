<?php

namespace App\Http\Middleware;

use Closure;
use App;

class SetLocalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,string $locale)
    {
        App::setLocale($locale);

        return $next($request);
    }
}

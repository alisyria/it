<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Carbon\Carbon;
use App\Models\AppLanguage;
use App\Models\Country;
use App\Enums\AuthGuard;
use App\Enums\CookieName;
use Jenssegers\Date\Date;
use App\Enums\Platform;

class LocalizeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->wantsJson())
        {
            $lang=$request->header('Accept-Language');
            if(blank($lang))
            {
                return response()->json(['error'=>'Accept-Language Header is Required'],422);
            }
            if(in_array($lang,AppLanguage::langs()))
            {
                App::setLocale($lang);
                Carbon::setLocale($lang);
                Date::setLocale($lang);
            }
            else
            {
                return response()->json(['error'=>'language not found'],404);
            }

            $platform=$request->header('A-Platform');
            if(blank($platform))
            {
                return response()->json(['error'=>'A-Platform Header is Required'],422);
            }
            if(in_array($platform,Platform::asArray()))
            {
                locale()->setPlatform($platform);
            }
            else
            {
                return response()->json(['error'=>'platform not found'],404);
            }
        }
        else
        {

        }
        return $next($request);
    }
}
//$user=  auth()->user() ?? auth(AuthGuard::API)->user();
//if($user)
//{
//    $country=Country::getAllItems()->where('id',$user->country_id)->first();
//    if($country)
//    {
//        config(['app.timezone'=>$country->timezone]);
//        config(['custom.currency'=>$country->currency_id]);
//        config(['custom.country'=>$country->id]);
//    }
//    App::setLocale($user->lang_id);
//    //dd(config('app.locale'),config('app.timezone'),config('custom.currency'),config('custom.country'));
//    return $next($request);
//}

<?php

namespace App\Http\Livewire\Admin;

use App\Enums\AuthGuard;
use App\Enums\Period;
use App\Repositories\DashboardRepository;
use Illuminate\Support\Collection;
use Livewire\Component;

class Dashboard extends Component
{
    public string $period;

    public function mount()
    {
        $this->period=Period::UNDETERMINED;
    }

    public function getStatsProperty():Collection
    {
        return app(DashboardRepository::class)
            ->getAdminCollectionCounts($this->period);
    }

    public function render()
    {
        return view('livewire.admin.dashboard')
                ->layout('admin.layouts.app');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Order;

class Sidebar extends Component
{

    public function mount()
    {
        $this->refresh();
    }
    public function refresh()
    {

    }
    public function render()
    {
        return view('livewire.admin.sidebar');
    }
}

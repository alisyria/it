<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Notification as NotificationModel;

class Notification extends Component
{
    public NotificationModel $notification;

    public function render()
    {
        return view('livewire.admin.notification')
            ->layout('admin.layouts.app');
    }
}

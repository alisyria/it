<?php

namespace App\Http\Livewire\Admin;

use App\Actions\StoreEmployeeAction;
use App\Enums\Operation;
use App\Enums\Platform;
use App\Enums\UsernameType;
use App\Models\User;
use App\Validators\Forms\Admin\StoreEmployeeValidator;
use Livewire\Component;
use App\Models\Employee;
use Livewire\WithFileUploads;

class Employees extends Component
{
    use WithFileUploads;

    public ?string $first_name=null;
    public ?string $last_name=null;
    public ?string $email=null;
    public ?string $mobile=null;
    public ?string $username=null;
    public ?string $password=null;
    public array $roles=[];
    public $image=null;

    public ?string $imageUrl=null;
    public ?Employee $employee=null;
    public ?User $user=null;
    public bool $isModalOpen=false;
    public string $operation=Operation::CREATE;

    protected $listeners=[
        'edit'
    ];

    public function mount()
    {
        $this->resetInputFields();
    }
    private function resetInputFields():void
    {
        $this->reset('first_name','last_name','email','mobile'
            ,'username','password','roles','image','imageUrl');
        $this->emit('rolesUpdated',[]);
    }
    public function isCreateOpertion():bool
    {
        return $this->operation==Operation::CREATE;
    }
    public function openModal()
    {
        $this->isModalOpen=true;
    }
    public function create()
    {
        $this->openModal();
        $this->resetInputFields();
        $this->resetValidation();
        $this->employee=null;
        $this->operation=Operation::CREATE;
    }
    public function closeModal()
    {
        $this->resetInputFields();
        $this->resetValidation();
        $this->isModalOpen=false;
    }
    public function edit(Employee $employee)
    {
        $this->resetInputFields();
        $this->resetValidation();

        $user=$employee->user;

        $this->first_name=$user->name;
        $this->last_name=$user->last_name;
        $this->username=$user->username;
        $this->mobile=$user->mobile;
        $this->email=$user->email;
        $this->imageUrl=mediaConversionUrl($user->avatar,'medium');
        $this->roles=$user->roles->pluck('id')->toArray();

        $rolesSelectedOptions=[];
        foreach ($user->roles as $role)
        {
            $rolesSelectedOptions[]=[
                'id'=>$role->id,'text'=>$role->name
            ];
        }

        $this->emit('rolesUpdated',$rolesSelectedOptions);

        $this->employee=$employee;
        $this->user=$user;
        $this->operation=Operation::UPDATE;
        $this->openModal();
    }
    public function getValidator():StoreEmployeeValidator
    {
        return new StoreEmployeeValidator(
            $this->operation,Platform::WEB,
            UsernameType::USERNAME,
            data_get($this->user,'id'));
    }
    public function store(StoreEmployeeAction $action)
    {
        $this->validate($this->getValidator()->getRules(),[],$this->getValidator()->getAttributes());

        $action->execute(
            $this->employee,$this->first_name,$this->last_name,UsernameType::USERNAME(),
            $this->username,$this->password,optional($this->image)->getRealPath(),$this->email,$this->mobile,
            $this->roles
        );

        if($this->isCreateOpertion())
        {
            $this->resetInputFields();
            $this->resetValidation();
        }
        else
        {
            $this->closeModal();
        }

        $this->emit('refreshLivewireDatatable');
        $this->emit('success',__j('Saved Successfully'));
    }
    public function getUsernameTypesProperty()
    {
        return [
            UsernameType::USERNAME=>UsernameType::getDescription(UsernameType::USERNAME),
        ];
    }
    public function render()
    {
        return view('livewire.admin.employees')
            ->layout('admin.layouts.app');
    }
}

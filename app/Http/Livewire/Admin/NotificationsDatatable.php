<?php

namespace App\Http\Livewire\Admin;

use App\Repositories\NotificationRepository;
use Livewire\Component;
use App\Models\Notification;
use Livewire\WithPagination;

class NotificationsDatatable extends Component
{
    use WithPagination;

    public int $perpage=1;

    protected $queryString = [
        'perpage'=> ['except' => 1],
    ];
    protected $listeners=[
        'refreshDataTable'
    ];

    public function mount()
    {
        $this->perpage=config('custom.pagination.general_notifications');
    }
    public function refreshDataTable()
    {

    }
    public function delete(Notification $notification)
    {
        $notification->delete();
        $this->emit('success',__j('Deleted Successfully'));
    }
    public function getNotificationsProperty()
    {
        return Notification::general()->latest()
            ->paginate($this->perpage);
    }
    public function render()
    {
        return view('livewire.admin.notifications-datatable');
    }
}

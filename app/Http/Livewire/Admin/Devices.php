<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Devices extends Component
{
    public function render()
    {
        return view('livewire.admin.devices')
            ->layout('admin.layouts.app');
    }
}

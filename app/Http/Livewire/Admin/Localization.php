<?php

namespace App\Http\Livewire\Admin;

use App\Actions\DownloadTranslationsFile;
use App\Enums\Disk;
use App\Enums\TranslationType;
use App\Imports\Localization\AppTranslationsImport;
use App\Models\AppLanguage;
use BenSampo\Enum\Rules\EnumValue;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\HeadingRowImport;

class Localization extends Component
{
    use WithFileUploads;

    public bool $isModalOpen=false;
    public ?string $translation_type=null;
    public $file;
    public $fileUrl=null;
    public $fileName=null;

    public function updatedFile()
    {
        $this->resetValidation();
        $this->validateOnly('file',$this->rules(),[],$this->attributes());
        $this->fileUrl="#";
        $this->fileName=$this->file->getClientOriginalName();
    }
    public function openUploadModal()
    {
        $this->isModalOpen=true;
    }
    public function closeModal()
    {
        $this->isModalOpen=false;
    }
    public function upload()
    {
        $this->validate($this->rules(),[],$this->attributes());
        $fileName=str_random(8).'.'.$this->file->getClientOriginalExtension();
        $this->file->storeAs('',$fileName,Disk::TEMPORARY);
        $filePath=\Storage::disk(Disk::TEMPORARY)->path($fileName);
        $headings = (new HeadingRowImport)->toArray($filePath)[0][0];
        if($this->translationType->is(TranslationType::GENERAL()))
        {
            if(!in_array('group',$headings))
            {
                $this->emit('error',__('translations.messages.excel_group_required'));
                return ;
            }
        }
        if(!in_array('key',$headings))
        {
            $this->emit('error',__('translations.messages.excel_key_required'));
            return ;
        }
        $headingWithoutDefault=array_diff($headings,['group','key','default']);
        $headingWithoutDefault=array_filter($headingWithoutDefault,function($v,$k){
            return $v;
        },ARRAY_FILTER_USE_BOTH);
        if(count(array_diff($headingWithoutDefault, AppLanguage::langs())))
        {
            $this->emit('error',__('translations.messages.langs_not_supported'));
            return ;
        }
        (new AppTranslationsImport($this->translationType))->import($fileName,Disk::TEMPORARY);
        \Storage::disk(Disk::TEMPORARY)->delete($fileName);

        $this->emit('success',__('translations.messages.success_update'));
        $this->resetValidation();
        $this->reset();
        $this->closeModal();
    }
    public function getTranslationTypesProperty()
    {
        return TranslationType::asSelectArray();
    }
    public function getTranslationTypeProperty()
    {
        return TranslationType::GENERAL();
    }
    public function getAppLanguagesProperty()
    {
        return AppLanguage::getSupportedLocales();
    }
    private function rules():array
    {
        return [
            'file'=>['required','file','mimes:xls,xlsx']
        ];
    }
    private function attributes():array
    {
        return [
            'translation_type'=>'Type',
        ];
    }
    public function download(DownloadTranslationsFile $action,string $translationType)
    {
        return $action->execute(
            TranslationType::fromValue($translationType)
        );
    }

    public function render()
    {
        return view('livewire.admin.localization')
            ->layout('admin.layouts.app');
    }
}

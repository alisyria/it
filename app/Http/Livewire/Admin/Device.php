<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Device as DeviceModel;

class Device extends Component
{
    public DeviceModel $device;

    public function render()
    {
        return view('livewire.admin.device')
            ->layout('admin.layouts.app');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use App\Actions\StoreAdminProfileAction;
use App\Enums\Operation;
use App\Enums\Platform;
use App\Enums\UsernameType;
use App\Models\User;
use App\Models\Employee;
use App\Validators\Forms\Admin\StoreProfileValidator;
use App\Validators\Forms\Admin\StoreRegionValidator;
use Livewire\Component;
use Livewire\WithFileUploads;

class Profile extends Component
{
    use WithFileUploads;

    public ?string $first_name=null;
    public ?string $last_name=null;
    public ?string $email=null;
    public ?string $mobile=null;
    public ?string $username_type=null;
    public ?string $username=null;
    public ?string $new_password=null;
    public ?string $new_password_confirmation=null;
    public ?string $current_password=null;
    public $image=null;

    public ?string $imageUrl=null;
    public ?Employee $employee=null;
    public ?User $user=null;
    public string $operation=Operation::UPDATE;

    public function mount()
    {
        $this->user=adminUser();
        $this->resetInputFields();
    }
    private function resetInputFields():void
    {
        $this->first_name=$this->user->name;
        $this->last_name=$this->user->last_name;
        $this->email=$this->user->email;
        $this->mobile=$this->user->mobile;
        $this->username_type=$this->user->username_type;
        $this->username=$this->user->username;
        $this->imageUrl=mediaConversionUrl($this->user->avatar,\App\Enums\ImageConversion::SMALL) ?? $this->user->getFirstMediaUrl(\App\Models\User::MEDIA_AVATAR);

        $this->reset('new_password','new_password_confirmation','current_password','image');
    }
    public function getUsernameTypesProperty()
    {
        return [
            UsernameType::Email=>UsernameType::getDescription(UsernameType::Email),
            UsernameType::Mobile=>UsernameType::getDescription(UsernameType::Mobile),
        ];
    }
    public function getValidator():StoreProfileValidator
    {
        return new StoreProfileValidator($this->operation,Platform::WEB,$this->username_type,adminUser());
    }
    public function store(StoreAdminProfileAction $action)
    {
        $this->validate($this->getValidator()->getRules(),[],$this->getValidator()->getAttributes());

        $this->user=$action->execute(adminUser(),$this->first_name,$this->last_name,$this->email,$this->mobile,
            UsernameType::fromValue($this->username_type),$this->username,$this->new_password,
            optional($this->image)->getRealPath());
        $this->image=null;
        $this->imageUrl=mediaConversionUrl($this->user->avatar,\App\Enums\ImageConversion::SMALL) ?? $this->user->getFirstMediaUrl(\App\Models\User::MEDIA_AVATAR);
        $this->emit('success',__j('Saved Successfully'));
    }
    public function render()
    {
        return view('livewire.admin.profile')
            ->layout('admin.layouts.app');
    }
}

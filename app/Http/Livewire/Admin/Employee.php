<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\User;
use App\Models\Employee as EmployeeModel;

class Employee extends Component
{
    public EmployeeModel $employee;
    public ?User $user=null;

    public function mount()
    {
        $this->user=$this->employee->user;
    }

    public function getTitleProperty()
    {
        return __j('Employee').' - '.$this->user->full_name;
    }

    public function render()
    {
        return view('livewire.admin.employee')
            ->layout('admin.layouts.app');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Role as RoleModel;

class Role extends Component
{
    public RoleModel $role;

    public function getTitleProperty()
    {
        return __j('Role').' - '.$this->role->name;
    }
    public function render()
    {
        return view('livewire.admin.role')
            ->layout('admin.layouts.app');
    }
}

<?php

namespace App\Http\Livewire\Admin\DataTables;

use App\Actions\DeleteEmployeeAction;
use App\Models\Employee;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class Employees extends LivewireDatatable
{
    public function builder()
    {
        return Employee::query()
            ->leftJoin('users','employees.id','users.id');
    }

    public function columns()
    {
        return [
            Column::name('id')
                ->label(__j('ID'))
                ->searchable()
                ->filterable(),

            Column::raw("CONCAT(users.name,' ',users.last_name) AS user_name")
                ->label(__j('Name'))
                ->searchable()
                ->filterable()
                ->truncate(25),

            Column::raw("users.username AS username")
                ->label(__j('Username'))
                ->searchable()
                ->filterable()
                ->truncate(25),

            Column::raw("users.mobile AS user_mobile")
                ->label(__j('Mobile'))
                ->searchable()
                ->filterable()
                ->truncate(25),

            Column::callback(['id'], function ($id) {
                return view('partials.table-actions', ['id' => $id]);
            })->label(__j('Actions')),
        ];
    }

    public function getShowUrl($id):string
    {
        return route('admin.employees.show',[$id]);
    }
    public function deleteModel(DeleteEmployeeAction $action,Employee $employee)
    {
        $action->execute($employee);

        $this->emit('success',$action->getSuccessMessage());
    }
}

<?php

namespace App\Http\Livewire\Admin\DataTables;

use App\Actions\DeleteRoleAction;
use App\Models\AppLanguage;
use App\Models\Role;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class Roles extends LivewireDatatable
{

    public function builder()
    {
        return Role::allowed();
    }

    public function columns()
    {
        $defaultLang=AppLanguage::getDefaultLocal();

        return [
            Column::name('id')
                ->label(__j('ID'))
                ->searchable()
                ->filterable(),

            Column::name('name')
                ->label(__j('Name'))
                ->searchable()
                ->filterable()
                ->truncate(25),

            Column::raw("JSON_UNQUOTE(JSON_EXTRACT(roles.description,'$.$defaultLang')) AS description")
                ->label(__j('Description'))
                ->truncate(70),

            Column::callback(['id'], function ($id) {
                return view('partials.table-actions', ['id' => $id]);
            })->label(__j('Actions')),
        ];
    }

    public function getShowUrl($id):string
    {
        return route('admin.roles.show',[$id]);
    }
    public function deleteModel(DeleteRoleAction $action,Role $role)
    {
        $action->execute($role);

        $this->emit('success',$action->getSuccessMessage());
    }
}

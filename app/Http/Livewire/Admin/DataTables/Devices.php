<?php

namespace App\Http\Livewire\Admin\DataTables;

use App\Enums\DeviceStatus;
use App\Exports\DevicesExport;
use App\Extensions\CustomLivewireDatatable;
use App\Models\Department;
use App\Models\Device;
use App\Models\DeviceType;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class Devices extends CustomLivewireDatatable
{
    public $model=Device::class;

    public $hideable = 'select';
    public $exportable=true;

    public function builder()
    {
        return Device::query()
            ->with(['owner.department','deviceType','deviceProperties'=>fn($q)=>$q->with('property','option')])
            ->leftJoin('device_types', 'device_types.id', 'devices.device_type_id')
            ->leftJoin('owners', 'owners.id', 'devices.owner_id')
            ->leftJoin('departments', 'departments.id', 'owners.department_id');
    }
    public function export()
    {
        return (new DevicesExport($this->builder()))
            ->download(__j('الأجهزة').'.xlsx');
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')
                ->label(__j('ID'))
                ->searchable()
                ->filterable()
                ->defaultSort(),
            Column::name('serial_number')
                ->label('معرف الجهاز')
                ->searchable()
                ->filterable(),
            Column::name('state')
                ->label(__j('Status'))
                ->filterable($this->statuses)
                ->view('admin.partials.data-tables.device-status'),
            Column::name('model')
                ->label(__j('Model'))
                ->searchable()
                ->filterable()
                ->hide(),
            Column::name('device_types.name')
                ->label(__j('Device Type'))
                ->filterable($this->deviceTypes),
            Column::raw("CONCAT(owners.name, ' ',owners.last_name) AS owner_name")
                ->label('المستثمر')
                ->filterable()
                ->searchable(),
            Column::name('departments.name')
                ->label('الدائرة')
                ->filterable($this->departments),
            Column::callback(['id'], function ($id) {
                return view('partials.devices-table-actions', [
                    'id' => $id,
                ]);
            })->label(__j('Actions'))
        ];
    }
    public function getShowUrl($id):string
    {
        return route('admin.devices.show',[$id]);
    }
    public function deleteModel(Device $device)
    {
        $device->delete();

        $this->emit('success',__j('Deleted Successfully'));
    }

    public function getStatusesProperty()
    {
        return DeviceStatus::asSelectArray();
    }
    public function getDeviceTypesProperty()
    {
        return DeviceType::pluck('name')->toArray();
    }
    public function getDepartmentsProperty()
    {
        return Department::pluck('name')->toArray();
    }
}

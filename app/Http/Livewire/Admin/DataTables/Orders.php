<?php

namespace App\Http\Livewire\Admin\DataTables;

use App\Enums\OrderStatus;
use App\Extensions\CustomLivewireDatatable;
use App\Models\Department;
use App\Models\DeviceType;
use App\Models\Order;
use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class Orders extends CustomLivewireDatatable
{
    public $model = Order::class;
    public $hideable = 'select';

    public function builder()
    {
        return Order::query()
                ->with([
                    'device' => fn($q) => 'owner',
                    'employee',
                ])
            ->leftJoin('devices', 'devices.id', 'orders.device_id')
            ->leftJoin('device_types', 'device_types.id', 'devices.device_type_id')
            ->leftJoin('owners', 'owners.id', 'devices.owner_id')
            ->leftJoin('departments', 'departments.id', 'owners.department_id')
            ->leftJoin('employees', 'employees.id', 'orders.employee_id')
            ->leftJoin('users as employee_user', 'employee_user.id', 'employees.id');
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')
                ->label('الرقم')
                ->searchable()
                ->filterable()
                ->defaultSort(),
            Column::name('state')
                ->label('الحالة')
                ->filterable($this->statuses)
                ->view('admin.partials.data-tables.order-status'),
            Column::raw("CONCAT(employee_user.name, ' ',employee_user.last_name) AS employee_user_name")
                ->label('الموظف')
                ->filterable()
                ->searchable(),
            Column::name('device_types.name')
                ->label('نوع الجهاز')
                ->filterable($this->deviceTypes),
            Column::name('departments.name')
                ->label('الدائرة')
                ->filterable($this->departments),
            DateColumn::name('requested_at')
                ->label('توقيت الطلب')
                ->filterable(),
            Column::callback(['id'], function ($id) {
                return view('partials.orders-table-actions', [
                    'id' => $id,
                ]);
            })->label(__j('Actions'))
        ];
    }

    public function getStatusesProperty()
    {
        return OrderStatus::asSelectArray();
    }
    public function getDeviceTypesProperty()
    {
        return DeviceType::pluck('name')->toArray();
    }
    public function getDepartmentsProperty()
    {
        return Department::pluck('name')->toArray();
    }
}

<?php

namespace App\Http\Livewire\Admin\Auth;

use App\Enums\AuthGuard;
use App\Enums\UsernameType;
use App\Enums\UserType;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public string $username = '';

    public string $password = '';

    public bool $remember = false;

    public function authenticate()
    {
        $credentials = $this->validate([
            'username' => ['required',],
            'password' => ['required',],
        ]);

        $user=User::active()->findUser(UsernameType::USERNAME,$this->username,[UserType::SUPER_ADMIN,UserType::EMPLOYEE])->first();

        if (!$user || !Auth::guard(AuthGuard::ADMIN)->attempt([
            'username'=>$this->username,'password'=>$this->password,
            'user_type'=>$user->user_type->value,
            ], $this->remember)) {
                $this->addError('username', trans('auth.failed'));

            return;
        }

        redirect()->intended(adminHome());
    }

    public function render()
    {
        return view('livewire.admin.auth.login')
                ->layout('admin.layouts.auth');
    }
}

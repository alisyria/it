<?php

namespace App\Http\Livewire\Admin;

use App\Actions\SendGeneralNotification;
use App\Enums\NotificationTarget;
use App\Enums\Operation;
use App\Enums\Platform;
use App\Enums\VendorType;
use App\Models\AppLanguage;
use App\Models\Notification;
use App\Validators\Forms\Admin\StoreNotificationValidator;
use Livewire\Component;
use Livewire\WithFileUploads;

class Notifications extends Component
{
    use WithFileUploads;

    public int $perpage=1;

    public $target=null;
    public $vendor_type=null;
    public $title=[];
    public $text=[];
    public $governorate_id=null;
    public $region_id=null;
    public $image=null;

    public ?string $imageUrl=null;
    public int $imageUploadIteration=0;

    protected $queryString = [
        'perpage'=> ['except' => 1],
    ];

    public function mount()
    {
        $this->resetInputFields();
        $this->perpage=config('custom.pagination.general_notifications');
    }
    private function resetInputFields():void
    {
        $this->resetTitleField();
        $this->resetTextField();
        $this->reset('target','vendor_type','governorate_id',
            'region_id','image','imageUrl');
        $this->imageUploadIteration++;
    }
    private function resetTitleField()
    {
        $this->title=[];
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $this->title[$lang->id]="";
        }
    }
    private function resetTextField()
    {
        $this->text=[];
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $this->text[$lang->id]="";
        }
    }
    public function getValidator():StoreNotificationValidator
    {
        return new StoreNotificationValidator(Operation::CREATE,Platform::WEB,
            $this->getPropertyValue('governorate_id'));
    }
    public function send(SendGeneralNotification $action)
    {
        $this->resetValidation();
        $this->validate($this->getValidator()->getRules(),[],$this->getValidator()->getAttributes());

        $governorates=$this->governorate_id?[$this->governorate_id]:[];
        $regions=$this->region_id?[$this->region_id]:[];

        $action->execute(NotificationTarget::coerce($this->target),VendorType::coerce($this->vendor_type),
        $this->title,$this->text,$governorates,$regions,optional($this->image)->getRealPath());

        $this->resetInputFields();
        $this->emit('refreshDataTable');
        $this->emit('success',$action->getSuccessMessage());
    }
    public function delete($id)
    {
        $notification=Notification::find($id);
        $notification->delete();
        $this->emit('success',__j('Deleted Successfully'));
    }
    public function getTargetsProperty()
    {
        return NotificationTarget::asSelectArray();
    }

    public function render()
    {
        $this->emit('pagination');

        return view('livewire.admin.notifications')
                ->layout('admin.layouts.app');
    }
}

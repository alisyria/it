<?php

namespace App\Http\Livewire\Admin\Settings;

use App\Models\Setting;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class AboutUs extends Component
{
    use WithFileUploads;

    public array $app_name;

    public function mount()
    {
        $this->app_name=setting()->getAllValues('app_name');
    }

    public function store()
    {
        Setting::updateOrCreate(['key'=>'app_name'], [
            'value'=>$this->app_name
        ]);

        $this->emit('success',__j('Saved Successfully'));
    }
    public function render()
    {
        return view('livewire.admin.settings.about-us')
                ->layout('admin.layouts.auth');
    }
}

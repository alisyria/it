<?php

namespace App\Http\Livewire\Admin\Modals;

use App\Actions\StoreOrderAction;
use App\Enums\OrderStatus;
use App\Models\Order;
use App\Models\Section;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use LivewireUI\Modal\ModalComponent;

class SaveOrderModal extends ModalComponent
{
    public $order = null;
    public $title = null;
    public $description = null;
    public $state = null;
    public $device_id = null;
    public $employee_id = null;
    public $items=[];

    public function mount(Order $order = null)
    {
        if (optional($order)->id) {
            $this->order = $order;
        }

        if($order) {
            $this->seed($order);
        } else {
            $this->order = null;
        }
    }
    public function seed(Order $order)
    {
        $this->resetFields();

        $this->title = $order->title;
        $this->description = $order->description;
        $this->device_id = $order->device_id;
        $this->employee_id = $order->employee_id;
        $this->state = $order->state->getStatus()->value;
        foreach ($order->items as $item)
        {
            $this->items[$item->id] = $item->pivot->value;
        }
    }
    public function render()
    {
        return view('livewire.admin.modals.save-order-modal');
    }

    public function getRules()
    {
        return [
            'title'=> [
                'required','max:500'
            ],
            'description'=> [
                'nullable',
            ],
            'state'=> [
                'required', new EnumValue(OrderStatus::class),
            ],
            'device_id'=> [
                'required', Rule::exists('devices','id')->whereNull('deleted_at'),
            ],
            'employee_id'=> [
                'required', Rule::exists('employees','id')->whereNull('deleted_at'),
            ],
            'items'=> [
                'nullable','array'
            ],
        ];
    }
    public function getValidationAttributes()
    {
        return __('fields.admin.order');
    }

    public function resetFields()
    {
        $this->reset(
            'title', 'description', 'state', 'device_id', 'employee_id',
            'items',
        );
    }
    public function store(StoreOrderAction $action)
    {
        $this->resetValidation();
        $this->validate($this->getRules(),[],$this->getValidationAttributes());

        $action->execute(
            $this->order, $this->title, $this->description, OrderStatus::fromValue($this->state),
            $this->device_id,$this->employee_id,$this->items ?? []
        );

        if(optional($this->order)->id)
        {
            $this->closeModal();
        }

        $this->resetFields();
        $this->emit('refreshLivewireDatatable');
        $this->emit('success','تم الحفظ بنجاح');
    }
    public static function modalMaxWidth(): string
    {
        return '5xl';
    }
    public function pageTitle(): string
    {
        if ($this->order) {
            return "طلب ". ' #' . $this->order->id;
        }
        return 'إنشاء طلب جديد';
    }

    public function getSectionsProperty(): Collection
    {
        return Section::query()
            ->with('items')
            ->get();
    }
}

<?php

namespace App\Http\Livewire\Admin\Modals;

use App\Actions\StoreOwnerAction;
use App\Models\Owner;
use Illuminate\Validation\Rule;
use LivewireUI\Modal\ModalComponent;

class SaveOwnerModal extends ModalComponent
{

    public ?string $name=null;
    public ?string $last_name=null;
    public ?int $department_id=null;

    public function store(StoreOwnerAction $action)
    {
        $this->resetValidation();
        $this->validate($this->getRules(),[],$this->getValidationAttributes());

        $ownerIsAlreadyExists=Owner::where('name',$this->name)->where('last_name',$this->last_name)
            ->where('department_id',$this->department_id)->exists();
        if($ownerIsAlreadyExists)
        {
            $this->addError('name','هذاالمالك موجود من قبل');
            return;
        }
        $owner=$action->execute($this->name,$this->last_name,$this->department_id);

        $this->closeModalWithEvents([
            SaveDeviceModal::getName()=>[
                'ownerAdded',[$owner->id]
            ]
        ]);
        $this->resetFields();
        $this->emit('success','تم الحفظ بنجاح');
    }
    public function getRules()
    {
        return [
            'name'=> [
                'required','max:200'
            ],
            'last_name'=> [
                'required','max:200'
            ],
            'department_id'=> [
                'required',Rule::exists('departments','id')
            ],
        ];
    }
    public function getValidationAttributes()
    {
        return __('fields.admin.owner');
    }
    public function resetFields()
    {
        $this->reset('name','department_id');
    }
    public function render()
    {
        return view('livewire.admin.modals.save-owner-modal');
    }

    public static function closeModalOnClickAway(): bool
    {
        return false;
    }
    public static function modalMaxWidth(): string
    {
        return '4xl';
    }
}

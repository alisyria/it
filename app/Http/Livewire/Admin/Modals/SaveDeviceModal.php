<?php

namespace App\Http\Livewire\Admin\Modals;

use App\Actions\StoreDeviceAction;
use App\Models\Device;
use App\Models\DeviceType;
use App\Models\Owner;
use App\Rules\ModelExistRule;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use LivewireUI\Modal\ModalComponent;

class SaveDeviceModal extends ModalComponent
{
    public $device=null;
    public $serial_number=null;
    public $model=null;
    public $manufacturer=null;
    public $device_type_id=null;
    public $owner_id=null;
    public $options=[];
    public $listeners=[
        'ownerAdded'
    ];

    public function mount(Device $device=null)
    {
        $this->device=$device;
        if($device)
        {
            $this->seed($device);
        }
        else
        {
            $this->device=null;
        }
    }
    public function seed(Device $device)
    {
        $this->resetFields();

        $this->serial_number=$device->serial_number;
        $this->model=$device->model;
        $this->manufacturer=$device->manufacturer;
        $this->device_type_id=$device->device_type_id;
        $this->owner_id=$device->owner_id;
        foreach ($device->deviceProperties as $deviceProperty)
        {
            $this->options[$deviceProperty->property_id]=$deviceProperty->option_id ?? $deviceProperty->value;
        }
    }
    public function ownerAdded($ownerId)
    {
        $this->owner_id=$ownerId;
    }
    public function render()
    {
        return view('livewire.admin.modals.save-device-modal');
    }
    public function getRules()
    {
        return [
            'serial_number'=> [
                'required','max:50'
            ],
            'model'=> [
                'nullable','max:200'
            ],
            'manufacturer'=> [
                'nullable','max:200'
            ],
            'device_type_id'=> [
                'required',Rule::exists('device_types','id')->whereNull('deleted_at'),
            ],
            'owner_id'=> [
                'required',Rule::exists('owners','id')->whereNull('deleted_at'),
            ],
            'options'=> [
                'nullable','array'
            ],
        ];
    }
    public function getValidationAttributes()
    {
        return __('fields.admin.device');
    }
    public function resetFields()
    {
        $this->reset('serial_number','model','manufacturer','device_type_id','owner_id','options');
    }
    public function store(StoreDeviceAction $action)
    {
        $this->resetValidation();
        $this->validate($this->getRules(),[],$this->getValidationAttributes());

        $action->execute($this->device,$this->serial_number,$this->model,$this->manufacturer,
            $this->device_type_id,$this->owner_id,$this->options ?? []);

        if(optional($this->device)->id)
        {
            $this->closeModal();
        }
        $this->resetFields();
        $this->emit('refreshLivewireDatatable');
        $this->emit('success','تم الحفظ بنجاح');
    }
    public function getPropertiesProperty():Collection
    {
        if(!$this->device_type_id)
        {
            return collect([]);
        }

        return DeviceType::find($this->device_type_id)
            ->properties()
            ->with('options')
            ->get();
    }
    public static function closeModalOnClickAway(): bool
    {
        return false;
    }
    public static function modalMaxWidth(): string
    {
        return '4xl';
    }
}

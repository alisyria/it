<?php

namespace App\Http\Livewire\Admin;

use App\Actions\StoreRoleAction;
use App\Enums\AuthGuard;
use App\Enums\Operation;
use App\Enums\Platform;
use App\Models\AppLanguage;
use App\Models\Permission;
use App\Validators\Forms\Admin\StoreRoleValidator;
use Livewire\Component;
use App\Models\Role;

class Roles extends Component
{
    public ?string $name=null;
    public ?array $description=[];
    public array $permissions=[];

    public ?Role $role=null;
    public bool $isModalOpen=false;
    public string $operation=Operation::CREATE;

    protected $listeners=[
        'edit'
    ];

    public function mount()
    {
        $this->resetInputFields();
    }
    private function resetInputFields():void
    {
        $this->reset('name','permissions');
        $this->resetDescriptionField();
    }
    private function resetDescriptionField()
    {
        $this->description=[];
        foreach (AppLanguage::getSupportedLocales() as $lang)
        {
            $this->description[$lang->id]="";
        }
    }
    public function isCreateOpertion():bool
    {
        return $this->operation==Operation::CREATE;
    }
    public function openModal()
    {
        $this->isModalOpen=true;
    }
    public function create()
    {
        $this->openModal();
        $this->resetInputFields();
        $this->resetValidation();
        $this->operation=Operation::CREATE;
        $this->role=null;
    }
    public function closeModal()
    {
        $this->resetInputFields();
        $this->resetValidation();
        $this->isModalOpen=false;
    }
    public function edit(Role $role)
    {
        $this->resetInputFields();
        $this->resetValidation();

        $this->name=$role->name;
        $this->description=$role->getTranslations('description');
        $permissionIds=$role->permissions()->pluck('id')->toArray();
        $permissionIdsWithValues=[];
        foreach ($permissionIds as $permissionId)
        {
            $permissionIdsWithValues[$permissionId]=$permissionId;
        }
        $this->permissions=$permissionIdsWithValues;
        $this->role=$role;
        $this->operation=Operation::UPDATE;
        $this->openModal();
    }
    public function getValidator():StoreRoleValidator
    {
        return new StoreRoleValidator($this->operation,Platform::WEB,data_get($this->role,'id'));
    }
    public function store(StoreRoleAction $action)
    {
        $this->validate($this->getValidator()->getRules(),[],$this->getValidator()->getAttributes());

        $action->execute(
            $this->role,$this->name,$this->description,$this->permissions,AuthGuard::ADMIN()
        );

        if($this->isCreateOpertion())
        {
            $this->resetInputFields();
            $this->resetValidation();
        }
        else
        {
            $this->closeModal();
        }

        $this->emit('refreshLivewireDatatable');
        $this->emit('success',__j('Saved Successfully'));
    }
    public function getPermissionsModelsProperty()
    {
        return Permission::allowed()->get();
    }
    public function render()
    {
        return view('livewire.admin.roles')
            ->layout('admin.layouts.app');
    }
}

<?php

namespace App\Http\Resources\Datatables\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'points'=>$this->points,
            'wallet'=>[
                'balance'=>data_get($this,'wallet.balance'),
            ],
            'user'=>$this->whenLoaded('user',function (){
                $user=$this->user;
                return[
                    'avatar'=>view('admin.shared.datatables.images.user-avatar',[
                        'user'=>$user
                    ])->render(),
                    'full_name'=>$user->full_name,
                    'username'=>$user->username,
                    'email'=>$user->email,
                    'created_at'=>$user->created_at->format('Y-m-d H:i'),
                ];
            }),
            'status'=>view('admin.shared.components.labels.user-status',[
                'user'=>$this->user
            ])->render(),
            'actions'=> view('admin.shared.datatables.columns.customer-actions',[
                'customer'=>$this
            ])->render(),
        ];
    }
}

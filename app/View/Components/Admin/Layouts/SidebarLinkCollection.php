<?php

namespace App\View\Components\Admin\Layouts;

use Illuminate\View\Component;

class SidebarLinkCollection extends Component
{
    public string $title;
    public bool $active;
    public ?array $permissions;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $title,bool $active=false,array $permissions=null)
    {
        $this->title=$title;
        $this->active=$active;
        $this->permissions=$permissions;
    }
    public function show():bool
    {
        if(is_null($this->permissions))
        {
            return true;
        }

        $show=false;
        foreach ($this->permissions as $permission)
        {
            if(currentWebUser()->can($permission))
            {
                return true;
            }
        }
        return $show;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.admin.layouts.sidebar-link-collection');
    }
}

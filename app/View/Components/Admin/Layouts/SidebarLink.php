<?php

namespace App\View\Components\Admin\Layouts;

use Illuminate\View\Component;

class SidebarLink extends Component
{
    public string $title;
    public string $href;
    public bool $active;
    public ?string $permission;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $title,string $href,bool $active=false,$permission=null)
    {
        $this->title=$title;
        $this->href=$href;
        $this->active=$active;
        $this->permission=$permission;
    }
    public function show():bool
    {
        if(is_null($this->permission))
        {
            return true;
        }
        return currentWebUser()->can($this->permission);
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.admin.layouts.sidebar-link');
    }
}

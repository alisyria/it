<?php

namespace App\View\Components\Admin\Select;

use App\Models\Role;
use Illuminate\View\Component;

class Roles extends Component
{
    public string $id;
    public string $name;
    public string $liveName;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name,string $liveName=null,string $id=null)
    {
        $this->name=$name;
        $this->liveName=$liveName ?? $name;
        $this->id=$id ?? $liveName ?? $name;
    }

    public function options()
    {
        return Role::allowed()->get();
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.admin.select.roles');
    }
}

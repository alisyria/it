<?php

namespace App\View\Components\Select;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class DeviceType extends Component
{
    public string $id;
    public string $name;
    public string $liveName;
    public bool $lazyUpdate;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name,string $liveName=null,string $id=null,bool $lazyUpdate=false)
    {
        $this->name=$name;
        $this->liveName=$liveName ?? $name;
        $this->id=$id ?? $liveName ?? $name;
        $this->lazyUpdate=$lazyUpdate;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.select.device-type');
    }

    public function deviceTypes():Collection
    {
        return \App\Models\DeviceType::getAllItems();
    }
}

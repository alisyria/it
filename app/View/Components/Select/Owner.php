<?php

namespace App\View\Components\Select;

use Illuminate\View\Component;

class Owner extends Component
{
    public string $name;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name='owner_id')
    {
        $this->name=$name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.select.owner');
    }
}

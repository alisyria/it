<?php

namespace App\View\Components\Select;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class Device extends Component
{
    public string $id;
    public string $name;
    public string $liveName;
    public bool $lazyUpdate;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name,string $liveName=null,string $id=null,bool $lazyUpdate=false)
    {
        $this->name=$name;
        $this->liveName=$liveName ?? $name;
        $this->id=$id ?? $liveName ?? $name;
        $this->lazyUpdate=$lazyUpdate;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.select.device');
    }

    public function items():Collection
    {
        return \App\Models\Device::with('deviceType', 'owner.department')
            ->get()
            ->transform(function (\App\Models\Device $device) {
                $device->name = $device->id . ' - ' . data_get($device, 'deviceType.name'). ' - ' . data_get($device, 'owner.full_name'). ' - ' . data_get($device, 'owner.department.name');

                return $device;
            });
    }
}

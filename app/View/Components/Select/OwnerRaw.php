<?php

namespace App\View\Components\Select;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class OwnerRaw extends Component
{
    public string $id;
    public string $name;
    public string $liveName;
    public bool $lazyUpdate;
    public ?string $class;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name,string $liveName=null,string $id=null,bool $lazyUpdate=false,
                                string $class=null)
    {
        $this->name=$name;
        $this->liveName=$liveName ?? $name;
        $this->id=$id ?? $liveName ?? $name;
        $this->lazyUpdate=$lazyUpdate;
        $this->class=$class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.select.owner-raw');
    }
    public function owners():Collection
    {
        return \App\Models\Owner::getAllItems()->load('department')->sortBy('department_id');
    }
}

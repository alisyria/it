<?php

namespace App\View\Components\Form;

use App\Models\AppLanguage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;

class CkEditorLangs extends Component
{
    public ?Model $model;
    public string $defaultLang;
    public string $name;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Model $model=null,string $defaultLang=null,string $name)
    {
        $this->model=$model;
        $this->defaultLang=$defaultLang ?? AppLanguage::getDefaultLocal();
        $this->name=$name;
    }
    public function langs():array
    {
        return AppLanguage::langsSelect();
    }
    public function translation(string $lang)
    {
        if(is_null($this->model))
        {
            return null;
        }
        return $this->model->getTranslation($this->name,$lang,false);
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form.ck-editor-langs');
    }
}

<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Img extends Component
{
    public string $liveName;
    public ?string $temporaryUrl;
    public ?string $mimes;
    public ?string $id;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $liveName,string $temporaryUrl=null,string $mimes='image/*',string $id=null)
    {
        $this->liveName=$liveName;
        $this->temporaryUrl=$temporaryUrl;
        $this->mimes=$mimes;
        $this->id=$id ?? $liveName;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form.img');
    }
}

<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class DateTimePicker extends Component
{
    public string $id;
    public string $name;
    public string $liveName;
    public bool $timePicker;
    public int $timePickerIncrement;
    public ?string $placeholder;
    public ?string $minDate;
    public ?string $maxDate;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name,string $liveName=null,string $id=null,bool $timePicker=true,
                                int $timePickerIncrement,string $placeholder=null,string $minDate=null,string $maxDate=null)
    {
        $this->name=$name;
        $this->liveName=$liveName ?? $name;
        $this->id=$id ?? $liveName ?? $name;
        $this->timePicker=$timePicker;
        $this->timePickerIncrement=$timePickerIncrement;
        $this->placeholder=$placeholder ?? __j('select date');
        $this->minDate=$minDate;
        $this->maxDate=$maxDate;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.date-time-picker');
    }
}

<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class CoordinatesPicker extends Component
{
    public string $id;
    public ?string $latitudeId;
    public string $latitudeName;
    public ?string $latitudeLiveName;
    public ?string $longitudeId;
    public string $longitudeName;
    public ?string $longitudeLiveName;
    public float $latitude;
    public float $longitude;
    public int $zoom;
    public string $addressId;
    public string $addressLiveName;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id,float $latitude,float $longitude,int $zoom=15,
       string $latitudeId=null,string $latitudeName='latitude',string $latitudeLiveName=null,
       string $longitudeId=null,string $longitudeName='longitude',string $longitudeLiveName=null,
       string $addressId='map-address',string $addressLiveName)
    {
        $this->id=$id;
        $this->latitude=$latitude;
        $this->longitude=$longitude;
        $this->zoom=$zoom;
        $this->latitudeId=$latitudeId ?? $latitudeName;
        $this->latitudeName=$latitudeName;
        $this->latitudeLiveName=$latitudeLiveName ?? $latitudeName;
        $this->longitudeId=$longitudeId ?? $longitudeName;
        $this->longitudeName=$longitudeName;
        $this->longitudeLiveName=$longitudeLiveName ?? $longitudeName;
        $this->addressId=$addressId;
        $this->addressLiveName=$addressLiveName;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.coordinates-picker');
    }
}

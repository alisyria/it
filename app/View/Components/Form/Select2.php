<?php

namespace App\View\Components\Form;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Select2 extends Component
{
    public string $id;
    public string $name;
    public string $liveName;
    public bool $multiple;
    public ?string $dataSourceRoute;
    public string $placeholder;
    public bool $lazyUpdate;
    public $options;
    public bool $hasDataSource;
    public string $key='id';
    public string $value='name';
    public bool $useTextAsKey;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name,string $liveName=null,string $id=null,string $dataSourceRoute=null,
         string $placeholder='',$lazyUpdate=false,bool $multiple=false,$options=null,
         string $key='id',string $value='name',bool $useTextAsKey=false)
    {
        $this->name=$name;
        $this->liveName=$liveName ?? $name;
        $this->id=$id ?? $liveName ?? $name;
        $this->dataSourceRoute=$dataSourceRoute;
        $this->placeholder=$placeholder;
        $this->lazyUpdate=$lazyUpdate;
        $this->multiple=$multiple;
        $this->options=$options;
        $this->hasDataSource=(bool)$dataSourceRoute;
        $this->key=$key;
        $this->value=$value;
        $this->useTextAsKey=$useTextAsKey;
    }

    public function isOptionsArray():bool
    {
        return is_array($this->options);
    }
    public function isOptionsCollection():bool
    {
        return $this->options instanceof Collection;
    }
    public function isSelected($value):bool
    {
        if(is_array($this->selected))
        {
            return in_array($value,$this->selected);
        }

        return $this->selected==$value;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.select2');
    }
}

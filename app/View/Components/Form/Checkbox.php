<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Checkbox extends Component
{
    public string $id;
    public string $name;
    public string $liveName;
    public string $label;
    public bool $lazyUpdate;
    public $value;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id=null,string $name,string $liveName=null,
                                string $label,$lazyUpdate=false,$value=true)
    {
        $this->id=$liveName?$liveName:$id ?? $name;
        $this->name=$name;
        $this->liveName=$liveName ?? $name;
        $this->lazyUpdate=$lazyUpdate;
        $this->label=$label;
        $this->value=$value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.checkbox');
    }
}

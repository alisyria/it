<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Images extends Component
{
    public string $liveName;
    public ?string $mimes;
    public ?string $id;
    public ?string $name;
    public string $maxFileSize;
    public int $maxFiles;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $liveName,string $mimes='image/*',string $id=null,string $name=null,
        $maxFileSize='10MB',int $maxFiles=1
    )
    {
        $this->liveName=$liveName;
        $this->mimes=$mimes;
        $this->id=$id ?? $liveName;
        $this->name=$name ?? $liveName;
        $this->maxFileSize=$maxFileSize;
        $this->maxFiles=$maxFiles;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.images');
    }
}

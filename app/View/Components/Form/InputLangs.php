<?php

namespace App\View\Components\Form;

use App\Models\AppLanguage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;

class InputLangs extends Component
{
    public ?Model $model;
    public string $defaultLang;
    public string $type;
    public string $name;
    public ?string $liveName;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Model $model=null,string $defaultLang=null,string $name,
                                string $type='text',string $liveName=null)
    {
        $this->model=$model;
        $this->defaultLang=$defaultLang ?? AppLanguage::getDefaultLocal();
        $this->name=$name;
        $this->type=$type;
        $this->liveName=$liveName ?? $name;
    }
    public function langs():array
    {
        return AppLanguage::langsSelect();
    }
    public function translation(string $lang)
    {
        if(is_null($this->model))
        {
            return null;
        }
        return $this->model->getTranslation($this->name,$lang,false);
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form.input-langs');
    }
}

<?php

namespace App\View\Components\Labels;

use Illuminate\View\Component;
use App\Enums\OrderStatus as OrderStatusEnum;

class OrderStatus extends Component
{
    public OrderStatusEnum $status;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(OrderStatusEnum $status)
    {
        $this->status=$status;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.labels.order-status');
    }
}

<?php

namespace App\View\Components\Labels;

use Illuminate\View\Component;
use App\Enums\DeviceStatus as DeviceStatusEnum;

class DeviceStatus extends Component
{
    public DeviceStatusEnum $status;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(DeviceStatusEnum $status)
    {
        $this->status=$status;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.labels.device-status');
    }
}

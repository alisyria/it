<?php

namespace App\View\Components\Components;

use Illuminate\View\Component;

class SaveButton extends Component
{
    public string $liveTarget;
    public string $liveMethod;
    public string $saveText;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $liveTarget,string $saveText=null,string $liveMethod=null)
    {
        $this->liveMethod=$liveMethod ?? $liveTarget;
        $this->liveTarget=$liveTarget;
        $this->saveText=$saveText ?? __j('Save');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.components.save-button');
    }
}

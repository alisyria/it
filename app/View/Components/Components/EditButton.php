<?php

namespace App\View\Components\Components;

use Illuminate\View\Component;

class EditButton extends Component
{
    public string $id;
    public bool $emit=false;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id,bool $emit=flase)
    {
        $this->id=$id;
        $this->emit=$emit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.components.edit-button');
    }
}

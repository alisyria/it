<?php

namespace App\View\Components\Components;

use Illuminate\View\Component;

class DeleteButton extends Component
{
    public string $id;
    public string $action;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id,string $action='delete')
    {
        $this->id=$id;
        $this->action=$action;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.components.delete-button');
    }
}

<?php

namespace App\View\Components\Components;

use Illuminate\View\Component;

class ShowButton extends Component
{
    public string $url;
    public string $target;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $url,string $target='_self')
    {
        $this->url=$url;
        $this->target=$target;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.components.show-button');
    }
}

<?php

namespace App\Repositories;

use App\Enums\ApiMessage;
use App\Enums\UsernameType;
use App\Models\Verification;
use App\Models\User;

class VerificationRepository
{
    public function store(string $username,string $usernameType,int $userID):Verification
    {
        $verification=new Verification();
        $verification->code= $this->generateCode();
        $verification->username=$username;
        $verification->username_type=$usernameType;
        $verification->user_id=$userID;
        $verification->save();

        return $verification;
    }
    public function verify(User $user,string $code,string $username=null,$usernameTypeID=null):bool
    {
        $username=$username ?? $user->username;
        $usernameTypeID=$usernameTypeID ?? $user->username_type;
        $verification= $this->findVerification($user, $username,$usernameTypeID,$code);
        if(blank($verification))
        {
            return FALSE;
        }

        return $this->valid($verification);
    }
    public function findVerification(User $user,string $username,string $usernameTypeID,string $code)
    {
        return $user->verifications()->where('username',$username)
            ->where('username_type',$usernameTypeID)
            ->where('code',$code)
            ->first();
    }
    public function generateCode():string
    {
        return (string)rand(11111,99999);
    }
    public function valid(Verification $verification):bool
    {
        $period=$verification->created_at->diffInMinutes(now());
        if(!($period< (int)config('custom.verifications.duration')))
        {
            return FALSE;
        }
        return TRUE;
    }
    public function getAlreadyVerifiedMessage():string
    {
        return __('messages.verification_code.verified_previously');
    }
    public function getCodeSentMessage($usernameTypeID):string
    {
        return __('messages.verification_code.send_code',
            ['usernameType'=> UsernameType::getDescription($usernameTypeID)]);
    }
}


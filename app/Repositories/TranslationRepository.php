<?php

namespace App\Repositories;


use App\Enums\Disk;
use App\Models\AppLanguage;
use App\Models\LanguageLine;
use Artisan;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Lang;
use Storage;

class TranslationRepository
{
    public $disk;
    public $deafultLang;

    public function __construct()
    {
        $this->disk=Storage::disk(Disk::TRANSLATIONS);
        $this->deafultLang=AppLanguage::getDefaultLocal();
    }

    public function importFromFiles():array
    {
        $files=$this->disk->allFiles($this->deafultLang);
        $trans=[];
        foreach ($files as $file)
        {
            $group=basename($file,'.php');

            if(in_array($group,['validation']))
            {
                continue;
            }
            $trans[$group]=Arr::dot(require $this->disk->path($this->deafultLang.'/'.$group.'.php'));
        }

        return $trans;
    }
    public function importFromJson():array
    {
        $fileName=$this->deafultLang.'.json';
        return json_decode($this->disk->get($fileName),true);
    }
    public function exportAllToDatabase()
    {
        $this->exportFileTranslations($this->importFromFiles());
        $this->exportJsonTranslations($this->importFromJson());
    }
    public function exportFileTranslations(array $translations)
    {
        foreach ($translations as $group=>$val)
        {
            foreach ($val as $key=>$defaultText)
            {
                if(!is_string($defaultText))
                {
                    continue;
                }
                $this->exportTranslationToDatabase($group,$key,$defaultText);
            }
        }
    }
    public function exportJsonTranslations(array $translations)
    {
        $group='*';
        foreach ($translations as $key=>$defaultText)
        {
            $this->exportTranslationToDatabase($group,$key,$defaultText);
        }
    }
    public function exportTranslationToDatabase(string $group,string $key,string $defaultText)
    {
        $languageLine=LanguageLine::where('group',$group)->whereRaw("BINARY `key`= ?",[$key])->first();
        if($languageLine)
        {
            return;
        }
        LanguageLine::create([
            'group'=>$group,
            'key'=>$key,
            'text'=>[$this->deafultLang=>$defaultText],
        ]);
    }
    public function extractFromProjectToJson()
    {
        Artisan::call('translatable:export',[
            'lang'=>$this->deafultLang
        ]);
    }
}

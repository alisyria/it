<?php

namespace App\Repositories;

use App\Models\Setting;
use App\Http\Resources\PDFResource;
use App\Enums\SettingDataType;

class SettingsRepository
{

    public $setting;
    public $settings;

    public function __construct() {

    }
    public function getAllSettings()
    {
        return Setting::getAllSettings();
    }
    public function setSetting(Setting $setting)
    {
        $this->setting=$setting;
        return $this;
    }
    public function get($key, $default = null,$lang=null)
    {
        if ($this->has($key) ) {
            $setting = $this->getAllSettings()->where('key', $key)->first();
            return $this->setSetting($setting)->format($lang);
        }

        return $default;
    }
    public function getAllValues($key, $default = null)
    {
        if ($this->has($key) ) {
            $setting = $this->getAllSettings()->where('key', $key)->first();
            return $this->setSetting($setting)->allValues();
        }

        return $default;
    }
    public function allValues()
    {
        return $this->setting->value->all();
    }
    public function allType($type,...$except):array
    {
        $result=[];
        $typeSettings=$this->getAllSettings()
                        ->where('setting_type_id', $type)
                        ->whereNotIn('key',$except);
        foreach($typeSettings as $setting)
        {
            $result[$setting->key]=$this->setSetting($setting)->format();
        }
        return $result;
    }
    public function add($key, $value, $type,$dataType)
    {
        if ($this->has($key) ) {
            return $this->set($key, $value, $type,$dataType);
        }
        $setting=Setting::create(['key' => $key, 'value' => $this->valueForStore($value,$dataType), 'setting_type_id' => $type,
            'setting_data_type_id'=>$dataType]);
        return $setting?$this->setSetting($setting)->format():FALSE;
    }
    public function set($key, $value, $type,$dataType)
    {
        $setting = $this->getAllSettings()->where('key', $key)->first();
        if ($setting) {
            $setting->update([
                'key' => $key,
                'value' => $this->valueForStore($value,$dataType),
                'setting_type_id' => $type,
                'setting_data_type_id'=>$dataType]);
            return $this->setSetting($setting)->format();
        }

        return $this->add($key, $value, $type,$dataType);
    }
    public function valueForStore($value,$dataType)
    {
        switch($dataType)
        {
            case "number.integer":
                $value= ['value'=>(int)$value];
                break;
        }
        return $value;
    }
    public function has($key)
    {
        return (boolean) $this->getAllSettings()->whereStrict('key', $key)->count();
    }
    public function remove($key)
    {
        if($this->has($key)) {
            return $this->getAllSettings()->where('key',$key)->first()->delete();
        }

        return false;
    }
    public function format(string $lang=null)
    {
        $result="";
        switch($this->setting->setting_data_type_id)
        {
            case SettingDataType::TEXT:
                $result=$this->formatText();
                break;
            case SettingDataType::TextLocalized:
                $result= $this->formatTextLocalized($lang);
                break;
            case SettingDataType::NumberInteger:
                $result= $this->formatNumberInteger();
                break;
            case SettingDataType::NumberDecimal:
                $result= $this->formatNumberDecimal();
                break;
            case SettingDataType::FilePDF:
                $result= $this->formatFilePdf();
                break;
            case SettingDataType::FileImage:
                $result= $this->formatFileImage();
                break;
        }
        return $result;
    }
    public function formatTextLocalized(string $lang=null)
    {
        if(!is_null($lang))
        {
            return $this->setting->value->get($lang);
        }
        if($this->setting->value->get(app()->getLocale()))
        {
            return $this->setting->value->get(app()->getLocale());
        }
        else
        {
            return $this->setting->value->get(config('translatable.fallback_locale'),"");
        }
    }
    public function formatNumberInteger()
    {
        return (int)$this->setting->value->get('value');
    }
    public function formatNumberDecimal()
    {
        return (float)$this->setting->value->get('value');
    }
    public function formatText()
    {
        return $this->setting->value->get('value');
    }
    public function formatFilePdf()
    {
        $media= $this->setting->getFirstMedia('pdf');
        return new PDFResource($media);
    }
    public function formatFileImage()
    {
        $media= $this->setting->getFirstMedia($this->setting->setting_type_id);
        if($media)
        {
            return new ImageResource($media);
        }
        return null;
    }
}


<?php

namespace App\Repositories;


use App\Models\Employee;
use App\Models\User;

class EmployeeRepository
{
    public function store(User $user):Employee
    {
        $employee=new Employee();
        $employee->user()->associate($user);
        $employee->save();

        return $employee;
    }
    public function update(Employee $employee):Employee
    {
        return $employee;
    }
}

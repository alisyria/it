<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\UnreachableUrl;
use Nahid\JsonQ\Jsonq;
use App\Enums\Disk;
use Cache;
use Storage;
use App\Models\Country;
use App\Models\Currency;
use Faker\Factory as Faker;

class SeedRepository
{
    public $faker;
    public $fakeDisk;
    public static $countriesPath='data/countries.json';
    public static $currenciesPath='data/currencies.json';

    public function __construct() {
        $this->fakeDisk=Storage::disk(Disk::FAKE);
        $this->faker=Faker::create();
    }

    public function seedCurrencies()
    {
        $jsonq=new Jsonq(Storage::disk(Disk::FAKE)->path(self::$currenciesPath));
        foreach ($jsonq->get() as $item)
        {
            Currency::firstOrCreate([
                'id'=>$item->id
            ],[
                'name'=> $item->name,
                'parts'=> $item->parts,
                'symbol'=> $item->symbol,
            ]);
        }
        Currency::flushCache();
    }
    public function seedCountries()
    {
        foreach (Countries::all() as $item)
        {
            $item=(object)$item;
            if(!in_array($item->cca2,['TR','DE','IQ']))
            {
                continue;
            }
            $country=Country::firstOrCreate([
                'code'=>$item->cca2
            ],[
                'name'=>value(function()use($item){
                    $name=[];
                    foreach (AppLanguage::getSupportedLocales() as $locale)
                    {
                        $trans=data_get($item,"name_$locale->id");
                        if(filled($trans))
                        {
                            $name[$locale->id]=$trans;
                        }
                    }
                    return $name;
                }),
                'calling_codes'=>data_get($item,'dialling.calling_code'),
                'idd_root'=>data_get($item,'idd.root'),
            ]);
            $this->seedFlag($country);
        }
        Country::flushCache();
    }
    public function seedFlag(Country $country)
    {
        $flagFilename=strtolower($country->code).".png";
        $flagUrl="http://flagpedia.net/data/flags/normal/{$flagFilename}";

        $country->addMediaFromUrl($flagUrl)
            ->preservingOriginal()
            ->toMediaCollection(Country::$ICON);
    }
    public function seedCities(Country $country,array $citiesArr)
    {
        foreach($citiesArr as $cityItem)
        {
            $country->cities()->create($cityItem);
        }
    }
    public function seedImage(Model $model,string $path,string $collection)
    {
        $model->addMedia($path)
            ->preservingOriginal()
            ->toMediaCollection($collection);
    }

}

<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Registration;
use App\Enums\OS;

class RegistrationsRepository
{
    public function store(Request $request) : Registration
    {
        $version= $this->extractVersion($request);
        $deviceId= $this->getDeviceID($request);

        $isAlreadyExist=Registration::device($request->instance_uuid)->exists();
        $registration=Registration::firstOrNew(['instance_uuid'=>$request->instance_uuid]);
        if($request->filled('device_id'))
        {
            $registration->fill(['device_id'=>$deviceId,'os_id'=>$request->os]);
        }
        if($request->filled('reg_id'))
        {
            $registration->reg_id=$request->reg_id;
        }
        if(!($isAlreadyExist && !$request->filled('lang')))
        {
            $registration->lang= $this->getLang($request,$isAlreadyExist);
        }
        if(!$isAlreadyExist)
        {
            $registration->is_notifiable=1;
        }
        $registration->version=$version;
        if($request->filled('ios_version'))
        {
            $registration->ios_version=$request->ios_version;
        }
        if($request->filled('is_notifiable'))
        {
            $registration->is_notifiable=$request->is_notifiable;
        }
        $registration->save();

        return $registration;
    }
    private function getLang(Request $request,bool $isAlreadyExist)
    {
        $lang=$request->lang;
        if(!$request->filled('lang') && !$isAlreadyExist)
        {
            $lang= config('app.locale');
        }
        return $lang;
    }
    private function getDeviceID(Request $request)
    {
        $deviceId=$request->device_id;
        if($request->os== OS::IOS)
        {
            $deviceId=str_replace('-','',$request->device_id);
        }
        return $deviceId;
    }
    private function extractVersion(Request $request):int
    {
        $pathSections= explode('/',$request->getPathInfo());
        if(!isset($pathSections[2]))
        {
            logger("version not found");
            abort(403);
        }
        $version= (int)substr($pathSections[2],1);
        if(!in_array($version, config('custom.app_versions')))
        {
            abort(404);
        }
        return $version;
    }
    public function associateUser(int $userID,string $instanceUUID,string $loginAt=null):Registration
    {
        $registration= Registration::firstOrNew(['instance_uuid'=>$instanceUUID]);
        if(!$registration->exists)
        {
            $registration->version= $this->extractVersion(request());
            $registration->lang=config('app.locale');
            $registration->save();
        }
        $registration->users()->sync([
            $userID=>['login_at'=>$loginAt]
        ]);

        return $registration;
    }
}
<?php

namespace App\Repositories;

use App\Enums\AuthGuard;
use App\Models\Contracts\Roleable;
use App\Models\Permission;
use App\Models\Role;
use App\Models\AppLanguage;

class RoleRepository
{
    public function store(?Role $role,string $name,array $description, AuthGuard $authGuard,
                          array $permissions,Roleable $roleable=null):Role
    {
        if(is_null($role))
        {
            $role=new Role();
        }

        $role->name=$name;
        $role->description=$description;
        $role->guard_name=$authGuard->value;
        if($roleable)
        {
            $role->roleable()->associate($roleable);
        }
        $role->save();

        $this->syncPermissions($role,$permissions);

        return $role;
    }
    public function syncPermissions(Role $role,?array $permissions)
    {
        if(is_null($permissions))
        {
            $permissions=[];
        }
        if(count($permissions)>0)
        {
            $this->checkPermissions($permissions);
        }
        $role->permissions()->sync($permissions);
    }
    public function checkPermissions(array $permissions)
    {
        $allowedPermIds=Permission::getAllowedPermissions()->pluck('id')->toArray();
        foreach ($permissions as $permission)
        {
            if(!in_array($permission,$allowedPermIds))
            {
                abort(403);
            }
        }
    }
    public function delete(Role $role):void
    {
        if(!auth()->user()->can('delete-role',$role))
        {
            abort(403);
        }
        $role->delete();
    }
}

<?php

namespace App\Repositories;

use App\Enums\NotificationTarget;
use App\Enums\VendorType;
use App\Http\Resources\ImageResource;
use App\Jobs\AssociateNotificationToUsers;
use App\Models\Contracts\NotificationSenderable;
use App\Values\NotificationStats;
use Illuminate\Support\Str;
use App\Models\Notification;
use App\Enums\NotificationKey;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\Admin\NotificationCtrl\SendGeneralReq;
use App\Models\Registration;
use App\Jobs\SendPushNotificationsAllJob;

class NotificationRepository
{
    public $registrations;

    public function __construct(RegistrationsRepository $registrations)
    {
        $this->registrations=$registrations;
    }

    public function handleSendGeneral(array $title,array $text):void
    {
        $notification=$this->store(NotificationKey::GENERAL,$title,$text,[]);
        $this->sendGeneral($notification);
    }
    public function getSendGeneralMessage():string
    {
        return __('notifications-view.send_message');
    }
    public function sendGeneral($notification)
    {
        $queue=config('custom.notification.general_push.manager_queue');
        SendPushNotificationsAllJob::dispatch($notification,[])
            //->onConnection('redis')->onQueue($queue)
            ->delay(now()->addSeconds(15));
        AssociateNotificationToUsers::dispatch($notification,[]);
        //->onConnection('redis')->onQueue(config('custom.notification.general_push.chunk_queue'));
    }
    public function buildSendGeneralQuery():Builder
    {
        return Registration::latest()
            ->notifiable();
    }
    public function store(?NotificationSenderable $senderable,?NotificationKey $key,?string $type,
         ?NotificationTarget $target,?VendorType $vendorType,array $title,array $text,array $data=[],
         array $governorates=[],array $regions=[],string $image=null,NotificationStats $stats=null):Notification
    {
        $notification=new Notification();
        $notification->id=Str::uuid();
        $notification->key=$key;
        $notification->type=$type;
        $notification->target=$target;
        $notification->vendor_type=$vendorType;
        $notification->title=$title;
        $notification->text=$text;
        $notification->data=$data;
        $notification->statistics=$stats;
        if(filled($senderable))
        {
            $notification->senderable()->associate($senderable);
        }
        $notification->save();

        if(filled($governorates))
        {
            $notification->governorates()->attach($governorates);
        }
        if(filled($regions))
        {
            $notification->regions()->attach($regions);
        }
        if(filled($image))
        {
            $imageNotification=$notification->imageNotification()->create();
            $imageMedia=$imageNotification->associateImage($image);
            $data['image']=(new ImageResource($imageMedia))->resolve();
            $notification->update([
                'data'=>$data,
            ]);
        }

        return $notification;
    }
    public function handleDelete(Notification $notification)
    {
        $this->delete($notification);
        flash(__('notifications-view.delete_message'))->success();
        return back();
    }
    public function delete(Notification $notification)
    {
        $notification->delete();
    }
}

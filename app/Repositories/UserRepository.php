<?php

namespace App\Repositories;

use App\Domain\ValueObjects\User\UserSettings;
use App\Enums\AuthGuard;
use App\Exceptions\InvalidResetPasswordTokenException;
use App\Exceptions\UserNotFoundException;
use App\Models\Registration;
use GuzzleHttp\Client as HttpClient;
use App\Exceptions\LoginException;
use Exception;
use App\Models\User;
use App\Enums\UsernameType;
use App\Enums\UserType;
use Illuminate\Http\UploadedFile;
use Webpatser\Uuid\Uuid;
use Avatar;
use Storage;
use App\Models\PasswordReset;
use App\Enums\Disk;
use App\Notifications\ResetPasswordNotify;
use App\Http\Requests\Admin\Auth\ForgotPasswordCtrl\SendResetLinkReq;

class UserRepository
{
    public $verifications;

    public function __construct(VerificationRepository $verifications) {
        $this->verifications=$verifications;
    }

    public function store(string $name,string $middleName=null,string $lastName,
                          string $username,string $password=null,string $email=null,string $mobile=null,
                          string $usernameTypeID,string $userTypeID,string $avatarPath=null):User
    {
        $user=new User();
        $user->name=$name;
        $user->middle_name=$middleName;
        $user->last_name=$lastName;
        $user->username=$username;
        $user->password= $password ? bcrypt($password):null;
        $user->email=$email;
        $user->mobile=$mobile;
        $user->user_type=$userTypeID;
        $user->username_type=$usernameTypeID;
        $user->extra=[];
        $user->save();

        if($avatarPath)
        {
            $user->associateAvatar($avatarPath);
        }

        return $user;
    }
    public function findUserByNormalCredentials(string $userTypeID, string $usernameType,
                                                string $username):User
    {
        $user=User::findUser($usernameType,$username,$userTypeID)->firstOrFail();
        return $user;
    }
    public function logoutWeb(User $user)
    {
        \Auth::guard(AuthGuard::WEB)->logout();
        request()->session()->invalidate();
    }
    public function logoutDevice(User $user,string $instanceUUID)
    {
        $this->logoutCurrentDeviceRegistration($user->id,$instanceUUID);
        $this->revokeCurrentToken($user);
    }
    public function logoutCurrentDeviceRegistration(int $userID,string $instanceUUID)
    {
        $registration= Registration::firstOrNew([
            'instance_uuid'=>$instanceUUID
        ]);
        if($registration)
        {
            $registration->users()->sync([
                $userID=>['login_at'=>null]
            ]);
        }
    }
    public function revokeCurrentToken(User $user)
    {
        $user->currentAccessToken()->delete();
    }

    public function storeAvatarDefault($user,$name)
    {
        if(config('custom.avatar.generation_method')=='name')
        {
            $this->generateAvatarFromName($user,$name);
        }
        else
        {
            $this->generateAvatarFromImage($user,config('custom.avatar.default_path'));
        }

    }
    public function generateAvatarFromName($user,$name)
    {
        $uuid=Uuid::generate();
        Storage::disk(Disk::LOCAL)->makeDirectory('temporary');
        $fullPath= storage_path("app/temporary/{$uuid}.png");
        Avatar::create($name)
            ->getImageObject()
            ->save($fullPath);
        $user->addMedia($fullPath)
            ->toMediaCollection(User::MEDIA_AVATAR);
    }
    public function generateAvatarFromImage(User $user,string $path)
    {
        $user->addMedia($path)
            ->preservingOriginal()
            ->toMediaCollection(User::MEDIA_AVATAR);
    }
    public function storeAvatar(User $user,$url=null)
    {
        if(!is_null($url))
        {
            $user->addMediaFromUrl($url)
                ->toMediaCollection(User::MEDIA_AVATAR);
        }
        else
        {
            $user->addMediaFromRequest('avatar')
                ->toMediaCollection(User::MEDIA_AVATAR);
        }
    }

    public function updateAvatar(User $user,UploadedFile $file)
    {
        $user->addMedia($file)
            ->toMediaCollection(User::MEDIA_AVATAR);
    }

    public function checkUsernameUpdate(User $user,string $username,string $usernameTypeID)
    {
        if($user->username!=$username || $user->user_type!=$usernameTypeID)
        {
            $this->verifications->store($username,$usernameTypeID,$user->id);
            return TRUE;
        }
        return FALSE;
    }
    public function activate(User $user)
    {
        $user->activated_at=now();
        $user->save();
    }
    public function verify(User $user)
    {
        $user->verified_at=now();
        $user->save();
    }
    public function isVerified(User $user)
    {
        return !is_null($user->verified_at);
    }
    public function isActivated(User $user)
    {
        return !is_null($user->activated_at);
    }
    public function verifyAccount(User $user)
    {
        $this->verify($user);
        $this->activate($user);
    }
    public function clearInactiveForUsername(string $username,string $usernameTypeID)
    {
        $user=User::inactive()->where('username',$username)
            ->where('user_type',$usernameTypeID)
            ->first();
        if($user)
        {
            $user->forceDelete();
        }
    }
    public function update(User $user,string $name,string $middleName=null,string $lastName,
        string $username=null,string $password=null,string $mobile=null,string $email=null,string $usernameTypeID=null,
        string $userTypeID=null,string $avatarPath=null):User
    {
        $user->name=$name;
        $user->middle_name=$middleName;
        $user->last_name=$lastName;
        if(!is_null($password))
        {
            $user->password= bcrypt($password);
        }
        $user->email= $email;
        $user->mobile= $mobile;

        if(filled($userTypeID))
        {
            $user->user_type=$userTypeID;
        }
        if(filled($usernameTypeID))
        {
            $user->username_type=$usernameTypeID;
        }
        if($username)
        {
            $user->username=$username;
        }

        $user->save();

        if($avatarPath)
        {
            $user->associateAvatar($avatarPath);
        }

        return $user;
    }
    public function resetUsername(User $user,string $username,
                                  string $usernameTypeID)
    {
        $user->username=$username;
        $user->user_type=$usernameTypeID;
        $user->save();
    }
    public function handleSendResetLink(SendResetLinkReq $request)
    {
        $user=User::findUser(UsernameType::USERNAME,$request->email,UserType::Employee)->firstOrFail();
        $this->sendResetLink($user);
        flash($this->sendResetLinkMessage())->success()->important();
        return back();
    }
    public function findUserByEmail(string $email):User
    {

    }
    public function sendResetLinkMessage():string
    {
        return __('messages.send_reset_link.success');
    }
    public function sendResetLink(User $user)
    {
        $link=$this->generateResetLink($user->username);
        $user->notify(new ResetPasswordNotify($user,$link,app()->getLocale()));
    }
    public function generateResetLink(string $email):string
    {
        $token=str_random(32);
        PasswordReset::create([
            'email'=>$email,
            'token'=>$token,
            'created_at'=>now()
        ]);
        return route('admin.resetPassword',['token'=>$token]);
    }
    public function getUserFromResetToken(string $token):User
    {
        $passwordReset=PasswordReset::where('created_at','>=',now()
            ->subMinutes(config('custom.reset_password.duration',20)))
            ->first();
        if(!$passwordReset)
        {
            throw new InvalidResetPasswordTokenException(__('errors.reset_password.invalid_token'));
        }
        $user=User::findUser(UsernameType::USERNAME,$passwordReset->email,
            UserType::Employee)->first();
        if(!$user)
        {
            throw new UserNotFoundException(__('errors.users.not_found'));
        }
        return $user;
    }
    public function resetPassword(User $user,string $newPassword)
    {
        $user->password=bcrypt($newPassword);
        $user->save();
    }
}

<?php

namespace App\Repositories;


use App\Enums\Admin\AdminStatistic;
use App\Enums\CompanyStatistic;
use App\Models\Ad;
use App\Models\Company;
use App\Models\CompanyEmployee;
use App\Models\Customer;
use App\Models\Device;
use App\Models\Employee;
use App\Models\Order;
use App\Models\Product;
use App\Models\Registration;
use App\Models\Shop;
use App\Models\User;

class DashboardRepository
{
    public function countEmployees($period): int
    {
        return Employee::period($period)->count();
    }

    public function countDevices($period): int
    {
        return Device::period($period)->count();
    }
    public function countOrdersPending(string $period=null)
    {
        return Order::pending()->period($period)->count();
    }
    public function countOrdersProcessing(string $period=null)
    {
        return Order::processing()->period($period)->count();
    }
    public function countOrdersSuccesses(string $period=null)
    {
        return Order::successes()->period($period)->count();
    }
    public function countOrdersFailed(string $period=null)
    {
        return Order::failed()->period($period)->count();
    }

    public function getAdminCollectionCounts(string $period=null)
    {
        return $this->createCollection(adminUser(),AdminStatistic::class,AdminStatistic::toCounts(),$period);
    }
    public function getCompanyCollectionCounts(string $period=null)
    {
        return $this->createCollection(companyUser(),CompanyStatistic::class,CompanyStatistic::toCounts(),$period);
    }

    public function createCollection(User $user,string $statisticEnumClass,array $statistics,$period)
    {
        $counts= collect();

        foreach ($statistics as $statistic)
        {
            if(\Gate::forUser($user)->denies($statisticEnumClass::getGate($statistic)))
            {
                continue;
            }
            $counter=$statisticEnumClass::getCounter($statistic);
            $item=$this->createInfo(
                $statisticEnumClass::getDescription($statistic),$statisticEnumClass::getBgColor($statistic),$statisticEnumClass::getIcon($statistic),
                $this->$counter($period),$statisticEnumClass::getPosition($statistic),true
            );
            $counts->put($statistic,$item);
        }

        return $counts;
    }
    public function createInfo(string $title,string $bgColor,string $icon,
                               string $count,int $position):object
    {
        return (object)[
            'title'=>$title,
            'bgColor'=>$bgColor,
            'icon'=>$icon,
            'count'=>$count,
            'position'=>$position,
        ];
    }
}


<?php


namespace App\Repositories;

use DB;
use App\Models\Contracts\LocationableContract;
use App\Models\Location;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class LocationRepository
{
    public function store(LocationableContract $locationable,Point $coordinates=null,int $governorateId=null,
                           int $regionId=null,array $address=null,bool $isMain=true):Location
    {
        $location=null;
        DB::transaction(function()use(
            &$location,$locationable,$coordinates,$governorateId,$regionId,$address,$isMain
        ){
            if($isMain)
            {
                Location::whereHasMorph('locationable',get_class($locationable))
                    ->where('locationable_id',$locationable->id)
                    ->get()
                    ->each->update(['is_main'=>false]);
            }
            $location=new Location();
            $location->coordinates=$coordinates;
            $location->governorate_id=$governorateId;
            $location->region_id=$regionId;
            $location->address=$address;
            $location->is_main=$isMain;
            $location->locationable()->associate($locationable);
            $location->save();
        });

        return $location;
    }
    public function update(Location $location,Point $coordinates=null,int $governorateId=null,
         int $regionId=null,array $address=null,bool $isMain=null):Location
    {
        DB::transaction(function()use(
            &$location,$coordinates,$governorateId,$regionId,$address,$isMain
        ) {
            if($isMain && !$location->is_main)
            {
                Location::whereHasMorph('locationable',$location->locationable_type)
                    ->where('locationable_id',$location->locationable_id)
                    ->get()
                    ->each->update(['is_main'=>false]);
            }
            $location->coordinates = $coordinates;
            $location->governorate_id = $governorateId;
            $location->region_id = $regionId;
            $location->address = $address;
            if (!is_null($isMain)) {
                $location->is_main = $isMain;
            }
            $location->save();
        });

        return $location;
    }
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LAST_SEEN_NOTIFICATIONS()
 * @method static static LAST_DELETE_NOTIFICATIONS()
 */
/**
 * @method static static LAST_SEEN_NOTIFICATIONS()
 * @method static static LAST_DELETE_NOTIFICATIONS()
 */
final class RegistrationExtra extends Enum
{
    const LAST_SEEN_NOTIFICATIONS = 'last_seen_notifications';
    const LAST_DELETE_NOTIFICATIONS= 'last_delete_notifications';
}

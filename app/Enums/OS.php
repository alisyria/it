<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static Android()
 * @method static static IOS()
 */
/**
 * @method static static Android()
 * @method static static IOS()
 */
final class OS extends Enum implements LocalizedEnum
{
    const Android ='android';
    const IOS ='ios';
}

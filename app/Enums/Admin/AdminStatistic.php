<?php

namespace App\Enums\Admin;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class AdminStatistic extends Enum implements LocalizedEnum
{
    //count
    const COUNT_EMPLOYEES = 'count_employees';
    const COUNT_DEVICES = 'count_devices';
    const COUNT_ORDERS_PENDING='count_orders_pending';
    const COUNT_ORDERS_PROCESSING='count_orders_processing';
    const COUNT_ORDERS_SUCCESSES='count_orders_successes';
    const COUNT_ORDERS_FAILED='count_orders_failed';

    static $counters=[
        self::COUNT_EMPLOYEES=>'countEmployees',
        self::COUNT_DEVICES=>'countDevices',
        self::COUNT_ORDERS_PENDING=>'countOrdersPending',
        self::COUNT_ORDERS_PROCESSING=>'countOrdersProcessing',
        self::COUNT_ORDERS_SUCCESSES=>'countOrdersSuccesses',
        self::COUNT_ORDERS_FAILED=>'countOrdersFailed',
    ];
    static $gates=[
        self::COUNT_EMPLOYEES=>'jsustSuperAdmin',
        self::COUNT_DEVICES=>'jsustSuperAdmin',
        self::COUNT_ORDERS_PENDING=>'jsustSuperAdmin',
        self::COUNT_ORDERS_PROCESSING=>'jsustSuperAdmin',
        self::COUNT_ORDERS_SUCCESSES=>'jsustSuperAdmin',
        self::COUNT_ORDERS_FAILED=>'jsustSuperAdmin',
    ];
    static $icons=[
        self::COUNT_EMPLOYEES=>'icons.fas-user-tie',
        self::COUNT_DEVICES=>'icons.zondicon-computer-desktop',
        self::COUNT_ORDERS_PENDING=>'icons.carbon-license-maintenance-32',
        self::COUNT_ORDERS_PROCESSING=>'icons.carbon-license-maintenance-32',
        self::COUNT_ORDERS_SUCCESSES=>'icons.carbon-license-maintenance-32',
        self::COUNT_ORDERS_FAILED=>'icons.carbon-license-maintenance-32',
    ];
    static $bgColor=[
        self::COUNT_EMPLOYEES=>'indigo',
        self::COUNT_DEVICES=>'green',
        self::COUNT_ORDERS_PENDING=>'yellow',
        self::COUNT_ORDERS_PROCESSING=>'blue',
        self::COUNT_ORDERS_SUCCESSES=>'green',
        self::COUNT_ORDERS_FAILED=>'red',
    ];                                        /*
                                            <div class="
                                        text-orange-500 bg-orange-100
                                        text-green-500 bg-green-100
                                        text-indigo-500 bg-indigo-100
                                        text-gray-500 bg-gray-100
                                        text-yellow-500 bg-yellow-100
                                        text-blue-500 bg-blue-100
                                        text-red-500 bg-red-100
                                        text-red-500 bg-purple-100
                                        text-red-500 bg-pink-100
                                        ">
                                        */
    static $positions=[
        self::COUNT_DEVICES=>1,
        self::COUNT_ORDERS_PENDING=>2,
        self::COUNT_ORDERS_PROCESSING=>3,
        self::COUNT_ORDERS_SUCCESSES=>4,
        self::COUNT_ORDERS_FAILED=>5,
        self::COUNT_EMPLOYEES=>6,
    ];

    public static function toCounts(bool $toComma=FALSE)
    {
        $values=[
            self::COUNT_EMPLOYEES,
            self::COUNT_DEVICES,
            self::COUNT_ORDERS_PENDING,
            self::COUNT_ORDERS_PROCESSING,
            self::COUNT_ORDERS_SUCCESSES,
            self::COUNT_ORDERS_FAILED,
        ];
        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }

    public static function getGate(string $statistic):string
    {
        return 'admin-statistics.'.$statistic;
    }
    public static function getIcon(string $statistic):string
    {
        return self::$icons[$statistic];
    }
    public static function getBgColor(string $statistic):string
    {
        return self::$bgColor[$statistic];
    }
    public static function getPosition(string $statistic):string
    {
        return self::$positions[$statistic];
    }
    public static function getCounter(string $statistic):string
    {
        return self::$counters[$statistic];
    }
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static MANAGE_EMPLOYEES()
 * @method static static MANAGE_ROLES()
 * @method static static MANAGE_DEVICES()
 * @method static static MANAGE_ORDERS()
 * @method static static MANAGE_MAINTENANCE()
 * @method static static MANAGE_SETTINGS()
 * @method static static MANAGE_LOCALIZATION()
 */
final class Permission extends Enum implements LocalizedEnum
{
    const MANAGE_EMPLOYEES='manage employees';
    const MANAGE_ROLES='manage roles';
    const MANAGE_DEVICES= 'manage devices';
    const MANAGE_ORDERS= 'manage orders';
    const MANAGE_MAINTENANCE= 'manage maintenance';
    const MANAGE_SETTINGS = 'manage settings';
    const MANAGE_LOCALIZATION='manage localization';


    public static function toGeneralManager(bool $toComma=FALSE)
    {
        $values=array_except(self::asSelectArray(),self::toNotAllowed());
        $values=array_keys($values);
        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }
    public static function toNotAllowed(bool $toComma=FALSE)
    {
        $values=[
//            self::MANAGE_MANAGERS,self::MANAGE_ROLES
        ];

        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }
}

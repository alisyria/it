<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static PENDING()
 * @method static static RESPONDED()
 */
/**
 * @method static static PENDING()
 * @method static static RESPONDED()
 */
final class EnquiryStatus extends Enum implements LocalizedEnum
{
    const PENDING = 'pending';
    const RESPONDED = 'responded';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LOCAL()
 * @method static static STAGING()
 * @method static static PRODUCTION()
 * @method static static SINGLE()
 * @method static static DAILY()
 * @method static static SLACK()
 * @method static static STDERR()
 * @method static static SYSLOG()
 * @method static static ERROR_LOG()
 */
/**
 * @method static static LOCAL()
 * @method static static STAGING()
 * @method static static PRODUCTION()
 * @method static static SINGLE()
 * @method static static DAILY()
 * @method static static SLACK()
 * @method static static STDERR()
 * @method static static SYSLOG()
 * @method static static ERROR_LOG()
 */
final class LogChannel extends Enum
{
    const LOCAL = 'local';
    const STAGING = 'staging';
    const PRODUCTION = 'production';
    const SINGLE = 'single';
    const DAILY = 'daily';
    const SLACK = 'slack';
    const STDERR = 'stderr';
    const SYSLOG = 'syslog';
    const ERROR_LOG = 'errorlog';
}

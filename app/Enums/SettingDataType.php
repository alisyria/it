<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static TEXT()
 * @method static static TextLocalized()
 * @method static static NumberInteger()
 * @method static static NumberDecimal()
 * @method static static FilePDF()
 * @method static static FileImage()
 */
/**
 * @method static static TEXT()
 * @method static static TextLocalized()
 * @method static static NumberInteger()
 * @method static static NumberDecimal()
 * @method static static FilePDF()
 * @method static static FileImage()
 */
final class SettingDataType extends Enum
{
    const TEXT='text';
    const TextLocalized = 'text.localized';
    const NumberInteger = 'number.integer';
    const NumberDecimal= 'number.decimal';
    const FilePDF = 'file.pdf';
    const FileImage = 'file.image';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

final class UsernameType extends Enum implements LocalizedEnum
{
    const USERNAME ='username';

    public static function toNormal(bool $toComma=FALSE)
    {
        $values=[self::USERNAME];
        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }
}

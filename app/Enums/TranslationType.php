<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static GENERAL()
 */
/**
 * @method static static GENERAL()
 */
/**
 * @method static static GENERAL()
 */
final class TranslationType extends Enum implements LocalizedEnum
{
    const GENERAL= 'general';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static MALE()
 * @method static static FEMALE()
 */
/**
 * @method static static MALE()
 * @method static static FEMALE()
 */
final class Gender extends Enum implements LocalizedEnum
{
    const MALE = 'male';
    const FEMALE = 'female';
}

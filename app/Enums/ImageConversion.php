<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SMALL()
 * @method static static MEDIUM()
 * @method static static LARGE()
 * @method static static DEFAULT()
 */
final class ImageConversion extends Enum
{
    const SMALL =   'small';
    const MEDIUM =   'medium';
    const LARGE =   'large';
    const DEFAULT =   'default';
}

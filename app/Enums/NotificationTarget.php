<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static COMPANIES()
 * @method static static SHOPS()
 */
/**
 * @method static static COMPANIES()
 * @method static static SHOPS()
 */
final class NotificationTarget extends Enum implements LocalizedEnum
{
    const COMPANIES =   'companies';
    const SHOPS = 'shops';
}

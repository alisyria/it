<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static TOTAL_DEVICES()
 * @method static static SUCCESS_DEVICES()
 * @method static static FAILED_DEVICES()
 */
/**
 * @method static static TOTAL_DEVICES()
 * @method static static SUCCESS_DEVICES()
 * @method static static FAILED_DEVICES()
 */
final class NotificationStatistic extends Enum
{
    const TOTAL_DEVICES = 'total_devices';
    const SUCCESS_DEVICES = 'success_devices';
    const FAILED_DEVICES = 'failed_devices';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static PENDING()
 * @method static static PROCESSING()
 * @method static static SUCCESSES()
 * @method static static FAILED()
 */
final class OrderStatus extends Enum implements LocalizedEnum
{
    const PENDING = 'pending';
    const PROCESSING = 'processing';
    const SUCCESSES = 'successes';
    const FAILED = 'failed';

    static $colors=[
        self::PENDING=>'yellow',
        self::PROCESSING=>'blue',
        self::SUCCESSES=>'green',
        self::FAILED=>'red',
    ];

    public static function getColor($value)
    {
        return data_get(self::$colors,$value);
    }
}

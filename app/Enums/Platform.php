<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static WEB()
 * @method static static MOBILE()
 */
final class Platform extends Enum
{
    const WEB = 'web';
    const MOBILE = 'mobile';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static OPERATIVE()
 * @method static static INOPERATIVE()
 */
final class DeviceStatus extends Enum implements LocalizedEnum
{
    const OPERATIVE =   'operative';
    const INOPERATIVE =   'inoperative';

    static $colors=[
        self::OPERATIVE=>'green',
        self::INOPERATIVE=>'red',
    ];

    public static function getColor($value)
    {
        return data_get(self::$colors,$value);
    }
}

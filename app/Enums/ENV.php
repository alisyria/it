<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LOCAL()
 * @method static static STAGING()
 * @method static static PRODUCTION()
 */
/**
 * @method static static LOCAL()
 * @method static static STAGING()
 * @method static static PRODUCTION()
 */
final class ENV extends Enum
{
    const LOCAL = 'local';
    const STAGING = 'staging';
    const PRODUCTION = 'production';
}

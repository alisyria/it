<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static App()
 * @method static static AboutApp()
 * @method static static Restriction()
 * @method static static PushNotification()
 * @method static static DEVELOPER()
 * @method static static INFO()
 * @method static static SOCIALS()
 */
/**
 * @method static static App()
 * @method static static AboutApp()
 * @method static static Restriction()
 * @method static static PushNotification()
 * @method static static DEVELOPER()
 * @method static static INFO()
 * @method static static SOCIALS()
 */
/**
 * @method static static App()
 * @method static static AboutApp()
 * @method static static Restriction()
 * @method static static Notification()
 * @method static static PushNotification()
 * @method static static DEVELOPER()
 * @method static static INFO()
 * @method static static SOCIALS()
 */
final class SettingType extends Enum
{
    const App = 'app';
    const AboutApp = 'about_app';
    const Restriction = 'restriction';
    const Notification = 'notification';
    const PushNotification = 'push_notification';
    const DEVELOPER= 'developer';
    const INFO = 'info';
    const SOCIALS = 'socials';
}

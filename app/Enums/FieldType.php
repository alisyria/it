<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static TEXT_SINGLE_LINE()
 * @method static static SINGLE_OPTION()
 */
final class FieldType extends Enum implements LocalizedEnum
{
    const TEXT_SINGLE_LINE =   'text_single_line';
    const SINGLE_OPTION =   'single_option';
}

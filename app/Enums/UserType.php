<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static SUPER_ADMIN()
 * @method static static EMPLOYEE()
 * @method static static COMPANY_OWNER()
 * @method static static COMPANY_EMPLOYEE()
 * @method static static SHOP_OWNER()
 * @method static static CUSTOMER()
 */
/**
 * @method static static SUPER_ADMIN()
 * @method static static EMPLOYEE(
 */
final class UserType extends Enum implements LocalizedEnum
{
    const SUPER_ADMIN =0;
    const EMPLOYEE =1;

    public static function getAdminTypes()
    {
        return [
            self::SUPER_ADMIN,self::EMPLOYEE,
        ];
    }
}

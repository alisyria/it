<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SUCCESS()
 * @method static static ERROR()
 */
/**
 * @method static static SUCCESS()
 * @method static static ERROR()
 */
final class ApiMessage extends Enum
{
    const SUCCESS ='success';
    const ERROR = 'error';
}

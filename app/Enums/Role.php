<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static SUPER_ADMIN()
 * @method static static GENERAL_MANAGER()
 */
/**
 * @method static static SUPER_ADMIN()
 * @method static static GENERAL_MANAGER()
 */
final class Role extends Enum implements LocalizedEnum
{
    const SUPER_ADMIN = 'مدير عام';
    const DEVICES_MANAGER= 'مدير أجهزة';

    public static function toNotAllowed(bool $toComma=FALSE)
    {
        $values=[
            self::SUPER_ADMIN
        ];
        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }
}

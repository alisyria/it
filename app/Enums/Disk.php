<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LOCAL()
 * @method static static PUBLIC()
 * @method static static MEDIA_LOCAL()
 * @method static static S3()
 * @method static static FAKE()
 * @method static static TEMPORARY()
 * @method static static TRANSLATIONS()
 */
/**
 * @method static static LOCAL()
 * @method static static PUBLIC()
 * @method static static MEDIA_LOCAL()
 * @method static static S3()
 * @method static static FAKE()
 * @method static static TEMPORARY()
 * @method static static TRANSLATIONS()
 */
final class Disk extends Enum
{
    const LOCAL = 'local';
    const PUBLIC = 'public';
    const MEDIA_LOCAL = 'media_local';
    const S3 = 's3';
    const FAKE= 'fake';
    const TEMPORARY='temporary';
    const TRANSLATIONS='translations';
}

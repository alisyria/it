<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static VIEW_MY()
 */
/**
 * @method static static VIEW_MY()
 */
final class Ability extends Enum
{
    const VIEW_MY = 'view-my';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static ADMIN()
 * @method static static API()
 */
/**
 * @method static static WEB()
 * @method static static ADMIN()
 * @method static static COMPANY()
 * @method static static API()
 */
/**
 * @method static static WEB()
 * @method static static ADMIN()
 * @method static static COMPANY()
 * @method static static API()
 */
final class AuthGuard extends Enum
{
    const WEB ='web';
    const ADMIN = 'admin';
    const COMPANY = 'company';
    const API = 'sanctum';
}

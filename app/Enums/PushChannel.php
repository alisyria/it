<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LOG()
 * @method static static PRODUCTION()
 */
/**
 * @method static static LOG()
 * @method static static PRODUCTION()
 */
final class PushChannel extends Enum
{
    const LOG = 'log';
    const PRODUCTION = 'production';
}

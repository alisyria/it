<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;


/**
 * @method static static GENERAL()
 * @method static static GENERAL_IMAGE()
 * @method static static GROUP()
 * @method static static PRIVATE()
 * @method static static ORDER_STATUS()
 * @method static static NEW_MESSAGE()
 */
final class NotificationKey extends Enum implements LocalizedEnum
{
    const GENERAL = 'general';
    const GENERAL_IMAGE = 'general_image';
    const GROUP='group';
    const PRIVATE='private';
    const ORDER_STATUS ='order_status';
    const NEW_MESSAGE='new_message';

    public static function toGeneral(bool $toComma=FALSE)
    {
        $values=[self::GENERAL,self::GENERAL_IMAGE];
        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }

    public static function toGroup(bool $toComma=FALSE)
    {
        $values=[self::GROUP];
        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }
    public static function toPrivate(bool $toComma=FALSE)
    {
        $general=self::toGeneral();
        $group=self::toGroup();
        $notPrivate=array_merge($general,$group);
        $values=array_filter(self::asArray(),function ($v,$k)use($notPrivate){
            if(in_array($v,$notPrivate))
            {
                return false;
            }
            return true;
        },ARRAY_FILTER_USE_BOTH);
        if($toComma)
        {
            return self::toComma($values);
        }
        return self::only($values);
    }
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static FAVORITE()
 */
final class Reaction extends Enum
{
    const FAVORITE = 'favorite';
//    const LIKE = 'like';
//    const HEART = 'heart';
//    const PRE_ORDER='pre_order';
}

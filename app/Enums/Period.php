<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static UNDETERMINED()
 * @method static static DAILY()
 * @method static static MONTHLY()
 * @method static static YEARLY()
 */
/**
 * @method static static UNDETERMINED()
 * @method static static DAILY()
 * @method static static MONTHLY()
 * @method static static YEARLY()
 */
final class Period extends Enum implements LocalizedEnum
{
    const UNDETERMINED='';
    const DAILY = 'daily';
    const MONTHLY = 'monthly';
    const YEARLY = 'yearly';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static IMAGE()
 * @method static static VIDEO()
 */
/**
 * @method static static IMAGE()
 * @method static static VIDEO()
 */
final class FileType extends Enum implements LocalizedEnum
{
    const IMAGE = 'image';
    const VIDEO = 'video';

}

<?php

namespace App\Actions;

use App\Models\Contracts\NotifiableContract;

class ClearNotificationsAction
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Prepare the action for execution, leveraging constructor injection.
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(NotifiableContract $notifiable)
    {
        $notifiable->clearNotifications();
    }
    public function getSuccessMessage():string
    {
        return __('messages.notifications.clear_all');
    }
}

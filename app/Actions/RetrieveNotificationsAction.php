<?php

namespace App\Actions;

use App\Models\Contracts\NotifiableContract;
use App\Repositories\NotificationRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Paginator;

class RetrieveNotificationsAction
{
    protected NotificationRepository $notifications;
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct(NotificationRepository $notifications)
    {
        $this->notifications=$notifications;
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(NotifiableContract $notifiable,int $perpage,?int $page):Paginator
    {
        $notifications=$notifiable->notifications()->latest()
            ->simplePaginate($perpage)
            ->withQueryString();

        if($page==1)
        {
            $notifiable->markAllNotificationsSeen();
        }

        return $notifications;
    }
}

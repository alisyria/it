<?php

namespace App\Actions;

use App\Models\Contracts\NotifiableContract;
use App\Models\Notification;

class DeleteNotificationAction
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Prepare the action for execution, leveraging constructor injection.
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(NotifiableContract $notifiable,Notification $notification):void
    {
        $notifiable->deleteNotification($notification);
    }
    public function getSuccessMessage():string
    {
        return __('messages.notifications.delete');
    }
}

<?php

namespace App\Actions;

use App\Models\Owner;

class StoreOwnerAction
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Prepare the action for execution, leveraging constructor injection.
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(string $name,string $lastName,int $departmentID):Owner
    {
        $owner=new Owner();
        $owner->name=$name;
        $owner->last_name=$lastName;
        $owner->department_id=$departmentID;
        $owner->save();

        return $owner;
    }
}

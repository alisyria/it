<?php

namespace App\Actions;

use App\Enums\OrderStatus;
use App\Models\Device;
use App\Models\Item;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class StoreOrderAction
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Prepare the action for execution, leveraging constructor injection.
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(
        ?Order $order,
        string $title,
        ?string $description,
        OrderStatus $status,
        int $deviceId,
        int $employeeId,
        array $items
    ) {
        DB::transaction(function()use(
            $order, $title, $description, $status, $deviceId, $employeeId, $items
        ) {
            if(is_null($order))
            {
                $order = new Order();
            }

            $order->title = $title;
            $order->description = $description;
            $order->device_id = $deviceId;
            $order->employee_id = $employeeId;
            $order->save();

            $order->transitionFromStatus($status, user());

            $validItemIds = [];
            foreach ($items as $itemID => $itemData)
            {
                $item = Item::where('items.id', $itemID)
                    ->first();
                if(is_null($item)) {
                    continue;
                }

                $validItemIds[]=$itemID;

                $order->items()->syncWithoutDetaching([$itemID => [
                    'value' => $itemData
                ]]);
            }

            $order->items()
                ->whereNotIn('item_order.item_id', $validItemIds)
                ->get()
                ->each
                ->delete();
        });
    }
}

<?php

namespace App\Actions;

use App\Models\Device;
use App\Models\DeviceType;
use App\Models\Property;

class StoreDeviceAction
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Prepare the action for execution, leveraging constructor injection.
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(?Device $device,string $serialNumber,string $model=null,string $manufacturer=null,
                            int $deviceTypeID,string $ownerID,array $options)
    {
        \DB::transaction(function()use(
            $device,$serialNumber,$model,$manufacturer,$deviceTypeID,$ownerID,$options
        ){
            if(is_null($device))
            {
                $device=new Device();
            }
            $device->serial_number=$serialNumber;
            $device->model=$model;
            $device->manufacturer=$manufacturer;
            $device->device_type_id=$deviceTypeID;
            $device->owner_id=$ownerID;
            $device->save();

            $validPropertyIds=[];
            foreach ($options as $propertyID=>$option)
            {
                $property=Property::where('properties.id',$propertyID)
                    ->whereHasMorph('propertiable',[DeviceType::class],
                    fn($q)=>$q->where('device_types.id',$deviceTypeID))
                    ->first();
                if(is_null($property))
                {
                    continue;
                }
                $validPropertyIds[]=$propertyID;
                if($property->isFieldSingleOption())
                {
                    $device->deviceProperties()->updateOrCreate([
                        'property_id'=>$propertyID,
                    ],[
                        'property_id'=>$propertyID,
                        'option_id'=>$option,
                    ]);
                }
                elseif ($property->isFieldTextSingleLine())
                {
                    $device->deviceProperties()->updateOrCreate([
                        'property_id'=>$propertyID,
                    ],[
                        'property_id'=>$propertyID,
                        'value'=>$option,
                    ]);
                }
            }

            $device->deviceProperties()
                ->whereNotIn('device_properties.property_id',$validPropertyIds)
                ->get()
                ->each->delete();
        });
    }
}

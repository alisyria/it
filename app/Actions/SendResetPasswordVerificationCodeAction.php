<?php

namespace App\Actions;

use App\Enums\UserType;
use App\Models\User;
use App\Models\Verification;
use App\Repositories\VerificationRepository;

class SendResetPasswordVerificationCodeAction
{
    private VerificationRepository $verifications;
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct(VerificationRepository $verifications)
    {
        $this->verifications=$verifications;
    }
    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(User $user)
    {
        return $this->verifications->store(
            $user->username,$user->username_type, $user->id
        );
    }

    public function getSuccessMessage(Verification $verification):string
    {
        return $this->verifications->getCodeSentMessage($verification->username_type);
    }
}

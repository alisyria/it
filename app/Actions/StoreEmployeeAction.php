<?php

namespace App\Actions;

use DB;
use App\Enums\UsernameType;
use App\Enums\UserType;
use App\Models\Employee;
use App\Repositories\EmployeeRepository;
use App\Repositories\UserRepository;

class StoreEmployeeAction
{
    public UserRepository $users;
    public EmployeeRepository $employees;
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct(UserRepository $users,EmployeeRepository $employees)
    {
        $this->users=$users;
        $this->employees=$employees;
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(
        ?Employee $employee,string $firstName,string $lastName,UsernameType $usernameType,
        string $username,string $password=null,string $imagePath=null,string $email=null,
        string $mobile=null,array $roles
    ):Employee
    {
        DB::transaction(function()use(
            &$employee,$firstName,$lastName,$usernameType,$username,$password,$imagePath,$email,
            $mobile,$roles
        ){
            $isCreate=is_null($employee);

            if($isCreate)
            {
                $user=$this->users->store(
                    $firstName,null,$lastName,$username,$password,$email,$mobile,
                    $usernameType,UserType::EMPLOYEE(),$imagePath
                );
                $this->users->activate($user);
                $employee=$this->employees->store($user);
            }
            else
            {
                $user=$this->users->update($employee->user,$firstName,null,$lastName,
                    $username,$password,$mobile,$email,$usernameType,null,$imagePath
                );
                $employee=$this->employees->update($employee);
            }
            $user->syncRoles($roles);
        });

        return $employee;
    }
}

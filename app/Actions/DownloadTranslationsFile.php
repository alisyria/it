<?php

namespace App\Actions;

use App\Enums\TranslationType;
use App\Exports\Localization\AppTranslationsExport;

class DownloadTranslationsFile
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(TranslationType $translationType,array $langs=[])
    {
        return (new AppTranslationsExport($translationType))->forLangs($langs)->download();
    }
}

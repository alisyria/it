<?php

namespace App\Actions;

use App\Models\Role;

class DeleteRoleAction
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Prepare the action for execution, leveraging constructor injection.
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(Role $role)
    {
        $role->delete();
    }

    public function getSuccessMessage():string
    {
        return __j('Deleted Successfully');
    }
}

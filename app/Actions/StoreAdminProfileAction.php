<?php

namespace App\Actions;

use App\Enums\UsernameType;
use App\Enums\UserType;
use App\Models\User;
use App\Repositories\UserRepository;
use DB;

class StoreAdminProfileAction
{
    public UserRepository $users;
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users=$users;
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(User $user,string $firstName,string $lastName,string $email=null,
        string $mobile=null,UsernameType $usernameType,string $username,string $newPassword=null,
        string $imagePath=null):User
    {
        DB::transaction(function()use(
            &$user,$firstName,$lastName,$usernameType,$username,$newPassword,$imagePath,$email,
            $mobile
        ){
            $user=$this->users->update($user,$firstName,null,$lastName,$username,
                $newPassword,$mobile,$email,$usernameType,null,$imagePath
            );
        });

        return $user;
    }
}

<?php

namespace App\Actions;

use App\Enums\ApiMessage;
use App\Models\User;
use App\Repositories\RegistrationsRepository;
use App\Repositories\UserRepository;
use App\Repositories\VerificationRepository;

class ResetPasswordAction
{
    protected VerificationRepository $verifications;
    protected RegistrationsRepository $registrations;
    protected UserRepository $users;
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct(
        VerificationRepository $verifications,RegistrationsRepository $registrations,
        UserRepository $users
    )
    {
        $this->verifications=$verifications;
        $this->registrations=$registrations;
        $this->users=$users;
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(User $user, string $code,string $password,
                            string $instanceUuid=null):bool
    {
        if($this->verifications->verify($user, $code))
        {

            $this->users->resetPassword($user,$password);

            if(filled($instanceUuid))
            {
                $this->registrations->associateUser($user->id,$instanceUuid,now());
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    public function getSuccessMessage():string
    {
        return __('messages.customer.reset_password');
    }
    public function getInvalidMessage():string
    {
        return __('errors.verification_code.invalid');
    }
}

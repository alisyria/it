<?php

namespace App\Actions;

use App\Enums\NotificationKey;
use App\Enums\OS;
use App\Http\Resources\ImageResource;
use App\Jobs\SendPushNotificationsChunkJob;
use App\Models\AppLanguage;
use App\Models\Registration;
use App\Models\User;
use App\Notifications\Channels\Notifications\PushNotification;
use App\Values\NotificationStats;
use DB;
use App\Enums\NotificationTarget;
use App\Enums\VendorType;
use App\Repositories\NotificationRepository;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueueableAction\QueueableAction;

class SendGeneralNotification
{
    use QueueableAction;

    public NotificationRepository $notifications;
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct(NotificationRepository $notifications)
    {
        $this->notifications=$notifications;
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(?NotificationTarget $target,?VendorType $vendorType,array $title,array $text,
                            array $governorates=[],array $regions=[],string $image=null)
    {
        DB::transaction(function()use(
            $target,$vendorType,$title,$text,$governorates,$regions,$image
        ){
            $registrationsQuery=Registration::filterNotifiable($target,$vendorType,$governorates,$regions);
            $totalDevices=$registrationsQuery->count();
            $stats=new NotificationStats($totalDevices);
            $key=NotificationKey::GENERAL();
            $data=[];
            if($image)
            {
                $key=NotificationKey::GENERAL_IMAGE();
            }
            $notification=$this->notifications->store(null,$key,
                null,$target,$vendorType,$title,$text,$data,$governorates,
                $regions,$image,$stats);
            dispatch(function () use ($notification,$governorates,$regions) {
                User::forNotification($notification->target,$notification->vendor_type,$governorates,$regions)
                    ->select('users.id')->chunk(1000,function(Collection $users)use($notification){
                        $ids=$users->pluck('id')->toArray();
                        $notification->users()->attach($ids);
                    });
            });

            dispatch(function () use ($notification,$target,$vendorType,$governorates,$regions) {
                $data=$notification->data;
                $pushNotification=new PushNotification($notification->key,
                    $notification->getTranslation('text',AppLanguage::getDefaultLocal()),
                    $data,
                    $notification->getTranslation('title',AppLanguage::getDefaultLocal()));
                Registration::filterNotifiable($target,$vendorType,$governorates,$regions)
                    ->select('reg_id')
                    ->chunk(900,function(Collection $registrations)use($notification,$pushNotification){
                        $tokens=$registrations->pluck('reg_id')->toArray();
                        SendPushNotificationsChunkJob
                            ::dispatch($tokens,OS::Android,AppLanguage::getDefaultLocal(),$pushNotification,
                                $notification);
                    });
            });
        });
    }

    public function getSuccessMessage():string
    {
        return __j('The process of sending notifications has  started ,to track the progress of the process You can see the table below, the process takes a few minutes');
    }
}

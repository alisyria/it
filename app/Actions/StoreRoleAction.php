<?php

namespace App\Actions;

use App\Enums\AuthGuard;
use App\Models\Contracts\Roleable;
use App\Repositories\RoleRepository;
use DB;
use App\Models\Role;

class StoreRoleAction
{
    public RoleRepository $roles;
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct(RoleRepository $roles)
    {
        $this->roles=$roles;
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(?Role $role,string $name,array $description,array $permissions,
                            AuthGuard $authGuard,Roleable $roleable=null)
    {
        DB::transaction(function()use(
            &$role,$name,$description,$permissions,$authGuard,$roleable
        ){
            $isCreate=is_null($role);

            $role=$this->roles->store($role,$name,$description,$authGuard,
                $permissions,$roleable);
        });

        return $role;
    }
}

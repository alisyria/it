<?php

namespace App\Actions;

use App\Models\Employee;

class DeleteEmployeeAction
{
    /**
     * Create a new action instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Prepare the action for execution, leveraging constructor injection.
    }

    /**
     * Execute the action.
     *
     * @return mixed
     */
    public function execute(Employee $employee)
    {
        $employee->delete();
    }

    public function getSuccessMessage():string
    {
        return __j('Deleted Successfully');
    }
}

<?php

namespace App\Exports;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class DevicesExport implements FromQuery,Responsable,WithHeadings,WithMapping,
    WithEvents,WithChunkReading
{
    use Exportable,RegistersEventListeners;

    private Builder $query;

    public function __construct(Builder $query)
    {
        $this->query=$query;
    }

    public function query()
    {
        return $this->query;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function headings(): array
    {
        return [
            'المعرف',
            'معرف الجهاز',
            'الموديل',
            'الشركة الصانعة',
            'الحالة',
            'نوع الجهاز',
            'المستثمر',
            'الدائرة',
            'الخصائص',
        ];
    }

    public function map($row): array
    {
        return [
            'المعرف'=>$row->id,
            'معرف الجهاز'=>$row->serial_number,
            'الموديل'=>$row->model,
            'الشركة الصانعة'=>$row->manufacturer,
            'الحالة'=>$row->state->getStatus()->description,
            'نوع الجهاز'=>optional($row->deviceType)->name,
            'المستثمر'=>optional($row->owner)->full_name,
            'الدائرة'=>optional(optional($row->owner)->department)->name,
            'الخصائص'=>$row->propertiesToText(false),
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->customize(
            'A1:I1',
            [
                'A'=>10,
                'B'=>20,
                'C'=>20,
                'D'=>20,
                'E'=>15,
                'F'=>25,
                'G'=>25,
                'H'=>20,
                'I'=>50,
            ]
        );
        $event->sheet->setRightToLeft(true);
        $event->sheet->freezePane('A2');
//        $event->sheet->getStyle('I1:I200')->getAlignment()->setWrapText(true);
//        foreach (range('B','Z') as $col)
//        {
//            $event->sheet->getDelegate()->getColumnDimension($col)->setWidth(50);
//        }

    }
    public function title(): string
    {
        return 'الأجهزة';
    }
}

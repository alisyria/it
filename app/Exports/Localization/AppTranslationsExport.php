<?php

namespace App\Exports\Localization;

use App\Enums\Platform;
use App\Enums\TranslationType;
use App\Models\AppLanguage;
use App\Models\LanguageLine;
use App\Models\TranslationTerm;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class AppTranslationsExport implements FromCollection,Responsable,WithHeadings,WithMapping,
    WithEvents,WithTitle
{
    use Exportable,RegistersEventListeners;

    public TranslationType $translationType;
    public string $fileName="translations.xlsx";
    public ?array $langs;

    public function __construct(TranslationType $translationType)
    {
        $this->langs=AppLanguage::langs();
        $this->translationType=$translationType;
        $this->fileName='Translations-'.Str::slug($this->translationType->description).".xlsx";
    }

    public function forLangs(array $langs)
    {
        if(filled($langs))
        {
            $this->langs=$langs;
        }
        if(count($langs)==1)
        {
            $fileName=optional(AppLanguage::getAllItems()->where('id',$langs[0])->first())->name ?? 'translations';
            $fileName.='.xlsx';
            $this->fileName=$fileName;
        }
        return $this;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection(): Collection {
        if($this->translationType->is(TranslationType::GENERAL))
        {
            return LanguageLine::get();
        }
        elseif($this->translationType->is(TranslationType::WEBSITE_APP()))
        {
            return TranslationTerm::where('platform',Platform::WEB)->get();
        }
        elseif($this->translationType->is(TranslationType::MOBILE_APP()))
        {
            return TranslationTerm::where('platform',Platform::MOBILE)->get();
        }
    }
    public function map($record): array
    {
        $data=[];
        if($this->translationType->is(TranslationType::GENERAL))
        {
            $data['group']=$record['group'];
            $data['key']=$record['key'];
            $data['default']=__($record['group'].'.'.$record['key'],[],AppLanguage::getDefaultLocal());
            $trans=$record->text;
        }
        else
        {
            $data['key']=$record['key'];
            $data['default']=$record->getTranslation('value',AppLanguage::getDefaultLocal());
            $trans=$record->getTranslations('value');

        }

        foreach ($this->langs as $lang)
        {
            $data[$lang] = data_get($trans, $lang);
        }
        //$data=array_merge($data,$record->getTranslations('value'));
        return $data;
    }
    public function headings():array {
        if($this->translationType->is(TranslationType::GENERAL))
        {
            $headings[]='group';
        }
        $headings[]='key';
        $headings[]='default';
        $headings=array_merge($headings,$this->langs);

        return $headings;
    }
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->customize(
            'A1:Z1',
            [
            ]
        );
        $event->sheet->setRightToLeft(false);
        $event->sheet->freezePane('A2');

        foreach (range('B','Z') as $col)
        {
            $event->sheet->getDelegate()->getColumnDimension($col)->setWidth(50);
        }

    }
    public function title(): string
    {
        return 'Translations-'.$this->translationType->description;
    }

}

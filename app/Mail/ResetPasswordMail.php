<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $resetUrl;
    public $lang;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,string $resetUrl,string $lang)
    {
        parent::__construct($lang);
        $this->user=$user;
        $this->resetUrl=$resetUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__('notifications.reset_password'))
                    ->markdown('emails.notifications.reset-password');
    }
}

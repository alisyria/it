<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable as BaseMailable;
use Illuminate\Queue\SerializesModels;
use App\Models\AppLanguage;

class Mailable extends BaseMailable
{
    use Queueable, SerializesModels;

    public $lang;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($lang)
    {
        if(AppLanguage::isLangDirection($lang,'rtl'))
        {
            $this->theme="rtl";
        }
        $this->lang=$lang;
    }

}

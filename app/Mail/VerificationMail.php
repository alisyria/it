<?php

namespace App\Mail;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Verification;

class VerificationMail extends Mailable
{
    public $user;
    public $verification;
    public $lang;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,Verification $verification,$lang)
    {
        parent::__construct($lang);
        $this->user=$user;
        $this->verification=$verification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__j('verification code'))
                    ->markdown('emails.notifications.verification');
    }
}

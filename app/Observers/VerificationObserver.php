<?php

namespace App\Observers;

use App\Models\Verification;
use App\Notifications\VerificationNotify;
use App\Enums\UsernameType;
use Exception;

class VerificationObserver
{
    /**
     * Handle the verification "created" event.
     *
     * @param  \App\App\Models\Verification  $verification
     * @return void
     */
    public function created(Verification $verification)
    {

            $forMobile=null;
            if($verification->username_type==UsernameType::Mobile)
            {
                $forMobile=$verification->username;
            }
            $verification->user->notify(new VerificationNotify($verification, app()->getLocale(),$forMobile));


    }

    /**
     * Handle the verification "updated" event.
     *
     * @param  \App\App\Models\Verification  $verification
     * @return void
     */
    public function updated(Verification $verification)
    {
        //
    }

    /**
     * Handle the verification "deleted" event.
     *
     * @param  \App\App\Models\Verification  $verification
     * @return void
     */
    public function deleted(Verification $verification)
    {
        //
    }

    /**
     * Handle the verification "restored" event.
     *
     * @param  \App\App\Models\Verification  $verification
     * @return void
     */
    public function restored(Verification $verification)
    {
        //
    }

    /**
     * Handle the verification "force deleted" event.
     *
     * @param  \App\App\Models\Verification  $verification
     * @return void
     */
    public function forceDeleted(Verification $verification)
    {
        //
    }
}

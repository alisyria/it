<?php

namespace App\Observers;

use App\Enums\EnquiryStatus;
use App\Models\Enquiry;
use App\Notifications\EnquiryNotification;
use Notification;

class EnquiryObserver
{
    /**
     * Handle the enquiry "created" event.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return void
     */
    public function created(Enquiry $enquiry)
    {
        $enquiry->notify(new EnquiryNotification($enquiry));
    }

    /**
     * Handle the enquiry "updated" event.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return void
     */
    public function updated(Enquiry $enquiry)
    {
        //
    }

    /**
     * Handle the enquiry "deleted" event.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return void
     */
    public function deleted(Enquiry $enquiry)
    {
        //
    }

    /**
     * Handle the enquiry "restored" event.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return void
     */
    public function restored(Enquiry $enquiry)
    {
        //
    }

    /**
     * Handle the enquiry "force deleted" event.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return void
     */
    public function forceDeleted(Enquiry $enquiry)
    {
        //
    }
}

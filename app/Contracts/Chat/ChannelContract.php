<?php

namespace App\Contracts\Chat;


interface ChannelContract
{
    public function authorize(int $firstUserID,int $secondUserID);
}
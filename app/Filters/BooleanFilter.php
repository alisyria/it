<?php


namespace App\Filters;


class BooleanFilter extends BaseFilter
{

    public function apply($value, $options = []):?bool
    {
        if(blank($value))
        {
            return null;
        }
        if(in_array($value,[true,'true',1,'1','on','yes'],true))
        {
            return true;
        }
        return false;
    }
}

<?php


namespace App\Filters;


use Waavi\Sanitizer\Contracts\Filter;

class HindiNumbersArabicFilter implements Filter
{
    public function apply($value, $options = [])
    {
        return arabicE2w($value);
    }
}
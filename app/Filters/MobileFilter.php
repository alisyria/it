<?php

namespace App\Filters;


use App\Models\Country;
use Sanitizer;

class MobileFilter extends BaseFilter
{

    /**
     *  Return the result of applying this filter to the given input.
     *
     * @param  mixed $value
     * @return mixed
     */
    public function apply($value, $options = [])
    {
        if(blank($value))
        {
            return null;
        }
        $callCode=964;
        if(data_get($options,0))
        {
            $callCode=Country::getCallCodeForCode(data_get($options,0));
        }
        $value=Sanitizer::make([
            'value'=>$value
        ],[
            'value'=>'arabic_number',
        ])->sanitize()['value'];

        return transformMobileE164($value,$callCode);
    }
}
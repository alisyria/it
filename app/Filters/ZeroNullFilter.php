<?php

namespace App\Filters;


class ZeroNullFilter extends BaseFilter
{

    /**
     *  Return the result of applying this filter to the given input.
     *
     * @param  mixed $value
     * @return mixed
     */
    public function apply($value, $options = [])
    {
        return $value==0?null:$value;
    }
}
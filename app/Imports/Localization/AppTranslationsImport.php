<?php

namespace App\Imports\Localization;

use App\Enums\Platform;
use App\Enums\TranslationType;
use App\Models\LanguageLine;
use App\Models\TranslationTerm;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AppTranslationsImport implements ToModel,WithHeadingRow
{
    use Importable;

    public TranslationType $translationType;

    public function __construct(TranslationType $translationType)
    {
        $this->translationType=$translationType;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if(empty($row['key']))
        {
            return;
        }
        if($this->translationType->is(TranslationType::GENERAL()) && empty($row['group']))
        {
            return ;
        }
        $values=array_filter(Arr::except($row,['key','group','default']),function($v,$k){
            return !is_null($v);
        },ARRAY_FILTER_USE_BOTH);
        if(blank(data_get($values,'en')))
        {
            return;
        }
        if($this->translationType->is(TranslationType::GENERAL()))
        {
            $languageLine=LanguageLine::where('group',$row['group'])->whereRaw("BINARY `key`= ?",$row['key'])->first();
            if(!$languageLine)
            {
                return;
            }
            foreach($values as $lang=>$value)
            {
                $languageLine->setTranslation($lang,$value);
            }
            $languageLine->save();
        }
        else
        {
            $platform=Platform::WEB;
            if($this->translationType->is(TranslationType::MOBILE_APP))
            {
                $platform=Platform::MOBILE;
            }
            $translationTerm=TranslationTerm::whereRaw("BINARY `key`= ?",$row['key'])
                ->where('platform',$platform)->first();
            if(is_null($translationTerm))
            {
                $translationTerm=new TranslationTerm([
                    'key'=>$row['key'],
                    'platform'=>$platform,
                ]);
            }
            foreach($values as $lang=>$value)
            {
                $translationTerm->setTranslation('value',$lang,$value);
            }
            $translationTerm->save();
        }
    }
}

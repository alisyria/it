<?php

namespace App\Exceptions;

use App\Exceptions\LoginException;
use Illuminate\Http\Client\RequestException;
use Log;
use Throwable;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Spatie\ModelStates\Exceptions\CouldNotPerformTransition;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Illuminate\Auth\AuthenticationException;
use App\Enums\AuthGuard;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Throwable $exception)
    {
        if($request->wantsJson())// && !app()->environment('local')
        {
            if($exception instanceof ModelNotFoundException)
            {
                return response()->json(["error"=>__j('The requested item is no longer available')],404);
            }
            if($exception instanceof AuthorizationException)
            {
                return response()->json(["error"=>$exception->getMessage()],403);
            }
            if($exception instanceof LoginException)
            {
                return response()->json(["error"=>__('errors.oauth.failed')],403);
            }
            if($exception instanceof ThrottleRequestsException)
            {
                return response()->json(["error"=>__j('You have exceeded maximum number of attempts')],429);
            }
            if($exception instanceof MaintenanceModeException)
            {
                return response()->json(["error"=>__j('The app is currently off for maintenance, please try again later',[],config('app.locale'))],503);
            }
            if($exception instanceof FatalThrowableError)
            {
                return response()->json(["error"=>__j('Server Error',[],config('app.locale'))],500);
            }
            if($exception instanceof HttpException) {
                $message=$exception->getMessage();
                switch($exception->getStatusCode())
                {
                    case 404:
                        $message=__j('The requested item does not exist',[],config('app.locale'));
                        break;
                    case 500:
                        $message=__j('Server Error');
                        break;
                }
                return response()->json(["error"=>$message],$exception->getStatusCode());
            }
        }
        else
        {
            if($exception instanceof AuthorizationException)
            {
                flash(__($exception->getMessage()))->error()->important();
                return back();
            }
            if($exception instanceof CouldNotPerformTransition)
            {
                flash(__('errors.state_transition_denied'))->error()->important();
                return back();
            }
            if($exception instanceof UnauthorizedException)
            {
                flash(__('errors.unauthorized'))->error()->important();
                return back();
            }
            if($exception instanceof ThrottleRequestsException)
            {
                flash(__('errors.throttle',[
                    'retry_after'=>ceil($exception->getHeaders()['Retry-After']/60)
                ]))->error()->important();
                return back();
            }
            if($exception instanceof HttpException) {
                $message=$exception->getMessage();
                switch($exception->getStatusCode())
                {
                    case 403:
                        flash($message)->error()->important();
                        return back();
                        break;
                }
            }
        }
        if($exception instanceof RequestException)
        {
            Log::error('osrm router request failed',[
                'body'=>$exception->response->body(),
            ]);
        }

        return parent::render($request, $exception);
    }

    protected function unauthenticated($request,AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        $firstGuard=$exception->guards()[0];
        $route="/";
        switch($firstGuard)
        {
            case AuthGuard::ADMIN:
                $route="admin.login";
                break;
            case AuthGuard::COMPANY:
                $route="company.login";
                break;
        }
        return redirect()->guest(route($route));
    }
}

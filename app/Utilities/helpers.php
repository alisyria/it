<?php

use App\Enums\AuthGuard;
use App\Enums\CurrencyCode;
use App\Enums\UserType;
use App\Models\AppLanguage;
use App\Models\Country;
use App\Models\Registration;
use App\Repositories\SettingsRepository;
use App\Repositories\RegistrationsRepository;
use App\Values\MoneyCurrencyConversion;
use Cknow\Money\Money;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Models\Media;


if (! function_exists('setting')) {

    function setting($key=null, $default = null,$lang=null)
    {
        $repo=new SettingsRepository();
        if (is_null($key)) {
            return $repo;
        }

        return $repo->get($key,$default,$lang);
    }
}
if (! function_exists('apiMessage')) {
    function apiMessage($text,$type)
    {
        return[
            'text'=>$text,
            'type'=>$type
        ];
    }
}
if (! function_exists('apiMessageResponse')) {
    function apiMessageResponse($text,$type)
    {
        return[
            'message'=>apiMessage($text,$type)
        ];
    }
}
if (! function_exists('webMessage')) {
    function webMessage(?string $title=null,string $message,string $level,bool $important=false,$options=[])
    {
        session()->push('flash_notification',[
            'title'=>$title,
            'message'=>$message,
            'level'=>$level,
            'important'=>$important,
            'options'=>$options
        ]);
    }
}
if(!function_exists('mainMedia'))
{
    function mainMedia($mediaItems)
    {
        $main=$mediaItems->first();
        foreach ($mediaItems as $media)
        {
            if($media->getCustomProperty('is_main', false))
            {
                $main=$media;
                break;
            }
        }
        return $main;
    }
}
if(!function_exists('mediaConversionUrl'))
{
    function mediaConversionUrl(?Media $media,string $conversion):?string
    {
        if(is_null($media))
        {
            return null;
        }
        $url=null;
        if($media->hasGeneratedConversion($conversion))
        {
            $url=$media->getFullUrl($conversion);
        }
        elseif($media->hasGeneratedConversion("default"))
        {
            $url=$media->getFullUrl("default");
        }
        else
        {
            $url= $media->getFullUrl();
        }

        return $url;
    }
}
if(!function_exists('localeDirection'))
{
    function localeDirection()
    {
        if(AppLanguage::isLocalDirection('rtl'))
        {
            return 'right';
        }
        else
        {
            return 'left';
        }
    }
}
if(!function_exists('localeDirectionInv'))
{
    function localeDirectionInv()
    {
        if(AppLanguage::isLocalDirection('rtl'))
        {
            return 'left';
        }
        else
        {
            return 'right';
        }
    }
}
if(!function_exists('langDirection'))
{
    function langDirection(string $lang)
    {
        if(AppLanguage::isLangDirection($lang,'rtl'))
        {
            return 'right';
        }
        else
        {
            return 'left';
        }
    }
}
if(!function_exists('langDirectionInv'))
{
    function langDirectionInv(string $lang)
    {
        if(AppLanguage::isLangDirection($lang,'rtl'))
        {
            return 'left';
        }
        else
        {
            return 'right';
        }
    }
}
if(!function_exists('localClass'))
{
    function localClass()
    {
        if(AppLanguage::isLocalDirection('rtl'))
        {
            return 'rtl';
        }
        else
        {
            return 'ltr';
        }
    }
}
if(!function_exists('localClassInv'))
{
    function localClassInv()
    {
        if(AppLanguage::isLocalDirection('rtl'))
        {
            return 'ltr';
        }
        else
        {
            return 'rtl';
        }
    }
}
if(!function_exists('langClass'))
{
    function langClass(string $lang)
    {
        if(AppLanguage::isLangDirection($lang,'rtl'))
        {
            return 'rtl';
        }
        else
        {
            return 'ltr';
        }
    }
}
if(!function_exists('langRtl'))
{
    function langRtl()
    {
        if(AppLanguage::isLocalDirection('rtl'))
        {
            return true;
        }
        return false;
    }
}
if(!function_exists('checkIfAllLangs'))
{
    function checkIfAllLangs(?array $localizedText)
    {
        if(is_null($localizedText))
        {
            return;
        }
        $localizedTextKeys=array_keys($localizedText);
        $langValues=array_values(AppLanguage::langs());
        sort($localizedTextKeys);
        sort($langValues);
        if($localizedTextKeys!=$langValues)
        {
            throw new RuntimeException('toText method should return array which contain text for all languages:'.implode(AppLanguage::langs(),','));
        }
    }
}

if(!function_exists('mobileNumberMutate')) {
    function mobileNumberMutate($mobile)
    {
        $mobile = ltrim($mobile, '0');
        if (starts_with($mobile, "964") && (strlen($mobile) > 9)) {
            //do nothing
        }
        else
        {
            $mobile = "964" . $mobile;
        }
        return $mobile;
    }
}
if(!function_exists('mobileNumberFormal')) {
    function mobileNumberFormal($mobile)
    {
        return "00".$mobile;
    }
}
/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
if(!function_exists('isActiveRoute')) {
    function isActiveRoute($route, $output = "active")
    {
        if (Route::currentRouteName() == $route) return $output;
    }
}
/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
if(!function_exists('areActiveRoutes')) {
    function areActiveRoutes(Array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }
    }
}
if(!function_exists('appendToQueryString')) {
    function appendToQueryString(\Illuminate\Http\Request $request, array $newQueries)
    {
        //Retrieve current query strings:
        $currentQueries = $request->query();

        //Merge together current and new query strings:
        $allQueries = array_merge($currentQueries, $newQueries);

        //Generate the URL with all the queries:
        return $request->fullUrlWithQuery($allQueries);
    }
}
if(!function_exists('cleanMobileNumber')) {
    function cleanMobileNumber($number)
    {
        //Remove any parentheses and the numbers they contain:
        $number = preg_replace("/\([0-9]+?\)/", "", $number);

        //Strip spaces and non-numeric characters:
        $number = preg_replace("/[^0-9]/", "", $number);

        //Strip out leading zeros:
        $number = ltrim($number, '0');
        return $number;
    }
}
if(!function_exists('transformMobileE164'))
{
    function transformMobileE164($mobile,$callCode)
    {
        $cleanedMobile=cleanMobileNumber($mobile);
        if(!Str::startsWith($cleanedMobile,$callCode))
        {
            $cleanedMobile = $callCode.$cleanedMobile;
        }
        return $cleanedMobile;
    }
}
if(!function_exists('transformMobileE164Country'))
{
    function transformMobileE164Country($mobile,$countryCode=null,$countryID=null)
    {
        $callCode=null;
        if (filled($countryCode))
        {
            $callCode=Country::getCallCodeForCode($countryCode);
        }
        elseif(filled($countryID))
        {
            $callCode=Country::getCallCodeForId($countryID);
        }
        else
        {
            throw new RuntimeException();
        }
        return transformMobileE164($mobile,$callCode);
    }
}
/**
 * Converts numbers in string from western to eastern Arabic numerals.
 *
 * @param  string $str Arbitrary text
 * @return string Text with western Arabic numerals converted into eastern Arabic numerals.
 */
if(!function_exists('arabicW2e')) {
    function arabicW2e($str)
    {
        $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic_western, $arabic_eastern, $str);
    }
}
/**
 * Converts numbers from eastern to western Arabic numerals.
 *
 * @param  string $str Arbitrary text
 * @return string Text with eastern Arabic numerals converted into western Arabic numerals.
 */
if(!function_exists('arabicE2w')) {
    function arabicE2w($str)
    {
        $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic_eastern, $arabic_western, $str);
    }
}
/**
 * Get domain
 */
if(!function_exists('domain')) {
    function domain()
    {
        return parse_url(config('app.url'))['host'];
    }
}
/**
 * Get sub-domain
 */
if(!function_exists('subDomain')) {
    /**
     * @param string $name
     * @return mixed
     */
    function subDomain(string $name)
    {
        return $name.'.'.domain();
    }
}


/*
 * Get or Create Registration by instance uuid
 */
if(!function_exists('getRegistration')) {
    function getRegistration()
    {
        static $registration=null;
        $request=request();

        if(is_null($registration) && $request->filled('instance_uuid'))
        {
            $registration=app(RegistrationsRepository::class)->store($request);
        }

        return $registration;
    }
}

/*
 * use translate function __ to get translation only from json files
 */
if(!function_exists('__j')) {
    function __j($key, $replace = [], $locale = null)
    {
        return __($key,$replace,$locale);
    }
}
/*
 * use translate function trans to get translation only from json files
 */
if(!function_exists('transj')) {
    function transj($key, $number, array $replace = [], $locale = null)
    {
        return trans($key, $number, $replace, $locale);
    }
}
/*
 * use translate function trans_choice to get translation only from json files
 */
if(!function_exists('trans_choice_j')) {
    function trans_choice_j($key, $number, array $replace = [], $locale = null)
    {
        return trans_choice($key, $number, $replace, $locale);
    }
}
/*
 * friendly date time
 */
if(!function_exists('dateTimeFriendly')) {
    function dateTimeFriendly(\Carbon\Carbon $carbon)
    {
        if($carbon->year==now()->year)
        {
            if($carbon->diffInDays()==0)
            {
                return $carbon->format('H:i a');
            }
            elseif($carbon->diffInDays()==1)
            {
                return __j('yesterday');
            }
            else
            {
                return $carbon->format('d M');
            }
        }
        else
        {
            return $carbon->format('Y-m-d');
        }
    }
}
/**
 * Get App domain
 */
if(!function_exists('domain')) {
    function domain()
    {
        return parse_url(config('app.url'))['host'];
    }
}
/*
 * locale configuration
 */
if(!function_exists('locale')) {
    function locale()
    {
        return \App\Utilities\LocaleContext::make();
    }
}
/*
 * general configuration
 */
if(!function_exists('general')) {
    function general()
    {
        return \App\Utilities\GeneralContext::make();
    }
}
/*
 * get current authenticated user
 */
if(!function_exists('user')) {
    function user(string $guard=null):?\App\Models\User
    {
        return auth()->guard($guard)->user();
    }
}
/*
 * get current optional authenticated user
 */
if(!function_exists('userOptional')) {
    function userOptional(string $guard=null):\Illuminate\Support\Optional
    {
        return optional(user($guard));
    }
}
/**
 * Determine if the optionally authenticated user has the given abilities.
 *
 * @param  iterable|string  $abilities
 * @param  array|mixed  $arguments
 * @return bool
 */
if(!function_exists('userCan')) {
    function userCan($abilities, $arguments = []):bool
    {
        $user=user();
        if(is_null($user))
        {
            return false;
        }
        return $user->can($abilities,$arguments);
    }
}

/*
 * get current authenticated admin user
 */
if(!function_exists('adminUser')) {
    function adminUser():?\App\Models\User
    {
        static $user=0;

        if($user===0)
        {
            $user= user(AuthGuard::ADMIN);
        }

        return $user;
    }
}

if(!function_exists('currentWebUser')) {
    function currentWebUser():?\App\Models\User
    {
        static $user=0;

        if($user===0)
        {
            if(adminUser())
            {
                $user=adminUser();
            }
            else
            {
                $user=null;
            }
        }

        return $user;
    }
}
if (! function_exists('apiMessageResponseSuccess')) {
    function apiMessageResponseSuccess(string $text)
    {
        return apiMessageResponse($text,\App\Enums\ApiMessage::SUCCESS);
    }
}
if (! function_exists('apiMessageResponseError')) {
    function apiMessageResponseError(string $text)
    {
        return apiMessageResponse($text,\App\Enums\ApiMessage::ERROR);
    }
}
/*
 * check if value mobile not email
 */
if(!function_exists('isMobileNotEmail')) {
    function isMobileNotEmail($value)
    {
        return !str_contains($value,'@') &&is_numeric(cleanMobileNumber($value));
    }
}
if(!function_exists('transformMobile'))
{
    function transformMobile($mobile)
    {
        return transformMobileE164($mobile,964);
    }
}

/**
 * Get App domain
 */
if(!function_exists('domain')) {
    function domain()
    {
        return parse_url(config('app.url'))['host'];
    }
}
/**
 * Get Admin domain
 */
if(!function_exists('adminDomain')) {
    function adminDomain()
    {
        return (string)Str::of(domain())
            ->after('api.')
            ->prepend('admin.');
    }
}

if(!function_exists('home')) {
    function home()
    {
        return url("https://".(string)Str::of(domain())
            ->after('api.'));
    }
}
/**
 * Get Home Route For Admin
 */
if(!function_exists('adminHome')) {
    function adminHome()
    {
        return route('admin.home');
    }
}

/*
 * guess model instance
 */
if(!function_exists('modelInstance')) {
    /**
     * @param string $modelClass
     * @param $model
     * @param bool $withTrashed
     * @param bool $fail
     * @return \App\Models\Model
     */
    function modelInstance(string  $modelClass, $model, $withTrashed=false, $fail=false):?\App\Models\Model
    {
        if($model instanceof $modelClass)
        {
            return $model;
        }

        $instance=$modelClass::where('id',$model)->when($withTrashed,function($query){
            $query->withTrashed();
        })->first();
        if(is_null($instance) && $fail)
        {
            throw (new \Illuminate\Database\Eloquent\ModelNotFoundException())->setModel($modelClass,
                [$model]);
        }
        return $instance;
    }
}

if(!function_exists('isLangRtl'))
{
    function isLangRtl(string $lang):bool
    {
        return AppLanguage::isLangDirection($lang,'rtl');
    }
}

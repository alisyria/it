<?php

namespace App\Utilities;


use App\Enums\Operation;
use App\Enums\Platform;
use App\Models\Shop;

class LocaleContext
{
    private function __construct()
    {
    }

    public static function make():self
    {
        static $instance=null;

        if(is_null($instance))
        {
            $instance= new self();
        }
        return $instance;
    }
    public function setPlatform(string $platform)
    {
        throw_if(!in_array($platform,Platform::asArray()),new \RuntimeException(
            'platform not supported'
        ));
        return config()->set('custom.app-locale.platform',$platform);
    }
    public function getPlatform():string
    {
        return config('custom.app-locale.platform');
    }
    public function isWebPlaform()
    {
        return $this->getPlatform()==Platform::WEB;
    }
    public function isMobilePlaform()
    {
        return $this->getPlatform()==Platform::MOBILE;
    }
    public function setInstance(string $instance)
    {
        throw_if(validator(['instance'=>$instance],['instance'=>'uuid'])->fails(),
            new \RuntimeException(
            'invalid instance'
        ));
        return config()->set('custom.app-locale.instance',$instance);
    }
    public function getInstance():?string
    {
        return config('custom.app-locale.instance');
    }
    public function guessOperation():string
    {
        if(request()->isMethod('POST'))
        {
            return Operation::CREATE;
        }
        if(request()->isMethod('PUT'))
        {
            return Operation::UPDATE;
        }
        if(request()->isMethod('PATCH'))
        {
            return Operation::UPDATE;
        }
    }
    public function isCreateOperation():bool
    {
        return $this->guessOperation()==Operation::CREATE;
    }
    public function isUpdateOperation():bool
    {
        return $this->guessOperation()==Operation::UPDATE;
    }
    public function shop():?Shop
    {
        return config('custom.local-state.shop.id');
    }
}

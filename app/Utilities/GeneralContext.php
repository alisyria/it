<?php


namespace App\Utilities;


class GeneralContext
{
    private function __construct()
    {
    }

    public static function make():self
    {
        static $instance=null;

        if(is_null($instance))
        {
            $instance= new self();
        }
        return $instance;
    }
}

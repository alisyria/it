<?php


namespace App\Values;


use App\Casts\NotificationStatsCaster;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class NotificationStats implements Castable,Arrayable,Jsonable
{
    public int $totalDevices;
    public int $successDevices;
    public int $failedDevices;

    public function __construct(int $totalDevices=0,int $successDevices=0,int $failedDevices=0)
    {
        $this->totalDevices=$totalDevices;
        $this->successDevices=$successDevices;
        $this->failedDevices=$failedDevices;
    }

    public static function fromArray(array $values):self
    {
        return new static(data_get($values,'total_devices',0),
            data_get($values,'success_devices',0),data_get($values,'failed_devices',0));
    }
    public function toArray()
    {
        return [
            'total_devices'=>$this->totalDevices,
            'success_devices'=>$this->successDevices,
            'failed_devices'=>$this->failedDevices,
        ];
    }
    public static function fromJson(string $values):self
    {
        return static::fromArray(json_decode($values,true));
    }
    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }

    public static function castUsing(array $arguments)
    {
        return NotificationStatsCaster::class;
    }

}

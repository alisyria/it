<?php


namespace App\Values;

use App\Casts\UserExtraCaster;
use Carbon\Carbon;
use Cknow\Money\Money;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class UserExtra implements Castable,Arrayable,Jsonable
{
    public ?Carbon $lastSeenNotifications;

    public function __construct(?Carbon $lastSeenNotifications=NULL)
    {
        $this->lastSeenNotifications=$lastSeenNotifications;
    }

    public function toArray()
    {
        return [
            'last_seen_notifications'=>(string)$this->lastSeenNotifications,
        ];
    }
    public static function castUsing(array $arguments)
    {
        return UserExtraCaster::class;
    }
    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }
    public static function fromArray(array $values):self
    {
        $lastSeenNotifications=data_get($values,'last_seen_notifications');
        if(blank($lastSeenNotifications))
        {
            $lastSeenNotifications=null;
        }
        if(!($lastSeenNotifications instanceof Carbon) && !blank($lastSeenNotifications))
        {
            $lastSeenNotifications=Carbon::parse($lastSeenNotifications);
        }

        return new static($lastSeenNotifications);
    }
    public static function fromJson(string $values):self
    {
        return static::fromArray(json_decode($values,true));
    }
}

<?php

namespace App\Notifications;

use App\Enums\AccountRole;
use App\Enums\NotificationKey;
use App\Enums\OrderStatus;
use App\Models\AppLanguage;
use App\Models\Order;
use App\Notifications\Channels\DatabaseChannel;
use Edujugon\PushNotification\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderStatusNotification extends Notification
{
    use Queueable;

    public $key;
    public Order $order;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->queue='notifications';

        $this->order=$order;
        $this->key=NotificationKey::ORDER_STATUS;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            FcmChannel::class,
            DatabaseChannel::class,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toFcm($notifiable)
    {
        return (new PushMessage)
            ->title(__('Order #:order',['order'=>$this->order->id]))
            ->body($this->notificationText(app()->getLocale()))
            ->sound('default')
            ->clickAction('FLUTTER_NOTIFICATION_CLICK')
            ->extra($this->toArray($notifiable));
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'key'=>$this->key,
            'order'=>[
                'id'=>$this->order->id,
                'status'=>$this->order->state,
            ]
        ];
    }
    public function toText($notifiable)
    {
        $texts=[];
        foreach (AppLanguage::getSupportedLocales() as $locale)
        {
            $texts[$locale->id]=$this->notificationText($locale->id);
        }
        return $texts;
    }
    public function toKey($notifiable):string
    {
        return  $this->key;
    }
    private function notificationText($lang):string
    {
        $text='';
        switch ((string)$this->order->state)
        {
            case OrderStatus::PROCESSING:
                $text=setting('notifications_order_shop_processing','',$lang);
                break;
            case OrderStatus::REJECTED:
                $text=setting('notifications_order_shop_rejected','',$lang);
                break;
            case OrderStatus::SHIPPING:
                $text=setting('notifications_order_shop_shipping','',$lang);
                break;
            case OrderStatus::DELIVERED:
                $text=setting('notifications_order_shop_delivered','',$lang);
                break;
            case OrderStatus::FAILED:
                $text=setting('notifications_order_shop_failed','',$lang);
                break;
        }
        $text=str_replace([':order_no'],[$this->order->id],$text);
        $text=str_replace([':company_name'],[$this->order->company->name],$text);

        return $text;
    }
}

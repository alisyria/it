<?php

namespace App\Notifications;

use App\Models\AppLanguage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\Channels\Contracts\SMSNotificationChannel;
use App\Mail\VerificationMail;
use App\Enums\UsernameType;
use App\Models\Verification;

class VerificationNotify extends Notification
{
    use Queueable;

    public $verification;
    public $lang;
    public $forUsername=true;
    public $forMobile=null;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Verification $verification,string $lang,string $forMobile=null)
    {
        $this->verification=$verification;
        $this->lang=$lang;
        $this->forMobile=$forMobile;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        switch($this->verification->username_type)
        {
            case UsernameType::Email:
                return ['mail'];
            case UsernameType::Mobile:
                return [get_class(app(SMSNotificationChannel::class))];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new VerificationMail($notifiable,$this->verification,$this->lang))
                ->to($this->verification->username);
    }
    public function toSMS($notifiable)
    {
        return $this->verification->code;
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
}

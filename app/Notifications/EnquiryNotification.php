<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Mail\EnquiryMailable;
use App\Models\Enquiry;

class EnquiryNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $lang;
    public $enquiry;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Enquiry $enquiry)
    {
        $this->queue='notifications';

        $this->lang=config('custom.panel.locale');
        $this->enquiry=$enquiry;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return ((new EnquiryMailable($this->lang,$this->enquiry))
                ->locale($notifiable->preferredLocale()))
                ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

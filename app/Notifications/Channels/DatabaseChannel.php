<?php

namespace App\Notifications\Channels;


use App\Models\AppLanguage;
use App\Notifications\Channels\Contracts\DatabaseNotificationChannel;
use Illuminate\Notifications\Notification;
use RuntimeException;

class DatabaseChannel implements DatabaseNotificationChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function send($notifiable, Notification $notification)
    {
        return $notifiable->routeNotificationFor('database', $notification)->create(
            $this->buildPayload($notifiable, $notification)
        );
    }

    /**
     * Get the data for the notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array
     *
     * @throws \RuntimeException
     */
    protected function getData($notifiable, Notification $notification)
    {
        if (method_exists($notification, 'toDatabase')) {
            return is_array($data = $notification->toDatabase($notifiable))
                ? $data : $data->data;
        }

        if (method_exists($notification, 'toArray')) {
            return $notification->toArray($notifiable);
        }

        throw new RuntimeException('Notification is missing toDatabaseCustom / toArray method.');
    }
    /**
     * Get the data for the notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array
     *
     * @throws \RuntimeException
     */
    protected function getText($notifiable, Notification $notification):array
    {
        if (method_exists($notification, 'toText')) {
            $result=$notification->toText($notifiable);
            checkIfAllLangs($result);
            return $result;
        }
        else
        {
            throw new RuntimeException('Notification is missing toText method.');
        }

    }
    protected function getKey($notifiable, Notification $notification):?string
    {
        if (method_exists($notification, 'toKey')) {
            $result=$notification->toKey($notifiable);
            return $result;
        }
        else
        {
                return null;
        }
    }
    /**
     * Build an array payload for the DatabaseNotification Model.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array
     */
    protected function buildPayload($notifiable, Notification $notification)
    {
        return [
            'id' => $notification->id,
            'key'=>$this->getKey($notifiable,$notification),
            'type' => get_class($notification),
            'data' => $this->getData($notifiable, $notification),
            'text'=> $this->getText($notifiable, $notification)
        ];
    }
}
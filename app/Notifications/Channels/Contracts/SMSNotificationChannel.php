<?php

namespace App\Notifications\Channels\Contracts;

use Illuminate\Notifications\Notification;

interface SMSNotificationChannel
{
    /**
     * Send the given sms to mobile.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification);
}
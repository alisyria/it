<?php

namespace App\Notifications\Channels\Contracts;

use Illuminate\Notifications\Notification;

interface PushNotificationChannel
{
    /**
     * Send the given notification to all notifiable devices.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification);

    /**
     * Send push notification to ios specified tokens
     * @param array $tokens
     * @param string $key
     * @param $messages
     * @param array $data
     * @param string $lang
     * @return void
     */
    public function sendIOS($tokens, Notification $notification);

    /**
     * Send push notification to android specified tokens
     * @param array $tokens
     * @param string $key
     * @param $messages
     * @param array $data
     * @param string $lang
     * @return void
     */
    public function sendAndroid($tokens, Notification $notification);
}
<?php

namespace App\Notifications\Channels\Contracts;

use Illuminate\Notifications\Notification;

interface DatabaseNotificationChannel
{
    /**
     * Store Notification to database
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification);
}
<?php


namespace App\Notifications\Channels;


use App\Notifications\Channels\Contracts\SMSNotificationChannel;
use Illuminate\Notifications\Notification;
use GuzzleHttp\Client as GuzzleClient;
use Log;

class ZainSMSChannel implements SMSNotificationChannel
{
    /**
     * Send the given sms to mobile.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $lang=$notification->lang ?? config('app.locale');
        $credentials=  config('services.zain.credentials');
        $credentials['lang']=$lang;
        $credentials['receiver']=$this->getMobileNumber($notifiable,$notification);
        $credentials['code']=$this->getMessage($notifiable,$notification);
        $httpClient=new GuzzleClient();
        $url=config('services.zain.url');
        $response=$httpClient->request('GET', $url, [
            'query' => $credentials
        ]);
        $this->logSMS($credentials,$response);
    }

    /**
     * @param $notifiable
     * @param Notification $notification
     * @return mixed
     */
    public function getMobileNumber($notifiable, Notification $notification)
    {
        return $notifiable->routeSMS($notification);
    }
    public function getMessage($notifiable,Notification $notification):string
    {
        return $notification->toSMS($notifiable);
    }
    /**
     * @param array $credentials
     * @param $response
     */
    public function logSMS(array $credentials, $response)
    {
        if(config('services.zain.log_enabled'))
        {
            Log::debug("zain sms  has been sent",[
                'credentials'=>$credentials,
                'response'=>(string)$response->getBody()->getContents()
            ]);
        }
    }
}

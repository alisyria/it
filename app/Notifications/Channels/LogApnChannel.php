<?php

namespace App\Notifications\Channels;

use App\Enums\OS;
use Illuminate\Notifications\Notification;
use App\Notifications\Channels\Repositories\PushNotificationRepository;

class LogApnChannel
{
    public $pushService;

    public function __construct(PushNotificationRepository $pushService)
    {
        $this->pushService=$pushService;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $this->pushService->checkForAPNChannelMethods($notifiable,$notification);

        $pushNotification = $notification->toPushNottification($notifiable);
        $registrations=$notifiable->routeNotificationForAPN($notification);

        $this->pushService->sendLocalizedLog(OS::IOS,$registrations,$pushNotification);
    }
}
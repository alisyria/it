<?php

namespace App\Notifications\Channels;

use App\Enums\OS;
use Illuminate\Notifications\Notification;
use App\Notifications\Channels\Repositories\PushNotificationRepository;

class LogAnroidChannel
{
    public $pushService;

    public function __construct(PushNotificationRepository $pushService)
    {
        $this->pushService=$pushService;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $this->pushService->checkForAnroidChannelMethods($notifiable,$notification);

        $pushNotification = $notification->toPushNottification($notifiable);
        $registrations=$notifiable->routeNotificationForAndroid($notification);

        $this->pushService->sendLocalizedLog(OS::Android,$registrations,$pushNotification);
    }
}
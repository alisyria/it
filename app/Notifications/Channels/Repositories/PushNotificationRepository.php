<?php

namespace App\Notifications\Channels\Repositories;

use Log;
use FCM;
use App\Enums\OS;
use App\Models\AppLanguage;
use RuntimeException;
use PushNotification as DavibennunPushNotification;
use LaravelFCM\Message\PayloadDataBuilder;
use Illuminate\Notifications\Notification;
use App\Notifications\Channels\Notifications\PushNotification;


class PushNotificationRepository
{

    public function sendLocalizedLog(string $os,$registrations,PushNotification $pushNotification)
    {
        $this->sendLocalized($os,$registrations,$pushNotification,true);
    }
    public function sendLocalized(string $os,$registrations,PushNotification $pushNotification,bool $log=null):void
    {
        foreach(AppLanguage::langs() as $lang)
        {
            $localTokens=$registrations->where('lang',$lang)->pluck('reg_id')->toArray();
            if(!$log)
            {
                $this->send($os,$localTokens,$lang,$pushNotification);
            }
            else
            {
                $this->sendLog($os,$localTokens,$lang,$pushNotification);
            }
        }
    }

    public function send(string $os,$tokens,string $lang,PushNotification $pushNotification):void
    {
        if(count($tokens)==0)
        {
            return;
        }
        switch ($os)
        {
            case OS::Android:
                $this->sendAndroid($tokens,$lang,$pushNotification);
                break;
            case OS::IOS:
                $this->sendIOS($tokens,$lang,$pushNotification);
                break;
            default:
                throw new RuntimeException('Invalid Os');
        }
    }
    public function sendLog(string $os,$tokens,string $lang,PushNotification $pushNotification):void
    {
        if(count($tokens)==0)
        {
            return;
        }
        switch ($os)
        {
            case OS::Android:
                $this->sendAndroidFake($tokens,$lang,$pushNotification);
                break;
            case OS::IOS:
                $this->sendIOSFake($tokens,$lang,$pushNotification);
                break;
            default:
                throw new RuntimeException('Invalid Os');
        }
    }
    public function sendAndroid(array $tokens,string $lang,PushNotification $pushNotification):void
    {
        $dataNotification=$pushNotification->toArray();
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($dataNotification);
        $dataObject = $dataBuilder->build();

        FCM::sendTo($tokens,Null,Null,$dataObject);
    }
    public function sendIOS(array $tokens,string $lang,PushNotification $pushNotification):void
    {
        $dataNotification=$pushNotification->toArray();
        $iosMessage = DavibennunPushNotification::Message($pushNotification->messages[$lang],[
            'content_available' => true,
            'priority'=>'high',
            "sound" => "default",
            "title" => setting('app_name'),
            "custom"=>$dataNotification
        ]);
        $count=count($tokens);
        for($i=0;$i<$count;$i++)
        {
            $devicesIOS[$i]=DavibennunPushNotification::Device($tokens[$i]);
        }
        $devicesCollectionIOS = DavibennunPushNotification::DeviceCollection($devicesIOS);
        DavibennunPushNotification::app('appNameIOS')
                ->to($devicesCollectionIOS)
                ->send($iosMessage);
    }

    public function sendAndroidFake(array $tokens,string $lang,PushNotification $pushNotification):void
    {
        Log::debug("android - ".now()->toDateTimeString(),[
            'key'=>$pushNotification->key,
            'messages'=>$pushNotification->messages,
            'data'=>$pushNotification->data,
            'lang'=>$lang,
            'tokens'=>$tokens
        ]);
    }
    public function sendIOSFake(array $tokens,string $lang,PushNotification $pushNotification):void
    {
        Log::debug("ios - ".now()->toDateTimeString(),[
            'key'=>$pushNotification->key,
            'messages'=>$pushNotification->messages,
            'data'=>$pushNotification->data,
            'lang'=>$lang,
            'tokens'=>$tokens
        ]);
    }
    public function checkForAnroidChannelMethods($notifiable,Notification $notification)
    {
        if(!method_exists($notifiable,'routeNotificationForAndroid'))
        {
            throw new RuntimeException('Notifiable is missing "routeNotificationForAndroid" method');
        }
        $this->checkNotification($notification);
    }
    public function checkForAPNChannelMethods($notifiable,Notification $notification)
    {
        if(!method_exists($notifiable,'routeNotificationForAPN'))
        {
            throw new RuntimeException('Notifiable is missing "routeNotificationForAPN" method');
        }
        $this->checkNotification($notification);
    }
    public function checkNotification(Notification $notification)
    {
        if(!method_exists($notification,'toPushNottification'))
        {
            throw new RuntimeException('Notification is missing "toPushNottification" method');
        }
    }
}
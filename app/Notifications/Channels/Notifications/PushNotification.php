<?php

namespace App\Notifications\Channels\Notifications;


use App\Models\AppLanguage;

class PushNotification
{
    public $key;
    public $title;
    public $message;
    public $data;

    public function __construct(string $key,string $message,array $data,string $title=null)
    {
        $this->setKey($key);
        $this->setTitles($title ?? '');
        $this->setMessages($message);
        $this->setData($data);
    }
    public function setKey(string $key)
    {
        $this->key=$key;
    }
    public function setTitles(string $title=null)
    {
        if(blank($title))
        {
            $this->title=setting('app_name',setting('app_name'));
        }
        $this->title=$title;
    }
    public function setMessages(string $message)
    {
        $this->message=$message;
    }
    public function setData(array $data)
    {
        $this->data=$data;
    }
    public function toArray():array
    {
        return [
            'key'=>$this->key,
            'title'=>$this->title,
            'message'=>$this->message,
            'data'=>$this->data,
        ];
    }
}

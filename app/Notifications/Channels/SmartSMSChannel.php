<?php

namespace App\Notifications\Channels;

use App\Notifications\Channels\Contracts\SMSNotificationChannel;
use Illuminate\Notifications\Notification;
use GuzzleHttp\Client as GuzzleClient;
use App\Models\AppLanguage;
use Illuminate\Support\Str;
use Log;

class SmartSMSChannel implements SMSNotificationChannel
{

    /**
     * Send the given sms to mobile.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $credentials=  config('services.smart_sms.credentials');
        $credentials['destination']=$this->getMobileNumber($notifiable,$notification);
        $credentials['source']=title_case(Str::slug(config('app.name')));
        $credentials['message']=$this->getMessage($notifiable,$notification);
        $httpClient=new GuzzleClient();
        $url=config('services.smart_sms.url');
        $response=$httpClient->request('GET', $url, [
            'query' => $credentials
        ]);
        $this->logSMS($credentials,$response);
    }

    /**
     * @param $notifiable
     * @param Notification $notification
     * @return mixed
     */
    public function getMobileNumber($notifiable, Notification $notification)
    {
        return $notifiable->routeSMS($notification);
    }

    /**
     * @param Notification $notification
     * @param $notifiable
     * @return string
     */
    public function getMessage($notifiable,Notification $notification):string
    {
        $message= $notification->toSMS($notifiable);
//        $lang=$notification->lang ?? config(app.locale);
//        if(AppLanguage::isUnicode($notification->lang))
//        {
//            return $this->formatMessageUnicode($message);
//        }
        return $message;
    }

    /**
     * @param $message
     * @return string
     */
    public function formatMessageUnicode($message):string
    {
        return $message;
    }
    /**
     * @param array $credentials
     * @param $response
     */
    public function logSMS(array $credentials, $response)
    {
        if(config('services.smart_sms.log_enabled'))
        {
            Log::debug("smart sms  has been sent",[
                'credentials'=>$credentials,
                'response'=>(string)$response->getBody()->getContents()
            ]);
        }
    }
}


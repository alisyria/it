<?php

namespace App\Notifications\Channels;

use App\Models\Channel;
use App\Models\User;
use Illuminate\Notifications\Notification;
use App\Repositories\MessageRepository;
use RuntimeException;

class MessageChannel
{
    public $messageService;

    public function __construct(MessageRepository $messageService)
    {
        $this->messageService=$messageService;
    }
    /**
     * Send the given message to socket.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $channel=$this->getChannel($notifiable,$notification);
        $sender=$this->getSender($notifiable,$notification);
        $body=$this->getBody($notifiable,$notification);
        $messageTypeID=$this->getMessageTypeID($notifiable,$notification);
        $this->messageService->store($sender,$body,$messageTypeID,$channel->id,null);
    }
    public function getMessageTypeID($notifiable, Notification $notification):?string
    {
        if (property_exists($notification, 'messageTypeID')) {
            return $notification->messageTypeID;
        }

        throw new RuntimeException('Notification is missing messageTypeID property.');
    }
    public function getBody($notifiable, Notification $notification):?string
    {
        if (property_exists($notification, 'body')) {
            return $notification->body;
        }

        throw new RuntimeException('Notification is missing body property.');
    }
    public function getSender($notifiable, Notification $notification):User
    {
        if (property_exists($notification, 'sender')) {
            return $notification->sender;
        }

        throw new RuntimeException('Notification is missing sender property.');
    }
    public function getChannel($notifiable, Notification $notification):Channel
    {
        if (method_exists($notification, 'forChannel')) {
            return $notification->forChannel($notifiable);
        }

        throw new RuntimeException('Notification is missing forChannel method.');
    }
}
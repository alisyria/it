<?php

namespace App\Notifications\Channels;

use App\Notifications\Channels\Contracts\PushNotificationChannel;
use Illuminate\Notifications\Notification;


class ProductionPushChannel implements PushNotificationChannel
{

    public $apn;
    public $android;

    public function __construct(APNChannel $apn,AndroidChannel $android)
    {
        $this->apn=$apn;
        $this->android=$android;
    }

    /**
     * Send the given notification to all notifiable devices.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $this->sendAndroid($notifiable,$notification);
        $this->sendIOS($notifiable,$notification);
    }

    /**
     * Send push notification to ios specified tokens
     * @param array $tokens
     * @param string $key
     * @param $messages
     * @param array $data
     * @param string $lang
     * @return void
     */
    public function sendIOS($notifiable,Notification $notification)
    {
        $this->apn->send($notifiable,$notification);
    }

    /**
     * Send push notification to android specified tokens
     * @param array $tokens
     * @param string $key
     * @param $messages
     * @param array $data
     * @param string $lang
     * @return void
     */
    public function sendAndroid($notifiable,Notification $notification)
    {
        $this->android->send($notifiable,$notification);
    }
}
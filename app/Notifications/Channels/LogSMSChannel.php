<?php

namespace App\Notifications\Channels;

use App\Notifications\Channels\Contracts\SMSNotificationChannel;
use Illuminate\Notifications\Notification;
use Log;

class LogSMSChannel implements SMSNotificationChannel
{

    /**
     * Send the given sms to mobile.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message= $notification->toSMS($notifiable);
        $mobileNumber=$notifiable->routeSMS($notification);
        Log::debug('log sms has been sent',[
            'message'=>$message,
            'mobile_number'=>$mobileNumber
        ]);
    }
}
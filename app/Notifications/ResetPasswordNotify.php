<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Mail\ResetPasswordMail;
use App\Models\User;

class ResetPasswordNotify extends Notification
{
    use Queueable;

    public $user;
    public $resetUrl;
    public $lang;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,string $resetUrl,string $lang)
    {
        $this->lang=$lang;
        $this->user=$user;
        $this->resetUrl=$resetUrl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new ResetPasswordMail($notifiable,$this->resetUrl,$this->lang))
                ->to($notifiable->username);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

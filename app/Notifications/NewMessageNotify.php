<?php

namespace App\Notifications;

use App\Models\AppLanguage;
use App\Enums\NotificationKey;
use App\Http\Resources\Chat\MessageResource;
use App\Models\Message;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\Channels\Contracts\PushNotificationChannel;
use App\Notifications\Channels\Notifications\PushNotification;

class NewMessageNotify extends Notification
{
    use Queueable;

    public $message;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Message $message,User $user)
    {
        $this->message=$message;
        $this->user=$user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            get_class(app(PushNotificationChannel::class)),
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }
    public function toPushNottification($notifiable):PushNotification
    {
        return new PushNotification(NotificationKey::NEW_MESSAGE,
            $this->toText($notifiable),$this->toArray($notifiable),$this->message->user->full_name);
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message'=> new MessageResource($this->message)
        ];
    }
    public function toText($notifiable)
    {
        $text=$this->message->body;
        return [
            'ar'=>$text,
            'en'=>$text,
        ];
    }
}

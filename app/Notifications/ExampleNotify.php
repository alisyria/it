<?php

namespace App\Notifications;

use App\Models\AppLanguage;
use App\Notifications\Channels\DatabaseChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\Channels\Contracts\PushNotificationChannel;
use App\Notifications\Channels\Notifications\PushNotification;

class ExampleNotify extends Notification
{
    use Queueable;

    public $lang;
    public $key;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(string $lang)
    {
        $this->lang=$lang;
        $this->key="example";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail',get_class(app(PushNotificationChannel::class)),
            DatabaseChannel::class,];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }
    public function toPushNottification($notifiable):PushNotification
    {
        return new PushNotification($this->key,
            $this->toText($notifiable),$this->toArray($notifiable));
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
    public function toKey($notifiable):string
    {
        return  $this->key;
    }
    public function toText($notifiable)
    {
        return [
            'ar'=>__('',[],'ar'),
            'en'=>__('',[],'en')
        ];
    }
}

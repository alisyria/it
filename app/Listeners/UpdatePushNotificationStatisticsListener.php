<?php

namespace App\Listeners;

use App\Events\PushNotificationChunkJobProcessedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\PushNotificationChunkEvent;
use App\Enums\NotificationStatistic;

class UpdatePushNotificationStatisticsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PushNotificationChunkJobProcessedEvent  $event
     * @return void
     */
    public function handle(PushNotificationChunkJobProcessedEvent $event)
    {
        $notification=$event->job->notification;

        switch($event->status)
        {
            case "processed":
                $notification->statistics->successDevices+=count($event->job->tokens);
                $notification->save();
                break;
            case "failed":
                $notification->statistics->failedDevices+=count($event->job->tokens);
                $notification->save();
                break;
        }

    }
}

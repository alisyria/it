<?php

namespace App\Listeners;

use App\Events\MessageCreatedEvent;
use App\Notifications\NewMessageNotify;
use App\Repositories\MessageRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessageListener
{
    public $messages;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MessageRepository $messages)
    {
        $this->messages=$messages;
    }

    /**
     * Handle the event.
     *
     * @param  MessageCreatedEvent  $event
     * @return void
     */
    public function handle(MessageCreatedEvent $event)
    {
        $message=$event->message;
        $channel=$event->message->channel;
        $user=$event->user;
        $participants=$message->channel->users;
        foreach ($participants as $participant)
        {
            if($participant->isOffline())
            {
                $participant->notify(new NewMessageNotify($message,$participant));
            }
        }
        $this->messages->sendMessageOverSocket($channel->name,$message);
    }

}
////send to other users private
//$otherUsers=$participants->where('id',$user->id);
//foreach ($otherUsers as $otherUser)
//{
//    $otherUserChannelName=$this->messages->channelService
//        ->getPrivateUserChannelName($otherUser);
//    $this->messages->sendMessageOverSocket($otherUserChannelName,$message,'request_join');
//}
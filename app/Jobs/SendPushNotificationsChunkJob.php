<?php

namespace App\Jobs;

use App\Models\AppLanguage;
use Edujugon\PushNotification\Facades\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Notification;
use App\Notifications\Channels\Notifications\PushNotification as PushNotificationInstance;

class SendPushNotificationsChunkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;

    public $tokens;
    public $os;
    public $lang;
    public $notification;
    public $pushNotification;
    public $isProduction;

    public function retryUntil()
    {
        return now()->addMinutes(30);
    }
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $tokens,string $os,string $lang,PushNotificationInstance $pushNotification,
        Notification $notification)
    {
        $this->tokens=$tokens;
        $this->os=$os;
        $this->lang=$lang;
        $this->pushNotification=$pushNotification;
        $this->notification=$notification;
        $this->isProduction=config('custom.notification.general_push.production');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data=$this->pushNotification->toArray();
        $data['click_action']='FLUTTER_NOTIFICATION_CLICK';

        PushNotification::setService('fcm')
            ->setMessage([
                'notification' => [
                    'title'=>$this->notification->getTranslation('title',$this->lang),
                    'body'=>$this->notification->getTranslation('text',$this->lang),
                    'sound' => 'default',
                    'color' => '#000000',
                ],
                'data' => $data,
            ])
            ->setDevicesToken($this->tokens)
            ->send();
    }
}

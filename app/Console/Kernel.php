<?php

namespace App\Console;

use App\Console\Commands\UpdateActiveCompanyPackagesCommand;
use App\Console\Commands\UpdateOfferStatesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Commands\LogCommand;
use App\Enums\ENV;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('telescope:prune')
            ->monthly()
            ->runInBackground()
            ->environments([ENV::LOCAL]);
        $schedule->command('telescope:prune')
            ->weekly()
            ->runInBackground()
            ->environments([ENV::PRODUCTION,ENV::STAGING]);

        $schedule->command('horizon:snapshot')->everyFiveMinutes();

        $schedule->command(UpdateOfferStatesCommand::class)
            ->everyThirtyMinutes()
            ->runInBackground();
        $schedule->command(UpdateActiveCompanyPackagesCommand::class)
            ->monthly()
            ->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;

class SyncAppCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Settings & Permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('db:seed',[
            '--class'=>'AppLanguagesSeeder','--force'=>true
        ]);
        $this->call('db:seed',[
            '--class'=>'PermissionsSeeder','--force'=>true
        ]);
        $this->call('db:seed',[
            '--class'=>'RolesSeeder','--force'=>true
        ]);
        $this->call('db:seed',[
            '--class'=>'SettingsSeeder','--force'=>true
        ]);

        Artisan::queue('translations:sync');

        $this->info('App sync finished');
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Offer;
use App\Models\States\Offer\Active;
use App\Models\States\Offer\Waiting;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class UpdateOfferStatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-offers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update offer states to active or finished';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Offer::whereState('state',[Waiting::class,Active::class])
            ->chunk(500,fn(Collection $offers)=>$offers->each->updateState());

        return 0;
    }
}

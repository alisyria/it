<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class UpdateActiveCompanyPackagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-active-packages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update active company packages monthly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Company::has('activePackage')
            ->with('activePackage')
            ->chunk(100,function (Collection $companies){
                $companies->each->resetActivePackage();
            });
        return 0;
    }
}

<?php

namespace App\Console\Commands\Translations;

use Illuminate\Console\Command;
use App\Repositories\TranslationRepository;

class ExtractCommand extends Command
{
    public $translations;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:extract';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import translations from project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TranslationRepository $translations)
    {
        parent::__construct();
        $this->translations=$translations;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->translations->extractFromProjectToJson();
    }
}

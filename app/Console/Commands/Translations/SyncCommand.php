<?php

namespace App\Console\Commands\Translations;

use App\Repositories\TranslationRepository;
use Artisan;
use Illuminate\Console\Command;

class SyncCommand extends Command
{
    public $translations;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync translations between project & database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TranslationRepository $translations)
    {
        parent::__construct();
        $this->translations=$translations;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call(ExtractCommand::class);
        Artisan::call(ImportDatabaseCommand::class);
    }
}

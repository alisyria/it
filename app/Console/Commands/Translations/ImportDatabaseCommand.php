<?php

namespace App\Console\Commands\Translations;

use App\Repositories\TranslationRepository;
use Illuminate\Console\Command;

class ImportDatabaseCommand extends Command
{
    public $translations;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:import-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import translations to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TranslationRepository $translations)
    {
        parent::__construct();
        $this->translations=$translations;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->translations->exportAllToDatabase();
    }
}

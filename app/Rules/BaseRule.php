<?php
namespace App\Rules;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\Rule;

class BaseRule
{
    protected $validator = null;
    /**
     * @param mixed $value
     * @param array|string|Rule $rules
     * @param string $name Name of the property (optional)
     *
     * @return boolean
     */
    protected function validate($value, $rules, $name = 'variable',$messages=[], $customAttributes=[])
    {
        if (!is_string($rules) && !is_array($rules)) {
            $rules = [$rules];
        }
        $this->validator = Validator::make([$name => $value], [$name => $rules],
            $messages,$customAttributes);

        return $this->validator->passes();
    }
    /**
     * @return null|Validator
     */
    protected function getValidator()
    {
        return $this->validator;
    }
    public function getAttribute(string $name,string $attribute)
    {
        $trans=[];
        if($this->transBase)
        {
            $trans[$name]=__($this->transBase.'.'.$attribute);
        }
        return $trans;
    }
}
<?php

namespace App\Rules\Custom;

use App\Models\User;
use App\Models\Verification;
use App\Repositories\VerificationRepository;
use Illuminate\Contracts\Validation\Rule;

class VerificationCodeValidRule implements Rule
{
    protected VerificationRepository $verifications;

    public ?string $verificationCode;
    public User $user;
    public string $username;
    public string $usernameTypeID;

    public ?Verification $verification=null;
    private string $message;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?string $verificationCode,User $user,string $username,
                                string $usernameTypeID)
    {
        $this->verifications=app(VerificationRepository::class);

        $this->verificationCode=$verificationCode;
        $this->user=$user;
        $this->username=$username;
        $this->usernameTypeID=$usernameTypeID;
        if(filled($verificationCode))
        {
            $this->verification=$this->verifications->findVerification($this->user,
                $this->username,$this->usernameTypeID,$this->verificationCode);
        }
        $this->message=__j("Invalid Verification Code");
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!$this->isExist())
        {
            $this->message=__('rules.custom.verification_code.not_exist');
            return false;
        }
        elseif ($this->isExist() && $this->isExpired())
        {
            $this->message=__('rules.custom.verification_code.expired');
            return false;
        }
        return true;
    }
    private function isExist()
    {
        return filled($this->verification);
    }
    private function isExpired()
    {
        return !$this->verifications->valid($this->verification);
    }
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}

<?php

namespace App\Rules\Custom;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Hash;

class ValidCurrentPasswordRule implements Rule
{
    protected User $user;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user=$user;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Hash::check($value,$this->user->password);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('rules.custom.password.invalid_current');
    }
}

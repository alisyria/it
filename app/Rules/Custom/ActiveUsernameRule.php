<?php

namespace App\Rules\Custom;

use App\Enums\UsernameType;
use App\Enums\UserType;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Hash;

class ActiveUsernameRule implements Rule
{
    protected string $message;
    protected string $usernameTypeID;
    protected string $userTypeID;
    protected string $password;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $usernameTypeID,string $userTypeID,
        string $password)
    {
        $this->usernameTypeID=$usernameTypeID;
        $this->userTypeID=$userTypeID;
        $this->password=$password;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!$this->userExists($value))
        {
            $this->message=__('errors.invalid_username_pass',[
                'usernameType'=>__(UsernameType::getDescription($this->usernameTypeID))
            ]);
            return false;
        }
        return true;
    }

    private function userExists(string $username):bool
    {
        $user= User::findUser($this->usernameTypeID,$username,
            $this->userTypeID)->first();
        if(is_null($user))
        {
            return FALSE;
        }
        return Hash::check($this->password,$user->password);
    }
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}

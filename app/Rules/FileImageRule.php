<?php

namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;

class FileImageRule extends BaseRule implements Rule
{
    public string $transBase;
    public ?array $mimes;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $formBaseTrans='',array $mimes=null)
    {
        $this->transBase=$formBaseTrans;
        $this->mimes=$mimes;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rules[]="bail";
        if(filled($this->mimes))
        {
            $rules[]='mimes:'.implode(',',$this->mimes);
        }
        $rules=array_merge($rules,['file','image','max:'.config('custom.images.max-size')]);
        $trans=$this->getAttribute('image',$attribute);
        if(!$this->validate($value,$rules,'image',[],$trans))
        {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = $this->getValidator()->errors();

        if ($errors->any()) {
            return $errors->all();
        }
        return __('rules.file.image');
    }
}

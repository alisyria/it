<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Rule as ValRule;

class MobileRule extends BaseRule implements Rule
{
    public $transBase;
    public $countries;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $formBaseTrans='',array $countries=[])
    {
        $this->transBase=$formBaseTrans;
        $this->countries=$countries ? $countries:['iq'];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $mobileRule=ValRule::phone()->country($this->countries)->mobile();
        $rules=[$mobileRule];
        $trans=$this->getAttribute('mobile',$attribute);
        if(!$this->validate($value,$rules,'mobile',[],$trans))
        {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = $this->getValidator()->errors();

        if ($errors->any()) {
            return $errors->all();
        }
        return __('rules.mobile');
    }
}

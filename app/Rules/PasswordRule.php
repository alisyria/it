<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PasswordRule extends BaseRule implements Rule
{
    public $formBaseTrans;
    public $action;
    public $minLength;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $formBaseTrans='',$action='POST',$minLength=6)
    {
        $this->transBase=$formBaseTrans;
        $this->action=$action;
        $this->minLength=$minLength;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->action!='PUT')
        {
            $rules[]='required';
        }
        else
        {
            $rules[]='nullable';
        }
        $rules[]='min:'.$this->minLength;
        $trans=$this->getAttribute('password',$attribute);
        if(!$this->validate($value,$rules,'password',[],$trans))
        {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = $this->getValidator()->errors();

        if ($errors->any()) {
            return $errors->all();
        }
        return __('rules.password.invalid');
    }
}

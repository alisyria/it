<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\Rule;

class ModelExistRule implements Rule
{
    /** @var string */
    public $transBase;
    /** @var string */
    protected $modelClassName;
    /** @var string */
    protected $modelAttribute;
    /** @var string */
    protected $attribute;
    protected ?Closure $conditions;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $formBaseTrans='',string $modelClassName, string $attribute = 'id',
                                Closure $conditions=null)
    {
        $this->transBase=$formBaseTrans;
        $this->modelClassName = $modelClassName;
        $this->modelAttribute = $attribute;
        $this->conditions=$conditions;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        return $this->modelClassName::where($this->modelAttribute, $value)
            ->when(!is_null($this->conditions),$this->conditions)
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('rules.model_exist', [
            'attribute' => __($this->transBase.'.'.$this->attribute),
        ]);
    }
}

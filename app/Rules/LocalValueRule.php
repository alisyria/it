<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LocalValueRule extends BaseRule implements Rule
{
    protected $required;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(bool $required=false)
    {
        $this->required=$required;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_null($value))
        {
            return true;
        }
        if(!$this->validate($value,[
            'array'
        ],'value')){
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = $this->getValidator()->errors();

        if ($errors->any()) {
            return $errors->all();
        }
        return __('rules.local_value');
    }
}

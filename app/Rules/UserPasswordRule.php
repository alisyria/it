<?php

namespace App\Rules;

use App\Models\User;
use Hash;
use Illuminate\Contracts\Validation\Rule;

class UserPasswordRule extends BaseRule implements Rule
{
    public $transBase;
    public $user;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $formBaseTrans='',User $user)
    {
        $this->transBase=$formBaseTrans;
        $this->user=$user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!Hash::check($value,$this->user->password))
        {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
//        $errors = $this->getValidator()->errors();
//
//        if ($errors->any()) {
//            return $errors->all();
//        }
        return __('rules.password.incorrect_user_password');
    }
}

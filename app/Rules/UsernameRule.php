<?php

namespace App\Rules;

use App\Enums\UsernameType;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Rule as ValRule;

class UsernameRule extends BaseRule implements Rule
{
    public $transBase;
    public $usernameTypeId;
    public $countryCodes;
    public $action;
    public $ignoreValue;
    public $ignoreField;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $formBaseTrans='',string $usernameTypeId,array $countryCodes=[],
        $action='POST',$ignoreValue=null,$ignoreField='id')
    {
        $this->transBase=$formBaseTrans;
        $this->usernameTypeId=$usernameTypeId;
        $this->countryCodes=$countryCodes;
        $this->action=$action;
        $this->ignoreValue=$ignoreValue;
        $this->ignoreField=$ignoreField;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $usernameRule=ValRule::unique('users','username')->where(function($query){
            $query->whereNotNull('activated_at')
                ->where('username_type', $this->usernameTypeId)
                ->whereNull('deleted_at')
                ->when($this->action=="PUT",function($query){
                    $query->where($this->ignoreField,'<>', $this->ignoreValue);
                });
        });
        $rules[]=$usernameRule;
        $trans=$this->getAttribute('username',$attribute);
        if(!$this->validate($value,$rules,'username',[],$trans))
        {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = $this->getValidator()->errors();

        if ($errors->any()) {
            return $errors->all();
        }
        return __('rules.username');
    }
}

<?php

use App\Http\Controllers\Admin\Auth\EmailVerificationController;
use App\Http\Controllers\Admin\Auth\LogoutController;
use App\Http\Controllers\Admin\DataSources\CompaniesDataSource;
use App\Http\Controllers\Admin\DataSources\GovernoratesDataSource;
use App\Http\Controllers\Admin\DataSources\OwnersDataSource;
use App\Http\Controllers\Admin\DataSources\ProductsDataSource;
use App\Http\Controllers\Admin\DataSources\RegionsDataSource;
use App\Http\Controllers\Admin\DataSources\RootCategoriesDataSource;
use App\Http\Controllers\Admin\DownloadTranslationFile;
use App\Http\Controllers\Admin\OrderInvoiceController;
use App\Http\Livewire\Admin\Ad;
use App\Http\Livewire\Admin\Ads;
use App\Http\Livewire\Admin\Auth\Login;
use App\Http\Livewire\Admin\Auth\Passwords\Confirm;
use App\Http\Livewire\Admin\Auth\Passwords\Email;
use App\Http\Livewire\Admin\Auth\Passwords\Reset;
use App\Http\Livewire\Admin\Auth\Verify;
use App\Http\Livewire\Admin\Categories;
use App\Http\Livewire\Admin\Companies;
use App\Http\Livewire\Admin\Company;
use App\Http\Livewire\Admin\CompanyPackages;
use App\Http\Livewire\Admin\Complaints;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Device;
use App\Http\Livewire\Admin\Devices;
use App\Http\Livewire\Admin\Employee;
use App\Http\Livewire\Admin\Employees;
use App\Http\Livewire\Admin\Enquiries;
use App\Http\Livewire\Admin\Governorate;
use App\Http\Livewire\Admin\Governorates;
use App\Http\Livewire\Admin\Localization;
use App\Http\Livewire\Admin\Notifications;
use App\Http\Livewire\Admin\Order;
use App\Http\Livewire\Admin\Orders;
use App\Http\Livewire\Admin\Profile;
use App\Http\Livewire\Admin\Region;
use App\Http\Livewire\Admin\Regions;
use App\Http\Livewire\Admin\Reports\CompaniesOrders;
use App\Http\Livewire\Admin\Reports\CompaniesSales;
use App\Http\Livewire\Admin\Reports\TopProducts;
use App\Http\Livewire\Admin\Reports\TopRegions;
use App\Http\Livewire\Admin\Reports\TopShops;
use App\Http\Livewire\Admin\Role;
use App\Http\Livewire\Admin\Roles;
use App\Http\Livewire\Admin\Settings;
use App\Http\Livewire\Admin\Shop;
use App\Http\Livewire\Admin\Shops;
use App\Http\Livewire\Admin\Notification;
use App\Enums\Permission;

Route::middleware('guest:admin')->group(function () {
    Route::get('login', Login::class)
        ->name('login');
    Route::get('password/reset', Email::class)
        ->name('password.request');
    Route::get('password/reset/{token}', Reset::class)
        ->name('password.reset');
});

Route::middleware('auth:admin')
    ->group(function () {
         Route::get('email/verify', Verify::class)
            ->middleware('throttle:6,1')
            ->name('verification.notice');

        Route::get('password/confirm', Confirm::class)
            ->name('password.confirm');
        Route::get('email/verify/{id}/{hash}', EmailVerificationController::class)
            ->middleware('signed')
            ->name('verification.verify');
        Route::post('logout', LogoutController::class)
            ->name('logout');

        Route::get('/',Dashboard::class)
            ->name('home');

        Route::prefix('employees-section')->group(function(){
            Route::middleware(['permission:'.Permission::MANAGE_EMPLOYEES])->group(function(){
                Route::get('employees',Employees::class)
                    ->name('employees.index');
                Route::get('employees/{employee}',Employee::class)
                    ->name('employees.show');
            });

            Route::middleware(['permission:'.Permission::MANAGE_ROLES])->group(function(){
                Route::get('roles',Roles::class)
                    ->name('roles.index');
                Route::get('roles/{role}',Role::class)
                    ->name('roles.show');
            });
        });

        Route::middleware(['permission:'.Permission::MANAGE_DEVICES])->group(function(){
            Route::get('devices',Devices::class)
                ->name('devices.index');
            Route::get('devices/{device}',Device::class)
                ->name('devices.show');
        });

        Route::middleware(['permission:'.Permission::MANAGE_ORDERS])->group(function(){
            Route::get('orders',Orders::class)
                ->name('orders.index');
            Route::get('orders/{order}',Order::class)
                ->name('orders.show');
        });

        Route::prefix('settings')->group(function(){

            Route::middleware(['permission:'.Permission::MANAGE_LOCALIZATION])->group(function(){
                Route::get('localization', Localization::class)
                    ->name('localization.index');
                Route::get('localization/download',DownloadTranslationFile::class)
                    ->name('localization.download');
            });

            Route::middleware(['permission:'.Permission::MANAGE_SETTINGS])->group(function(){
                Route::get('/others', Settings::class)
                    ->name('settings.others');
            });
        });

//        Route::middleware(['permission:'.Permission::MANAGE_NOTIFICATIONS])->group(function(){
//            Route::get('notifications', Notifications::class)
//                ->name('notifications.index');
//            Route::get('notifications/{notification}', Notification::class)
//                ->name('notifications.show');
//        });
        Route::get('profile',Profile::class)
            ->name('profile.index');

        Route::prefix('data-sources')
            ->name('dataSources.')
            ->group(function(){
                Route::get('owners',OwnersDataSource::class)->name('owners');
            });
    });
Route::post('sort', '\Rutorika\Sortable\SortableController@sort');

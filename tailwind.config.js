const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    theme: {
        extend: {
            fontFamily: {
                sans: ['Inter var', ...defaultTheme.fontFamily.sans],
            },
        },
        opacity:{
            '0': '0',
            '25': '.25',
            '50': '.5',
            '75': '.75',
            '10': '.1',
            '20': '.2',
            '30': '.3',
            '40': '.4',
            '50': '.5',
            '60': '.6',
            '70': '.7',
            '80': '.8',
            '90': '.9',
            '100': '1',
        },
        aspectRatio: { // defaults to {}
            'none': 0,
            'square': [1, 1], // or 1 / 1, or simply 1
            '16/9': [16, 9],  // or 16 / 9
            '4/3': [4, 3],    // or 4 / 3
            '21/9': [21, 9],  // or 21 / 9
        },
    },
    variants: {
        aspectRatio: ['responsive'], // defaults to ['responsive']
    },
    purge: {
        content: [
            './app/**/*.php',
            './resources/**/*.html',
            './resources/**/*.js',
            './resources/**/*.jsx',
            './resources/**/*.ts',
            './resources/**/*.tsx',
            './resources/**/*.php',
            './resources/**/*.vue',
            './resources/**/*.twig',
            './vendor/livewire-ui/modal/resources/views/*.blade.php',
            './storage/framework/views/*.php',
        ],
        options: {
            defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
            whitelistPatterns: [/-active$/, /-enter$/, /-leave-to$/, /show$/],
            safelist: [
                'sm:max-w-2xl',
                'sm:max-w-4xl',
                'sm:max-w-6xl'
            ]
        },
    },
    plugins: [
        require('@tailwindcss/ui'),
        require('@tailwindcss/typography'),
        require("tailwindcss-responsive-embed"),
        require("tailwindcss-aspect-ratio"),
        require('tailwindcss-rtl'),
    ],
};

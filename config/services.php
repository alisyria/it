<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],


    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
    'passport'=>[
        'client-id'=> env('PASSPORT_PASSWORD_CLIENT_ID'),
        'client-secret'=> env('PASSPORT_PASSWORD_CLIENT_SECRET')
    ],
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT_URL'),
    ],
    'facebook'=>[
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_REDIRECT_URL'),
    ],
    'smart_sms'=>[
        'url'=>env('SMART_SMS_URL'),
        'credentials'=>[
                'username'=>env('SMART_SMS_USERNAME'),
                'password'=>env('SMART_SMS_PASSWORD'),
                'type'=>env('SMART_SMS_TYPE'),
                'dlr'=>env('SMART_SMS_DLR'),
            ],
        'log_enabled'=>env('SMART_SMS_LOG_ENABLED',false)
    ],
    'zain'=>[
        'url'=>env('ZAIN_SMS_URL'),
        'credentials'=>[
            'token'=>env('ZAIN_SMS_TOKEN'),
        ],
    ],
    'unsplash'=>[
        'key'=> env('UNSPLASH_KEY'),
        'secret'=> env('UNSPLASH_SECRET'),
        'utmSource'=>env('NSPLASH_UTM_SOURCE')
    ],
];

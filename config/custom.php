<?php

return[
    'app_versions'=>[1],
    'last_ios_version'=> env('APP_IOS_LAST_VERSION','production'),
    'languages'=>['ar','en'],
    'images'=>[
        'max-size'=>8*1024
    ],
    'avatar'=>[
        'generation_method'=>'default', //name,default
        'default_path'=>storage_path('app/public/defaults/avatar.png'),
    ],
    'verifications'=>[
        'duration'=>20,  //minutes
        'throttle_duration'=>1440,//minutes,
        'throttle_tries'=>10,
    ],
    'reset_password'=>[
        'duration'=>20  //minutes
    ],
    'pagination'=>[
        'general_notifications'=>10,
    ]
];


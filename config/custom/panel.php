<?php

return [
    'logo_tiny'=>'img/logo/logo_50_50.png',
    'logo'=>'img/logo/logo.png',
    /**
    |blue,blue-light,black,black-light,green,green-light,purple,purple-light,red,red-light
    |yellow,yellow-light
     */
    'skin'=>'purple-light',
    'locale'=>'ar',
];
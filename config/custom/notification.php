<?php

return [
    /*
    | drivers:log,smart,syriatel
     */
    'sms_channel'=>env('NOTIFICATION_SMS_CHANNEL','log'),
    /*
    | drivers:log,production
     */
    'push_channel'=>env('NOTIFICATION_PUSH_CHANNEL','log'),
    /*
    |
     */
    'general_push'=>[
        'manager_queue'=>env('PUSH_NOTIFICATIONS_MANAGER_QUEUE'),
        'chunk_queue'=>env('PUSH_NOTIFICATIONS_CHUNK_QUEUE'),
        'production'=>env('PUSH_NOTIFICATIONS_BROADCAST_PRODUCTION',true),
        'max_chunks_minute'=>env('PUSH_NOTIFICATIONS_CHUNK_MAX',60),
    ],
];
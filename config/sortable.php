<?php

return [
    'entities' => [
        'appLanguages'=>\App\Models\AppLanguage::class,
        'categories'=>\App\Models\Category::class,
        'ads'=>\App\Models\Ad::class,
        'governorates'=>\App\Models\Governorate::class,
        'regions'=>\App\Models\Region::class,
        'companyPackages'=>\App\Models\CompanyPackage::class,
        // 'articles' => '\Article' for simple sorting (entityName => entityModel) or
        // 'posts' => ['entity' => '\Post', 'relation' => 'tags'] for many to many or many to many polymorphic relation sorting
    ],
];

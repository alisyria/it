const mix = require("laravel-mix");

require("laravel-mix-tailwind");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/admin/app.js", "public/js/admin/app.js")
    .scripts('resources/js/admin/scripts','public/js/admin/scripts.js')
    .sass("resources/sass/admin/app.scss", "public/css/admin/app.css")
    //.copyDirectory('resources/img/admin','public/img/admin')
    .tailwind("./tailwind.config.js")
    .sourceMaps();

mix.version();

// if (mix.inProduction()) {
//     mix.version();
// }

